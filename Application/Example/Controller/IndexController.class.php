<?php

namespace Example\Controller;

/**
 * IndexController 类
 *
 * @since VER:1.0; DATE:2016-6-18; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class IndexController {
    function index(){
        $this->display();
    }
}
