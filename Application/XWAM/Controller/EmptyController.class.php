<?php

namespace XWAM\Controller;

/**
 * 空模块，主要用于显示404页面，请不要删除
 */
class EmptyController extends AppbaseController {

    /**
     * 空控制器的空操作
     * @since 1.0 <2015-3-26> SoChishun Added.
     */
    public function index() {
        $this->display('XPage/404');
    }

}
