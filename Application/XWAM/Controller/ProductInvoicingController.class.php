<?php

namespace XWAM\Controller;

/**
 * ProductInvoicingController 类
 *
 * @since 1.0 <2016-5-28> SoChishun <14507247@qq.com> Added.
 */
class ProductInvoicingController extends AppbaseController {

    function invoicing_list_search() {
        $search = $_GET;
        $where = array();
        if (!empty($search['search_key'])) {
            $where['g.title'] = array('like', '%' . $search['search_key'] . '%');
        }
        if (!empty($search['category_id'])) {
            $where['g.category_id'] = $search['category_id'];
        }
        if (!empty($search['product_id'])) {
            $where['i.product_id'] = $search['product_id'];
        }
        $where['i.site_id'] = $this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    function invoicing_list($product_id = 0) {
        $asearch = $this->invoicing_list_search();
        $m_invoicing = new \XWAM\Model\ProductInvoicingModel();
        $list = $m_invoicing->get_paging_list($page, array('table' => 't_porg_product_invoicing i left join t_porg_product_goods g on i.product_id=g.id', 'where' => $asearch['where'], 'field' => 'i.*, g.title as product_title', 'order' => 'i.id desc'), array('page_params' => $asearch['search']));
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    function invoicing_out_edit($id = 0, $product_id = 0) {
        if (!$id && !$product_id) {
            exit('参数有误!');
        }
        $data = array();
        if ($id) {
            $m_invoicing = new \XWAM\Model\ProductInvoicing();
            $data = $m_invoicing->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_name, 'site_id' => $this->site_id, 'product_id' => $product_id, 'old_stock' => 0);
        }
        $this->assign('meta_title', $id ? '编辑' . $data['title'] : '新增出库');
        $this->assign('data', $data);
        $this->display();
    }

    function invoicing_out_edit_save() {
        $m_invoicing = new \XWAM\Model\ProductInvoicingModel();
        $result = $m_invoicing->save_invoicing_out();
        $this->dialogJump($result['status'], $result['info']);
    }

    function invoicing_in_edit($id = 0, $product_id = 0) {
        if (!$id && !$product_id) {
            exit('参数有误!');
        }
        $data = array();
        if ($id) {
            $m_invoicing = new \XWAM\Model\ProductInvoicing();
            $data = $m_invoicing->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_name, 'site_id' => $this->site_id, 'product_id' => $product_id, 'old_stock' => 0);
        }
        $this->assign('meta_title', $id ? '编辑' . $data['title'] : '新增入库');
        $this->assign('data', $data);
        $this->display();
    }

    function invoicing_in_edit_save() {
        $m_invoicing = new \XWAM\Model\ProductInvoicingModel();
        $result = $m_invoicing->save_invoicing_in();
        $this->dialogJump($result['status'], $result['info']);
    }

    // 编辑备注 2016-6-7
    function remark_edit($id = 0) {
        if (!$id) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_invoicing = new \XWAM\Model\ProductInvoicingModel();
        $this->assign('data', $m_invoicing->find($id));
        $this->display();
    }

    function remark_edit_save($id = 0, $remark = '') {
        if (!$id) {
            $this->error('参数有误!');
        }
        $m_invoicing = new \XWAM\Model\ProductInvoicingModel();
        $m_invoicing->where(array('id' => $id))->setField('remark', $remark);
        $this->dialogJump(true);
    }

    function change_status($id, $status) {
        $m_invoicing = new \XWAM\Model\ProductInvoicing();
        $this->ajaxReturn($m_invoicing->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_invoicing = new \XWAM\Model\ProductInvoicing();
        $this->ajaxReturn($m_invoicing->change_sort($id, $sort));
    }

    function invoicing_delete($id = '') {
        $m_invoicing = new \XWAM\Model\ProductInvoicing();
        $result = $m_invoicing->delete_invoicing($id);
        $this->dialogJump($result['status'], $result['info']);
    }

}
