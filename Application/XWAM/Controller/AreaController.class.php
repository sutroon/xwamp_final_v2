<?php

namespace XWAM\Controller;

/**
 * 地区控制器
 * 
 * @since 1.0 2014-12-8 by sutroon
 */
class AreaController extends AppbaseController {

    /**
     * 获取json数据
     * @since 2014-12-10 by sutroon
     * @example 
     *  省份：?s=Region/json.html
     *  城市：?s=Region/json/province/福建省.html
     *  地区：?s=Region/json/province/福建省/city/厦门市.html
     */
    public function index($province = '', $city = '') {
        $m_area = new \XWAM\Service\AreaModel();
        $list = $m_area->get_areas($province, $city);
        $this->ajaxReturn($list);
    }

}
