<?php

namespace XWAM\Controller;

/**
 * IndexController控制器
 *
 * @since 1.0.0 <2015-03-21> SoChishun <14507247@qq.com> Added.
 */
class IndexController extends AppbaseController {

    public function index() {
        // 主菜单资料
        $m_rule = new \XWAM\Model\RuleModel();
        $this->assign('menuset', $m_rule->get_menus());
        $site_conf = session('site_conf');
        $this->display($site_conf['site_home_url']);
    }

    public function home() {
        $this->display();
    }

    // 2016-5-9 sochishun added.
    public function desktop() {
        // 主菜单资料
        $m_rule = new \XWAM\Model\RuleModel();
        $this->assign('menuset', $m_rule->get_menus());
        $this->display();
    }

    function about() {
        $this->display();
    }

}
