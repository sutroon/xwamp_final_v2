<?php

namespace XWAM\Controller;

/**
 * CustomConfController 类
 *
 * @since VER:1.0; DATE:2016-5-26; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class CustomConfController extends AppbaseController {

    function conf_list_search() {
        $search = $_GET;
        if ($search['search_key']) {
            $where['_string'] = sprintf("title like '%s'", $search['search_key'], $search['search_key']);
        }
        $where['site_id'] = $this->site_id;
        $where['conf_group'] = $search['group'];
        return array('where' => $where, 'search' => $search);
    }

    function conf_list($group = '') {
        if (!$group) {
            exit('参数无效');
        }
        $asearch = $this->conf_list_search();
        $m_conf = new \XWAM\Model\CustomConfModel();
        $list = $m_conf->get_paging_list($page, array('where' => $asearch['where'], 'order' => 'sort, id desc'), array('page_params' => $asearch['search']));
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    function conf_edit($id = '', $group = '') {
        $data = array();
        $m_conf = new \XWAM\Model\CustomConfModel();
        if ($id) {
            $data = $m_conf->find($id);
        }
        if (!$data) {
            $data = array('conf_group' => $group, 'site_id' => $this->site_id, 'user_name' => $this->user_name);
        }
        $this->assign('data', $data);
        $this->display();
    }

    function conf_edit_save() {
        $m_conf = new \XWAM\Model\CustomConfModel();
        $result = $m_conf->save_conf();
        $this->dialogJump($result['status'], $result['info']);
    }

    function conf_delete($id = '') {
        $m_conf = new \XWAM\Model\CustomConfModel();
        $result = $m_conf->remove_conf($id);
        $this->ajaxReturn($result);
    }

    function change_status($id, $status) {
        $m_conf = new \XWAM\Model\CustomConfModel();
        $this->ajaxReturn($m_conf->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_conf = new \XWAM\Model\CustomConfModel();
        $this->ajaxReturn($m_conf->change_sort($id, $sort));
    }

}
