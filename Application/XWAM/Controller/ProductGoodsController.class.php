<?php

namespace XWAM\Controller;

/**
 * ProductGoodsController 类
 *
 * @since 1.0 <2016-5-28> SoChishun <14507247@qq.com> Added.
 */
class ProductGoodsController extends AppbaseController {

    protected $content_codes = array(array('id' => 'description', 'text' => '商品描述'), array('id' => 'after_service', 'text' => '售后保障'));

    function product_goods_list_search() {
        $search = $_GET;
        $where = array();
        if (!empty($search['search_key'])) {
            $where['title|marque'] = array('like', '%' . $search['search_key'] . '%');
        }
        if (!empty($search['category_id'])) {
            $where['category_id'] = $search['category_id'];
        }
        if (!empty($search['title'])) {
            $where['title'] = array('like', '%' . $search['title'] . '%');
        }
        $where['site_id'] = $this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    function product_goods_list($category_id = 0) {
        $asearch = $this->product_goods_list_search();
        $m_goods = new \XWAM\Model\ProductGoodsModel();
        $list = $m_goods->get_paging_list($page, array('where' => $asearch['where'], 'order' => 'id desc'), array('page_params' => $asearch['search']));
        $m_category = new \XWAM\Model\ProductCategoryModel();
        $categories = $m_category->get_idtext_list($this->site_id, true);
        if ($list && $categories) {
            foreach ($list as &$row) {
                $cid = $row['category_id'];
                $row['category_text'] = isset($categories[$cid]) ? $categories[$cid] : '';
            }
        }
        $category_title = $category_id && isset($categories[$category_id]) ? $categories[$category_id] : '商品管理';
        $this->assign('meta_title', $category_title);
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    function get_goods_json_item($id = '') {
        $m_goods = new \XWAM\Model\ProductGoodsModel();
        $this->ajaxReturn($m_goods->find($id));
    }

    function get_goods_json_list() {
        $m_goods = new \XWAM\Model\ProductGoodsModel();
        $this->ajaxReturn($m_goods->get_idtext_list($this->site_id));
    }

    function product_goods_edit($id = 0, $category_id = 0) {
        $data = array();
        if ($id) {
            $m_goods = new \XWAM\Model\ProductGoodsModel();
            $data = $m_goods->find($id);
            unset($m_goods);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_name, 'site_id' => $this->site_id);
        }
        if ($category_id) {
            $data['category_id'] = $category_id;
        }
        $this->assign('meta_title', $id ? '编辑' . $data['title'] : '新增商品');
        $this->assign('data', $data);
        $this->assign('content_codes', $this->content_codes);
        $this->display();
    }

    function product_goods_edit_save() {
        $m_goods = new \XWAM\Model\ProductGoodsModel();
        $result = $m_goods->save_goods();
        $this->dialogJump($result['status'], $result['info']);
    }

    // 2016-7-1
    function product_content_edit($id = 0, $code = '') {
        if(!$id || !$code){
            $this->error('参数有误!');
        }
        $m_goods = new \XWAM\Model\ProductGoodsModel();
        $data = $m_goods->find_content($id, $code);
        unset($m_goods);
        $this->assign('data', $data);
        $this->assign('content_codes', $this->content_codes);
        $this->display();
    }

    function product_content_edit_save() {
        $m_goods = new \XWAM\Model\ProductGoodsModel();
        $result = $m_goods->save_content();
        $this->dialogJump($result['status'], $result['info']);
    }

    function change_status($id, $status) {
        $m_goods = new \XWAM\Model\ProductGoodsModel();
        $this->ajaxReturn($m_goods->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_goods = new \XWAM\Model\ProductGoodsModel();
        $this->ajaxReturn($m_goods->change_sort($id, $sort));
    }

    function product_goods_delete($id = '') {
        $m_goods = new \XWAM\Model\ProductGoodsModel();
        $result = $m_goods->delete_goods($id);
        $this->dialogJump($result['status'], $result['info']);
    }

}
