<?php

namespace XWAM\Controller;

/**
 * Description of SystemConfController
 *
 * @since 1.0 <2015-10-20> SoChishun <14507247@qq.com> Added.
 * @since 2.0 2016-6-15 SoChishun Config拆分为SystemConf和CustomConf
 */
class SystemConfController extends AppbaseController {

    function conf_list_search() {
        $search = $_GET;
        if ($search['search_key']) {
            $where['_string'] = sprintf("conf_name like '%s' or title like '%s'", $search['search_key'], $search['search_key']);
        }
        $where['site_id'] = $this->site_id;
        return array('where' => $where, 'search' => $search);
    }

    function conf_list() {
        $asearch = $this->conf_list_search();
        $m_conf = new \XWAM\Model\SystemConfModel();
        $list = $m_conf->get_paging_list($page, array('where' => $asearch['where'], 'order' => 'sort, id desc'), array('page_params' => $asearch['search']));
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    function conf_edit($id = '') {
        $data = array();
        $m_conf = new \XWAM\Model\SystemConfModel();
        if ($id) {
            $data = $m_conf->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_name, 'site_id' => $this->site_id);
        }
        $this->assign('data', $data);
        $this->display();
    }

    function conf_edit_save() {
        $m_conf = new \XWAM\Model\SystemConfModel();
        $result = $m_conf->save_conf();
        $this->dialogJump($result['status'], $result['info']);
    }

    function conf_delete($id = '') {
        $m_conf = new \XWAM\Model\SystemConfModel();
        $result = $m_conf->remove_conf($id);
        $this->ajaxReturn($result);
    }

    function change_status($id, $status) {
        $m_conf = new \XWAM\Model\SystemConfModel();
        $this->ajaxReturn($m_conf->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_conf = new \XWAM\Model\SystemConfModel();
        $this->ajaxReturn($m_conf->change_sort($id, $sort));
    }

}
