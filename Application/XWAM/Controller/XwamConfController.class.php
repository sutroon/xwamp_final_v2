<?php

namespace XWAM\Controller;

/**
 * Description of XwamConfigController
 *
 * @since 1.0 <2016-6-20> SoChishun <14507247@qq.com> Added.
 * @since 1.1 2016-7-6 SoChishun XWAMConfig 重构为 XwamConf
 */
class XwamConfController extends AppbaseController {

    function store_list_search() {
        $search = $_GET;
        if ($search['search_key']) {
            $where['_string'] = sprintf("title like '%s'", $search['search_key']);
        }
        $where['site_id'] = $this->site_id;
        return array('where' => $where, 'search' => $search);
    }

    // 2016-5-26
    function store_list() {
        $asearch = $this->store_list_search();
        $m_conf = new \XWAM\Model\StoreConfModel();
        $list = $m_conf->get_paging_list($page, array('where' => $asearch['where'], 'order' => 'sort, id desc'), array('page_params' => $asearch['search']));
        unset($m_conf);
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    // 2016-4-21
    public function site_edit($site_id = 0, $user_name = '') {
        if (!$site_id) {
            $site_id = $this->site_id;
            $user_name = $this->user_name;
        }
        $m_site = new \XWAM\Model\SiteConfModel();
        $data = $m_site->find_site($site_id, $user_name);
        $this->assign('data', $data);
        $this->display();
    }

    public function site_edit_save() {
        $m_site = new \XWAM\Model\SiteConfModel();
        $result = $m_site->save_site();
        $this->dialogJump($result['status'], $result['info']);
    }

}
