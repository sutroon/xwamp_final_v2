<?php

namespace XWAM\Controller;

/**
 * EmployeeController 类
 *
 * @since VER:1.0; DATE:2016-5-26; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class EmployeeController extends AppbaseController {
    public function employee_list(){
        $this->display();
    }
}
