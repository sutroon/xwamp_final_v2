<?php

namespace XWAM\Controller;

/**
 * ProductCategoryController 控制器
 * @since 1.0 <2016-5-28> SoChishun <14507247@qq.com> Added.
 */
class ProductCategoryController extends AppbaseController {

    /**
     * 导出数据
     * @since 1.0 <2014-6-13> SoChishun Added.
     */
    public function category_export() {
        $asearch = $this->get_category_search_data();
        $m_category = new \XWAM\Model\ProductCategoryModel();
        $list = $m_category->scope('export')->where($asearch['where'])->select();
        if (!$list) {
            $this->ajaxMsg(false, '找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    public function category_copy($id = 0) {
        if (!$id) {
            $this->ajaxMsg(false, '参数无效');
        }
        $m_category = new \XWAM\Model\ProductCategoryModel();
        $m_category->copy_category($id);
        $this->ajaxMsg(true);
    }

    public function category_list() {
        $search=$_GET;
        $m_category = new \XWAM\Model\ProductCategoryModel();
        $this->assign('tree', $m_category->select_tree(array('where' => $where)));
        $this->assign('search',$search);
        $this->display_cpp();
    }

    public function category_edit($id = 0, $pid = 0) {
        $data = array();
        if ($id) {
            $m_category = new \XWAM\Model\ProductCategoryModel();
            $data = $m_category->find($id);
        }
        if (!$data) {
            $data = array('pid' => $pid, 'site_id' => $this->site_id);
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function category_edit_save() {
        $m_category = new \XWAM\Model\ProductCategoryModel();
        $result = $m_category->save_category();
        $this->dialogJump($result['status'], $result['info']);
    }

    public function get_category_tree_json($pid = 0) {
        $options = array();
        if ($pid) {
            $options['where']['pid'] = $pid;
        }
        $m_category = new \XWAM\Model\ProductCategoryModel();
        $data = $m_category->select_json_tree($options);
        $this->ajaxReturn($data);
    }

    function change_status($id, $status) {
        $m_category = new \XWAM\Model\ProductCategoryModel();
        $this->ajaxReturn($m_category->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_category = new \XWAM\Model\ProductCategoryModel();
        $this->ajaxReturn($m_category->change_sort($id, $sort));
    }

    /**
     * category_delete操作
     * @param string $id 主键编号
     * @since 1.0<2015-7-31> SoChishun Added.
     */
    public function category_delete($id = '') {
        $m_category = new \XWAM\Model\ProductCategoryModel();
        $this->ajaxReturn($m_category->delete_category($id));
    }
}
