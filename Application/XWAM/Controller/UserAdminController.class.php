<?php

namespace XWAM\Controller;

/**
 * 用户控制器（包含管理员和座席）
 *
 * @since 1.0 <2014-6-13> sutroon Added.
 */
class UserAdminController extends AppbaseController {

    function user_list_search() {
        $search = $_GET;
        if (!empty($search['search_key'])) {
            $searchkey = $search['search_key'];
            $where['_string'] = "user_name like '%$searchkey%' or name like '%$searchkey%' or mobile like '%$searchkey%'";
        }
        if (!empty($search['user_name'])) {
            $where['user_name'] = array('like', '%' . $search['user_name'] . '%');
        }
        $search['type_name'] = 'ADMIN';
        $where['type_name'] = $search['type_name'];
        $where['site_id'] = $this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    public function user_list() {
        $asearch = $this->user_list_search();
        $this->assign('usertype_list', array('ADMIN' => '管理员', 'SERVICES' => '客服人员', 'CUSTOMER' => 'VIP客户'));
        $m_admin = new \XWAM\Model\UserAdminModel();
        $list = $m_admin->get_paging_list($page, array('where' => $asearch['where'], 'order' => 'id'), array('page_params' => $asearch['search']));
        $m_user = new \XWAM\Model\UserModel();
        $m_user->set_fields_text($list);
        $this->assign('search', $asearch['search']);
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->display_cpp();
    }

    public function user_export() {
        $list = $m_user->select();
        if (!$list) {
            $this->error('找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    /**
     * 编辑个人资料
     * @since 1.0 <2015-11-11> SoChishun Added.
     */
    function profile_edit() {
        $this->user_edit($this->user_login_data['id']);
    }

    /**
     * 保存个人资料
     * @since 1.0 <2016-3-11> SoChishun Added.
     */
    function profile_edit_save() {
        $m_admin = new \XWAM\Model\UserAdminModel();
        $result = $m_admin->save_user($this->user_login_data['id']);
        $this->dialogClose($result['status'], $result['info']);
    }

    public function user_edit($id = 0) {
        $m_admin = new \XWAM\Model\UserAdminModel();
        $data = $id ? $m_admin->find($id) : array();
        $this->assign('data', $data);
        $this->assign('meta_title', $id ? '编辑 ' . $data['user_name'] : '新增管理员');
        $this->display();
    }

    public function user_edit_save() {
        $m_admin = new \XWAM\Model\UserAdminModel();
        $result = $m_admin->save_user($this->user_login_data['id']);
        $this->dialogJump($result['status'], $result['info']);
    }

    public function user_delete($id = '') {
        if (!$id) {
            $this->ajaxMsg(false, '编号丢失!');
        }
        if ($id == $this->user_login_data['id']) {
            $this->ajaxMsg(false, '不能删除自己!');
        }
        $m_admin = new \XWAM\Model\UserAdminModel();
        $result = $m_admin->delete_user($id, $this->user_login_data['id']);
        $this->ajaxMsg($result, '删除{%}!');
    }

}
