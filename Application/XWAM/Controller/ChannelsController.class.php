<?php

namespace XWAM\Controller;

/**
 * DocumentCategoryController 类
 *
 * @since 1.0 <2015-10-22> SoChishun <14507247@qq.com> Added.
 */
class ChannelsController extends AppbaseController {

    public function channel_list() {
        $where['site_id'] = $this->site_id;
        $where['pid'] = 0;
        $this->assign('type_list', array('DOCUMENT' => array('title' => '文章', 'url' => U('Documents/document_list')), 'FILE' => array('title' => '文件', 'url' => U('File/file_list'))));
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $list = $m_category->select_tree(array('where' => $where));
        $this->assign('tree', $list);
        $this->display_cpp();
    }

    public function channel_edit($id = 0, $pid = 0) {
        $data = array();
        if ($id) {
            $m_category = new \XWAM\Model\DocumentCategoryModel();
            $data = $m_category->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_name, 'site_id' => $this->site_id, 'type_name' => 'page');
        }
        if ($pid) {
            $data['pid'] = $pid;
        }
        $atype_names = array(
            array('value' => 'document-list', 'text' => '文章栏目', 'comment' => '用于在此栏目下添加文章 (可以增加下级栏目)'),
            array('value' => 'file-list', 'text' => '文件栏目', 'comment' => '用于在此栏目下添加文件, 如图片或下载 (可以增加下级栏目)'),
            array('value' => 'node', 'text' => '普通节点', 'comment' => '用于上下层级关联, 自身无任何详细内容 (可以增加下级栏目)'), // 2016-6-17 新增
            array('value' => 'page', 'text' => '单独页面', 'comment' => '会生成一个全新的独立页面 (不能增加下级栏目)'),
            array('value' => 'ulink', 'text' => '分类链接', 'comment' => '可以链接到指定分类 (不能增加下级栏目)'),
            array('value' => 'url', 'text' => '链接地址', 'comment' => '增加外链, 如论坛地址 (不能增加下级栏目)'),
        );
        $this->assign('atype_names', $atype_names);
        $this->assign('data', $data);
        $this->display();
    }

    public function channel_edit_save() {
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $result = $m_category->save_category();
        $this->dialogJump($result['status'], $result['info']);
    }

    public function get_channel_tree_json($type_name = '', $pid = 0) {
        $where['site_id'] = $this->site_id;
        $where['pid'] = $pid;
        if ($type_name) {
            $where['type_name'] = $type_name;
        }
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $data = $m_category->select_json_tree(array('where' => $where));
        $this->ajaxReturn($data);
    }

    function channel_copy($id = 0) {
        if (!$id) {
            $this->ajaxMsg(false, '参数无效');
        }
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $m_category->copy_category($id);
        $this->ajaxMsg(true);
    }

    function change_status($id, $status) {
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $this->ajaxReturn($m_category->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $this->ajaxReturn($m_category->change_sort($id, $sort));
    }

    function channel_delete($id = '') {
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $this->ajaxReturn($m_category->delete_channel($id));
    }

    //********************************link***********************************
    /**
     * link
     * @param type $id
     * @since 1.0 <2015-10-26> SoChishun <14507247@qq.com> Added.
     */
    public function link_edit($id = 0) {
        if (!$id) {
            $this->error('参数有误!');
        }
        $m_category_link = new \XWAM\Model\DocumentCategoryLinkModel();
        $data = $m_category_link->find($id);
        if (!$data) {
            $data['category_id'] = $id;
            $data['action'] = 'add';
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function link_edit_save() {
        $m_category_link = new \XWAM\Model\DocumentCategoryLinkModel();
        $result = $m_category_link->save_link();
        if ($result['status']) {
            $this->success('保存成功');
        } else {
            $this->error(implode('<br />', $result['info']));
        }
    }

    //*******************************content***************************************    

    /**
     * content
     * @param type $id
     * @since 1.0 <2015-10-24> SoChishun <14507247@qq.com> Added.
     */
    public function content_edit($id = 0) {
        if (!$id) {
            $this->error('参数有误!');
        }
        $m_category_content = new \XWAM\Model\DocumentCategoryContentModel();
        $data = $m_category_content->find($id);
        if (!$data) {
            $data['category_id'] = $id;
            $data['action'] = 'add';
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function content_edit_save() {
        $m_category_content = new \XWAM\Model\DocumentCategoryContentModel();
        $result = $m_category_content->save_content();
        if ($result['status']) {
            $this->success('保存成功');
        } else {
            $this->error(implode('<br />', $result['info']));
        }
    }

}
