<?php

namespace XWAM\Controller;

/**
 * SummaryReportController 类
 *
 * @since VER:1.0; DATE:2016-5-26; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class SummaryReportController {
    public function index(){
        $this->display();
    }
}
