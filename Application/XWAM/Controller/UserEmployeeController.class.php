<?php

namespace XWAM\Controller;

/**
 * 员工控制器
 *
 * @since 1.0 <2016-5-26> sutroon Added.
 */
class UserEmployeeController extends AppbaseController {

    function user_list_search() {
        $search = $_GET;
        if (!empty($search['search_key'])) {
            $searchkey = $search['search_key'];
            $where['_string'] = "user_name like '%$searchkey%' or name like '%$searchkey%' or mobile like '%$searchkey%'";
        }
        if (!empty($search['user_name'])) {
            $where['user_name'] = array('like', '%' . $search['user_name'] . '%');
        }
        $search['type_name'] = 'EMPLOYEE';
        $where['site_id'] = $this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    public function user_list() {
        $asearch = $this->user_list_search();
        $m_emp = new \XWAM\Model\UserEmployeeModel();
        $list = $m_emp->get_paging_list($page, array('where' => $asearch['where'], 'order' => 'user_name'), array('page_params' => $asearch['search']));
        unset($m_emp);
        if ($list) {
            $m_store = new \XWAM\Model\StoreConfModel();
            $store_list = $m_store->get_idtext_list(true);
            unset($m_store);
            $m_role = new \XWAM\Model\RoleModel();
            $role_list = $m_role->get_idtext_list(true);
            unset($m_role);
            $sid = '';
            $stext = '';
            $aids = array();
            foreach ($list as &$row) {
                // 分店
                $sid = $row['branch_ids'];
                $stext = '';
                if ($sid && $store_list) {
                    $aids = explode(',', $sid);
                    foreach ($aids as $tempid) {
                        if (isset($store_list[$tempid])) {
                            $stext.=' ' . $store_list[$tempid];
                        }
                    }
                }
                $row['branch_text'] = $stext;
                // 角色
                $sid = $row['role_ids'];
                $stext = '';
                if ($sid && $role_list) {
                    $aids = explode(',', $sid);
                    foreach ($aids as $tempid) {
                        if (isset($role_list[$tempid])) {
                            $stext.=' ' . $role_list[$tempid];
                        }
                    }
                }
                $row['role_text'] = $stext;
            }
        }
        $this->assign('search', $asearch['search']);
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->display_cpp();
    }

    public function user_export() {
        $where = array('site_id' => $this->site_id);
        $m_user = new \XWAM\Model\UserModel();
        $list = $m_user->where($where)->select();
        if (!$list) {
            $this->error('找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    /**
     * 编辑个人资料
     * @since 1.0 <2015-11-11> SoChishun Added.
     */
    function profile_edit() {
        $m_emp = new \XWAM\Model\UserEmployeeModel();
        $data = $m_emp->find($this->user_login_data['id']);
        $this->assign('data', $data);
        $this->display();
    }

    public function profile_edit_save() {
        $m_emp = new \XWAM\Model\UserEmployeeModel();
        $result = $m_emp->save_user($this->user_login_data['id']);
        $this->dialogClose($result['status'], array('success' => '保存成功!<br /><span style="font-size:12px">如有变更[来电弹屏模式]内容，需刷新浏览器才会生效!</span>', 'error' => $result['info'], 'success_action' => '!window.parent.location.reload();', 'ajax' => 5));
    }

    public function user_edit($id = 0) {
        $data = array();
        if ($id) {
            $m_emp = new \XWAM\Model\UserEmployeeModel();
            $data = $m_emp->find($id);
        }
        if (!$data) {
            $data = array('site_id' => $this->site_id);
        }

        $this->assign('data', $data);
        $this->display();
    }

    public function user_edit_save() {
        $m_emp = new \XWAM\Model\UserEmployeeModel();
        $result = $m_emp->save_user($this->user_login_data['id']);
        $this->dialogJump($result['status'], $result['info']);
    }

    public function user_delete($id = '') {
        if (!$id) {
            $this->ajaxMsg(false, '编号丢失!');
        }
        $m_emp = new \XWAM\Model\UserEmployeeModel();
        $result = $m_emp->delete_user($id, $this->user_login_data['id']);
        $this->ajaxMsg($result, '删除{%}!');
    }

    /**
     * 从XCall同步分机到坐席
     * @since 1.0 <2016-2-15> SoChishun Added.
     */
    public function user_from_xcall() {
        $site_conf = session('site_conf');
        if (!$site_conf) {
            exit('登录超时');
        }
        $site_id = $site_conf['id'];
        $m_emp = new \XWAM\Model\UserEmployeeModel();
        $count = $m_emp->user_from_xcall($site_id);
        $this->success('同步成功,共同步 ' . $count . ' 个坐席!');
    }

    function store_picker($uid) {
        $m_temp = new \XWAM\Model\StoreConfModel();
        $list = $m_temp->get_idtext_list();
        $m_emp = new \XWAM\Model\UserEmployeeModel();
        $ids = $m_emp->where(array('id' => $uid))->getField('branch_ids');
        $this->assign('list', $list);
        $this->assign('ids', $ids);
        $this->assign('uid', $uid);
        $this->display();
    }

    function setting_picker_save() {
        $m_emp = new \XWAM\Model\UserEmployeeModel();
        $result = $m_emp->save_conf_setting();
        $this->dialogJump($result['status'], $result['info']);
    }

}
