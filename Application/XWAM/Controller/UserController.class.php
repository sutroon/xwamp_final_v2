<?php

namespace XWAM\Controller;

/**
 * 用户控制器
 * <br />中转站
 *
 * @since 1.0 <2014-6-13> sutroon Added.
 */
class UserController extends AppbaseController {

    // 个人资料跳转
    function profile_edit() {
        $type_name = $this->user_login_data['type_name'];
        $urls = array(
            'SYSTEMADMIN' => U('UserAdmin/profile_edit'),
            'SITEADMIN' => U('UserAdmin/profile_edit'),
            'ADMIN' => U('UserAdmin/profile_edit'),
            'SERVICES' => U('UserServices/profile_edit'),
            'CUSTOMER' => U('UserCustomer/profile_edit'),
            'SEAT' => U('UserSeat/profile_edit'),
            'EMPLOYEE' => U('UserEmployee/profile_edit'),
        );
        if (array_key_exists($type_name, $urls)) {
            redirect($urls[$type_name]);
        } else {
            exit('不支持的用户类型[' . $type_name . ']!');
        }
    }

    function setting() {
        $id = $this->user_login_data['id'];
        if (!$id) {
            exit('未登录!');
        }
        $m_user = new \XWAM\Model\UserModel();
        $data = $m_user->field('id, page_size, lang, theme')->find($id);
        $this->assign('data', $data);
        $this->display();
    }

    public function setting_save() {
        $data = I('post.');
        $data['id'] = $this->user_login_data['id'];
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->save($data);
        $this->dialogClose($result, array('error' => $m_user->getError()));
    }

    /**
     * 编辑密码
     * @since 1.0 <2015-11-11> SoChishun Added.
     */
    function password_edit($uid = 0) {
        if (!$uid) {
            $uid = $this->user_login_data['id'];
        }
        $this->assign('uid', $uid);
        $this->display();
    }

    /**
     * 保存密码
     * @since 1.0 <2016-3-11> SoChishun Added.
     */
    function password_edit_save() {
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->save_password();
        $this->dialogClose($result['status'], array('success' => '保存成功', 'error' => $result['info']));
    }

    // 重置密码
    public function password_reset($uid = 0) {
        if (!$uid) {
            $this->result(false, '参数有误!');
        }
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->where(array('id' => $uid))->setField('password', '123456');
        $this->ajaxMsg($result, '密码重置{%}!');
    }

    // 更改用户在线状态
    function change_online_status($uid, $status) {
        $m_user = new \XWAM\Model\UserModel();
        $this->ajaxReturn($m_user->change_online_status($uid, $status));
    }

    // 2016-1-20
    function user_role_one($uid) {
        $m_user = new \XWAM\Model\UserModel();
        $role_ids = $m_user->where(array('id' => $uid))->getField('role_ids');
        $this->assign('role_ids', $role_ids);
        $this->assign('uid', $uid);
        $this->display();
    }

    function user_role($uid) {
        $m_user = new \XWAM\Model\UserModel();
        $role_ids = $m_user->where(array('id' => $uid))->getField('role_ids');
        $m_role = new \XWAM\Model\RoleModel();
        $list = $m_role->select();
        $this->assign('list', $list);
        $this->assign('role_ids', $role_ids);
        $this->assign('uid', $uid);
        $this->display();
    }

    /**
     * 保存角色
     * @since 1.0 <2015-8-4> SoChishun Added.
     */
    public function user_role_save($uid = 0, $role_ids = '') {
        if (!$role_ids || !$uid) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->save_role($uid, $role_ids);
        $this->dialogJump($result['status'], $result['info']);
    }

    // 2016-1-20
    function user_department_one($uid = '') {
        if (!$uid) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $aids = explode(',', $uid);
        $msg = count($aids) > 1 ? '共' . count($aids) . '人.' : '';
        $m_user = new \XWAM\Model\UserModel();
        $department_ids = $m_user->where(array('id' => $uid))->getField('department_ids');
        $this->assign('department_ids', $department_ids);
        $this->assign('uid', $uid);
        $this->assign('msg', $msg);
        $this->display();
    }

    function user_department($uid = '') {
        if (!$uid) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_user = new \XWAM\Model\UserModel();
        $department_ids = $m_user->where(array('id' => $uid))->getField('department_ids');
        $m_department = new \XWAM\Model\DepartmentModel();
        $this->assign('tree', $m_department->select_tree());
        $this->assign('department_ids', $department_ids);
        $this->assign('uid', $uid);
        $this->display();
    }

    /**
     * 保存部门
     * @since 1.0 <2015-8-4> SoChishun Added.
     */
    public function user_department_save($uid = 0, $department_ids = '') {
        if (!$department_ids || !$uid) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->save_department($uid, $department_ids);
        $this->dialogJump($result['status'], $result['info']);
    }

}
