<?php

namespace XWAM\Controller;

/**
 * 用户控制器（包含管理员和座席）
 *
 * @since 1.0 <2014-6-13> sutroon Added.
 */
class UserServicesController extends AppbaseController {

    function user_list_search() {
        $search = $_GET;
        if (!empty($search['search_key'])) {
            $searchkey = $search['search_key'];
            $where['_string'] = "user_name like '%$searchkey%' or name like '%$searchkey%' or mobile like '%$searchkey%'";
        }
        if (!empty($search['user_name'])) {
            $where['user_name'] = array('like', '%' . $search['user_name'] . '%');
        }
        $search['type_name'] = 'SERVICES';
        $where['type_name'] = $search['type_name'];
        $where['site_id']=$this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    public function user_list() {
        $asearch = $this->user_list_search();
        if ('SYSTEMADMIN' == $this->user_login_data['type_name']) {
            $usertype_list = array('SYSTEMADMIN' => '系统管理员', 'SITEADMIN' => '网站创始人', 'ADMIN' => '管理员');
        } else {
            $usertype_list = array('ADMIN' => '管理员', 'SERVICES' => '客服人员', 'CUSTOMER' => 'VIP客户');
        }
        $this->assign('usertype_list', $usertype_list);
        $m_services=new XWAM\Model\UserServicesModel();
        $list = $m_services->get_paging_list($page, array('where' => $asearch['where'], 'order' => 'sort, id desc'), array('page_params' => $asearch['search']));
        $this->assign('search', $asearch['search']);
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->display_cpp();
    }

    public function user_export() {
        $m_services=new XWAM\Model\UserServicesModel();
        $list = $m_services->select();
        if (!$list) {
            $this->error('找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    /**
     * 编辑个人资料
     * @since 1.0 <2015-11-11> SoChishun Added.
     */
    function profile_edit() {
        $this->user_edit($this->user_login_data['id']);
    }

    public function user_edit($id = 0) {
        $data=array();
        if($id){
            $m_services=new XWAM\Model\UserServicesModel();
            $data=$m_services->find($id);
        }
        $this->assign('data', $data);
        $this->display('user_edit');
    }

    public function user_save() {
        $m_services=new XWAM\Model\UserServicesModel();
        $result = $m_services->save_user($this->user_login_data['id']);
        $this->dialogJump($result['status'], $result['info']);
    }

    public function user_delete($id = '') {
        if (!$id) {
            $this->ajaxMsg(false, '编号丢失!');
        }
        $m_services=new XWAM\Model\UserServicesModel();
        $result = $m_services->delete($id);
        $this->ajaxMsg($result, '删除{%}!');
    }
}
