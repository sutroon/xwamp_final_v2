<?php

namespace XWAM\Controller;

/**
 * DocumentMenuController 类
 *
 * @since 1.0 <2016-6-17> SoChishun <14507247@qq.com> Added.
 */
class DocumentMenuController extends AppbaseController {

    public function menu_list() {
        $where['site_id'] = $this->site_id;
        $where['pid'] = 0;
        $this->assign('type_list', array('DOCUMENT' => array('title' => '文章', 'url' => U('Documents/document_list')), 'FILE' => array('title' => '文件', 'url' => U('File/file_list'))));
        $m_menu = new \XWAM\Model\DocumentMenuModel();
        $list = $m_menu->select_tree(array('where' => $where));
        $this->assign('tree', $list);
        $this->display_cpp();
    }

    public function menu_edit($id = 0, $pid = 0) {
        $data = array();
        if ($id) {
            $m_menu = new \XWAM\Model\DocumentMenuModel();
            $data = $m_menu->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_name, 'site_id' => $this->site_id);
        }
        if ($pid) {
            $data['pid'] = $pid;
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function menu_edit_save() {
        $m_menu = new \XWAM\Model\DocumentMenuModel();
        $result = $m_menu->save_menu();
        $this->dialogJump($result['status'], $result['info']);
    }

    public function get_menu_tree_json($pid = 0) {
        $where['site_id'] = $this->site_id;
        $where['pid'] = $pid;
        $m_menu = new \XWAM\Model\DocumentMenuModel();
        $data = $m_menu->select_json_tree(array('where' => $where));
        $this->ajaxReturn($data);
    }

    function menu_copy($id = 0) {
        if (!$id) {
            $this->ajaxMsg(false, '参数无效');
        }
        $m_menu = new \XWAM\Model\DocumentMenuModel();
        $m_menu->copy_menu($id);
        $this->ajaxMsg(true);
    }

    function change_status($id, $status) {
        $m_menu = new \XWAM\Model\DocumentMenuModel();
        $this->ajaxReturn($m_menu->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_menu = new \XWAM\Model\DocumentMenuModel();
        $this->ajaxReturn($m_menu->change_sort($id, $sort));
    }

    function menu_delete($id = '') {
        $m_menu = new \XWAM\Model\DocumentMenuModel();
        $this->ajaxReturn($m_menu->delete_channel($id));
    }

}
