<?php

namespace XWAM\Controller;

/**
 * Description of MenuController
 *
 * @since 1.0 <2015-9-18> SoChishun <14507247@qq.com> Added.
 */
class MenuController {

    /**
     * 获取侧边栏菜单
     * @param type $id
     * @since VER:1.0; DATE:2016-3-11; AUTHOR:SoChishun;
     */
    public function get_aside_menu($id = 0) {
        $m_rule = new \XWAM\Model\RuleModel();
        $list = $m_rule->get_aside_menus_by_id($id);
        $sout = '<fieldset>';
        foreach ($list as $row) {
            $sout.='<legend>' . $row['title'] . '<a href="javascript:"><i class="fa fa-toggle-on"></i></a></legend>';
            $sout.='<ul>';
            foreach ($row['children'] as $row2) {
                $sout.='<li' . ($id == $row2['id'] ? ' class="active"' : '') . '><i class="fa fa-caret-right fw"></i><a class="menu-link" href="' . U($row2['url']) . '" data-id="' . $row2['id'] . '">' . $row2['title'] . '</a></li>';
            }
            $sout.='</ul>';
        }
        $sout.='</fieldset>';
        echo $sout;
    }

    /**
     * 获取历史菜单记录
     * @return string
     * @since VER:1.0; DATE:2016-3-11; AUTHOR:SoChishun;
     */
    public function get_history_menu() {
        $m_rule = new \XWAM\Model\RuleModel();
        $list = $m_rule->get_history_menus();
        $sout = '';
        foreach ($list as $row) {
            $sout.='<li><a href="' . U($row['url']) . '" class="menu-link" data-id="' . $row['id'] . '">' . $row['title'] . '</a><a href="#" class="close-link" data-id="' . $row['id'] . '"><i class="fa fa-times"></i></a></li>';
        }
        $sout.='<li><a href="#" class="clear-link">[清空列表]</a></li>';
        echo $sout;
    }

}
