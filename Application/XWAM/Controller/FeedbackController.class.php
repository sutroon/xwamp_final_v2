<?php

namespace XWAM\Controller;

/**
 * FeedbackController 类
 *
 * @since VER:1.0; DATE:2016-6-23; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class FeedbackController extends AppbaseController {

    function feedback_list_search() {
        $search = $_GET;
        $where = array();
        if (!empty($search['search_key'])) {
            $where['title'] = array('like', '%' . $search['search_key'] . '%');
        }
        $where['pid'] = 0;
        $where['site_id'] = $this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    function feedback_list() {
        $asearch = $this->feedback_list_search();
        $m_feedback = new \XWAM\Model\FeedbackModel();
        $list = $m_feedback->get_paging_list($page, array('where' => $asearch['where'], 'order' => 'sort, id desc'), array('page_params' => $asearch['search']));
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    function reply_list_search() {
        $search = $_GET;
        $where = array();
        if (!empty($search['search_key'])) {
            $where['title'] = array('like', '%' . $search['search_key'] . '%');
        }
        $where['pid'] = $search['pid'];
        return array('search' => $search, 'where' => $where);
    }

    function reply_list($id = 0, $pid = 0) {
        $asearch = $this->reply_list_search();
        $m_feedback = new \XWAM\Model\FeedbackModel();
        $list = $m_feedback->get_paging_list($page, array('where' => $asearch['where'], 'order' => 'sort, id desc'), array('page_params' => $asearch['search']));
        $data = array();
        if ($id) {
            $data = $m_feedback->find($id);
        }
        if (!$data) {
            $data = array('pid' => $pid, 'user_name' => $this->user_name, 'site_id' => $this->site_id);
        }
        $this->assign('data', $data);
        $this->assign('list', $list);
        $this->assign('page', $page->show());
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    function reply_edit_save() {
        $m_feedback = new \XWAM\Model\FeedbackModel();
        $result = $m_feedback->reply_feedback();
        if($result['status']){
            $this->success('回复成功!');
        }else{
            $this->error($result['info']);
        }
    }

    function change_status($id, $status) {
        $m_feedback = new \XWAM\Model\FeedbackModel();
        $this->ajaxReturn($m_feedback->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_feedback = new \XWAM\Model\FeedbackModel();
        $this->ajaxReturn($m_feedback->change_sort($id, $sort));
    }

    function feedback_delete($id = '') {
        $m_feedback = new \XWAM\Model\FeedbackModel();
        $result = $m_feedback->delete_feedback($id);
        $this->ajaxMsg($result['status'], $result['info']);
    }

}
