<?php

namespace XWAM\TagLib;

/**
 * SiteConf 标签类
 *
 * @since VER:1.0; DATE:2016-6-20; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class SiteConf extends TagBase {

    protected $tags = array(
        'meta_info' => array('attr' => 'attrs', 'level' => 1, 'close' => 0),
    );

    /**
     * 获取Meta值
     * @param type $attr
     * @param type $content
     * @return string
     * @since 1.0 <2016-6-20> SoChishun Added.
     * @example <SiteConf:meta_info name="meta_keywords" />
     */
    function _meta_info($attr, $content) {
        switch ($attr['name']) {
            case 'meta_keywords':
                $field = 'meta_keywords';
                $tpl = '<meta name="keywords" content="%s" />';
                break;
            case 'meta_description':
                $field = 'meta_description';
                $tpl = '<meta name="description" content="%s" />';
                break;
            default:
                return '';
        }
        $content = M('t_porg_site_conf')->getField($field);
        $sout = sprintf($tpl, $content);
        return $sout;
    }

}
