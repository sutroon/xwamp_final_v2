<?php

namespace XWAM\TagLib;

/**
 * Products 类
 *
 * @since VER:1.0; DATE:2016-6-20; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class ProductGoods extends TagBase {

    protected $tags = array(
        'list' => array('attr' => 'attrs', 'level' => 1),
        'paginglist' => array('attr' => 'attrs', 'level' => 1),
        'item' => array('attr' => 'attrs', 'level' => 1),
    );

    /**
     * list标签解析 循环输出数据集
     * @access public
     * @param array $attr 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example path description
     * @since 1.0 <2015-10-27> SoChishun Added.
     */
    public function _list($attr, $content) {
        $options = $this->parseSQLOption($attr);
        $name = '$' . (empty($attr['name']) ? 'res_list' : $attr['name']);
        $item = empty($attr['item']) ? 'vo' : $attr['item'];
        $key = !empty($attr['key']) ? $attr['key'] : 'key';
        $parseStr = '<?php ' . $name . '=M(\'t_porg_product_goods\')->select(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

    /**
     * list标签解析 循环输出数据集
     * @access public
     * @param array $attr 标签属性
     * @param string $content  标签内容
     * @return string|void
     * 
     */
    public function _paginglist($attr, $content) {
        $sqloptions = array('where', 'field', 'limit', 'order', 'table');
        $options = array('TABLE' => 't_porg_product_goods');
        foreach ($sqloptions as $key) {
            if (!empty($attr[$key])) {
                $options[$key] = $attr[$key];
            }
        }
        // 标签不支持<>等html标签,只能用neq,gt,lt代替
        if (!empty($options['where'])) {
            $alias = array('neq' => '<>', 'gt' => '>', 'lt' => '<', 'egt' => '>=', 'elt' => '<=');
            foreach ($alias as $key => $value) {
                $options['where'] = str_replace(" $key ", " $value ", $options['where']);
            }
        }
        $pagination_name = empty($attr['pagination_name']) ? '' : $attr['pagination'];
        $name = '$' . (empty($attr['name']) ? 'ds_document' : $attr['name']);
        $item = empty($attr['item']) ? 'vo' : $attr['item'];
        $key = !empty($attr['key']) ? $attr['key'] : 'key';
        $parseStr = '<?php ' . $name . '=M(\'Common\')->get_paging_list(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php ' . $pagination_name . '="123"; ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

    /**
     * 获取字段值
     * @param type $attr
     * @param type $content
     * @return string
     * @since 1.0 <2015-11-13> SoChishun Added.
     * @example <ProductGoods:item id="id" click_count="true">{$ds_item.title}</ProductGoods:item>
     */
    function _item($attr, $content) {
        $id = $this->tpl->get($attr['id']);
        if (!$id) {
            exit('参数无效!');
        }
        $options = array(
            'where' => array('id' => $id),
            'cache' => empty($attr['cache']) ? 5 : $attr['cache']
        );
        if (!empty($attr['field'])) {
            $options['field'] = $attr['field'];
        }
        $click_count = $attr['click_count']; // 统计访问数量
        if ('true' == $click_count) {
            M('t_porg_product_goods')->where(array('id' => $id))->setInc('visit_count');
        }
        $name = '$' . (empty($attr['name']) ? 'ds_item' : $attr['name']);
        $parseStr = '<?php ' . $name . ' = M(\'t_porg_product_goods\')->find(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')):?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endif; ?>';
        return $parseStr;
    }

}
