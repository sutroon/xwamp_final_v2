<?php

namespace XWAM\TagLib;

/**
 * AdvertisingContent 标签类
 * 用途：轮播插件、Banner广告等
 *
 * @since 1.0 <2015-11-10> SoChishun <14507247@qq.com> Added.
 */
class AdvertisingContent extends TagBase {

    protected $tags = array(
        'list' => array('attr' => 'attrs', 'level' => 1),
    );

    /**
     * list标签解析 循环输出数据集
     * @access public
     * @param array $tag 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example path description
     * @since 1.0 <2015-10-27> SoChishun Added.
     * @example 
     */
    //<taglib name="XWAM\Addon\POrgDocument\TagLib\Document,XWAM\Addon\POrgDocument\TagLib\DocumentCategory,XWAM\Addon\POrgDocument\TagLib\AdvertisingContent,Common\TagLib\DB" />
    //<AdvertisingContent:list where="advertising_id=2" code="SY_JDT_RM1">
    //    <img src="{$vo.file_path}" alt="" />
    //</AdvertisingContent:list>

    public function _list($tag, $content) {
        $options = $this->parseSQLOption($tag);
        if (!empty($tag['code'])) {
            $options['where'] = array('code' => $tag['code']);
        }
        $options = array_merge($options, array('sort' => 'sort,id'));
        $name = '$' . (empty($tag['name']) ? 'res_list' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $parseStr = '<?php ' . $name . '=M(\'t_porg_advertising_content\')->select(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

}
