<?php

namespace XWAM\TagLib;

/**
 * ProductCategory 标签类
 * 用途：商品类别
 *
 * @since 1.0 <2016-6-20> SoChishun <14507247@qq.com> Added.
 */
class ProductCategory extends TagBase {

    protected $tags = array(
        'list' => array('attr' => 'attrs', 'level' => 1),
        'tree' => array('attr' => 'attrs', 'level' => 3),
        'item' => array('attr' => 'attrs', 'level' => 1),
    );

    /**
     * foreach标签解析 循环输出数据集
     * @access public
     * @param array $attr 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example path description
     * @since 1.0 <2015-10-27> SoChishun Added.
     * @example 
     * <ProductCategory:list where="pid=2 and status=1" order="sort, id">
     *      <li><a href="{:U('AboutUs/index','id='.$vo['id'])}">{$vo.title}</a></li>
     * </ProductCategory:list>
     */
    public function _list($attr, $content) {
        $options = $this->parseSQLOption($attr);
        $options = array_merge($options, array('sort' => 'sort, id'));
        $name = '$' . (empty($attr['name']) ? 'res_list' : $attr['name']);
        $item = empty($attr['item']) ? 'vo' : $attr['item'];
        $key = !empty($attr['key']) ? $attr['key'] : 'key';
        $parseStr = '<?php ' . $name . '=M(\'t_porg_product_category\')->select(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

    /**
     * foreach标签解析 循环输出数据集
     * @access public
     * @param array $attr 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example path description
     * @since 1.0 <2015-10-27> SoChishun Added.
     * @example 
     * 
      <ProductCategory:tree where='{"pid":36,"status":1}' order="sort,id desc" field="id, title, type_id">
      <li>
      <if condition="1 eq $vo['type_id']">
      <a href="{:U('Services/services_info','id='.$vo['id'])}">{$vo.title}</a>
      <else/>
      <a href="{:U('Services/index','id='.$vo['id'])}">{$vo.title}</a>
      </if>
      </li>
      </ProductCategory:tree>
     */
    public function _tree($attr, $content) {
        $options = $this->parseSQLOption($attr);
        // where内容只能是json_encode()处理过的字符串,如where='{"pid":20}'
        if (!is_array($options['where'])) {
            exit('[TREE]的where条件格式无效!'); // 一定要传入where='{"pid":0,"status":1}'
        }
        $name = '$' . (empty($attr['name']) ? 'res_tree' : $attr['name']);
        $item = empty($attr['item']) ? 'vo' : $attr['item'];
        $key = !empty($attr['key']) ? $attr['key'] : 'key';
        $parseStr = '<?php ' . $name . '=D(\'XWAM/ProductCategory\')->select_tree(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

    /**
     * 获取字段值
     * @param type $attr
     * @param type $content
     * @return string
     * @since 1.0 <2015-11-12> SoChishun Added.
     * @example <ProductCategory:item pk="$Think.Get.id??3" field="title" extract="true">{$vo.title}</ProductCategory:item>{title}
     */
    function _item($attr, $content) {
        $pk = $this->parseIdExp($attr['pk']);
        $options = array('limit' => 1);
        if (!empty($attr['field'])) {
            $options['field'] = $attr['field'];
        }
        $options['cache'] = empty($attr['cache']) ? 5 : $attr['cache'];
        if ($pk) {
            $options['where'] = array('id' => '{#id}');
        }
        $soptions = '';
        if ($options) {
            $soptions = var_export($options, true);
            if ($pk) {
                $soptions = str_replace('\'{#id}\'', $pk, $soptions);
            }
        }
        $name = '$' . (empty($attr['name']) ? 'res_item' : $attr['name']);
        $item = empty($attr['item']) ? 'vo' : $attr['item'];
        $key = !empty($attr['key']) ? $attr['key'] : 'key';
        $extract = !empty($attr['extract']);
        $parseStr = '<?php ' . $name . ' = M(\'t_porg_product_category\')->select(' . $soptions . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';
        if ($extract) {
            $parseStr .= '<?php if(is_array(' . $name . ')): extract(' . $name . '[0]); endif; ?>';
        }

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

}
