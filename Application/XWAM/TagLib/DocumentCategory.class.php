<?php

namespace XWAM\TagLib;

/**
 * DocumentCategory 标签类
 * 用途：文章分类、菜单栏目
 *
 * @since 1.0 <2015-10-27> SoChishun <14507247@qq.com> Added.
 */
class DocumentCategory extends TagBase {

    protected $tags = array(
        'list' => array('attr' => 'attrs', 'level' => 1),
        'tree' => array('attr' => 'attrs', 'level' => 3),
        'item' => array('attr' => 'attrs', 'level' => 1),
    );

    /**
     * foreach标签解析 循环输出数据集
     * @access public
     * @param array $tag 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example path description
     * @since 1.0 <2015-10-27> SoChishun Added.
     * @example 
     * <DocumentCategory:list where="pid=2 and status=1" order="sort, id">
     *      <li><a href="{:U('AboutUs/index','id='.$vo['id'])}">{$vo.title}</a></li>
     * </DocumentCategory:list>
     */
    public function _list($tag, $content) {
        $options = $this->parseSQLOption($tag);
        $options = array_merge($options, array('sort' => 'sort, id'));
        $name = '$' . (empty($tag['name']) ? 'res_list' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $parseStr = '<?php ' . $name . '=M(\'t_porg_document_category\')->select(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

    /**
     * foreach标签解析 循环输出数据集
     * @access public
     * @param array $tag 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example path description
     * @since 1.0 <2015-10-27> SoChishun Added.
     * @example 
     * 
      <DocumentCategory:tree where='{"pid":36,"status":1}' order="sort,id desc" field="id, title, type_id">
      <li>
      <if condition="1 eq $vo['type_id']">
      <a href="{:U('Services/services_info','id='.$vo['id'])}">{$vo.title}</a>
      <else/>
      <a href="{:U('Services/index','id='.$vo['id'])}">{$vo.title}</a>
      </if>
      </li>
      </DocumentCategory:tree>
     */
    public function _tree($tag, $content) {
        $options = $this->parseSQLOption($tag);
        // where内容只能是json_encode()处理过的字符串,如where='{"pid":20}'
        if (!is_array($options['where'])) {
            throw new Exception('[TREE]的where条件格式无效!');
        }
        $name = '$' . (empty($tag['name']) ? 'res_tree' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $parseStr = '<?php ' . $name . '=D(\'XWAM/DocumentCategory\')->select_tree(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

    /**
     * 获取字段值
     * @param type $tag
     * @param type $content
     * @return string
     * @since 1.0 <2015-11-12> SoChishun Added.
     * @example <DocumentCategory:item pk="$Think.Get.id??3" field="title" extract="true">{$vo.title}</DocumentCategory:item>{title}
     */
    function _item($tag, $content) {
        $pk = $tag['pk'];
        if (strpos($pk, '??')) {
            $apk = explode('??', $pk);
            $pk = $apk[0];
            if (false !== strpos($pk, '$Think')) {
                $pk = $this->parseThinkVar($pk);
            }
            $pk = '(isset(' . $pk . ') ? ' . $pk . ' : ' . $apk[1] . ')';
        } else {
            if (false !== strpos($pk, '$Think')) {
                $pk = $this->parseThinkVar($pk);
            }
        }
        $options = array('limit' => 1);
        if (!empty($tag['field'])) {
            $options['field'] = $tag['field'];
        }
        $options['cache'] = empty($tag['cache']) ? 5 : $tag['cache'];
        if ($pk) {
            $options['where'] = array('id' => '{#id}');
        }
        $soptions = '';
        if ($options) {
            $soptions = var_export($options, true);
            if ($pk) {
                $soptions = str_replace('\'{#id}\'', $pk, $soptions);
            }
        }
        $name = '$' . (empty($tag['name']) ? 'res_item' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $extract = !empty($tag['extract']);
        $parseStr = '<?php ' . $name . ' = M(\'t_porg_document_category\')->select(' . $soptions . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';
        if ($extract) {
            $parseStr .= '<?php if(is_array(' . $name . ')): extract(' . $name . '[0]); endif; ?>';
        }

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

}
