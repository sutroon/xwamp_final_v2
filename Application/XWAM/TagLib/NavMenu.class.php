<?php

namespace XWAM\TagLib;

/**
 * NavMenu 标签类
 *
 * @since 1.0 <2016-6-20> SoChishun <14507247@qq.com> Added.
 */
class NavMenu extends TagBase {

    protected $tags = array(
        'list' => array('attr' => 'attrs', 'level' => 1),
        'tree' => array('attr' => 'attrs', 'level' => 3),
    );

    /**
     * foreach标签解析 循环输出数据集
     * @access public
     * @param array $attr 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example path description
     * @since 1.0 <2015-10-27> SoChishun Added.
     * @example 
     * <NavMenu:list where="pid=2 and status=1" order="sort, id">
     *      <li><a href="{:U('AboutUs/index','id='.$vo['id'])}">{$vo.title}</a></li>
     * </NavMenu:list>
     * <NavMenu:list code="aboutus">
     *      <li><a href="{:U($vo['link_url'])}">{$vo.title}</a></li>
     * </NavMenu:list>
     */
    public function _list($attr, $content) {
        $options = $this->parseSQLOption($attr);
        if (!empty($attr['code'])) {
            $pid = M('t_porg_document_menu')->where(array('code' => $attr['code']))->limit(1)->getField('id');
            $options['where']['pid'] = $pid;
        }
        $options = array_merge($options, array('sort' => 'sort, id'));
        $name = '$' . (empty($attr['name']) ? 'res_list' : $attr['name']);
        $item = empty($attr['item']) ? 'vo' : $attr['item'];
        $key = !empty($attr['key']) ? $attr['key'] : 'key';
        $parseStr = '<?php ' . $name . '=M(\'t_porg_document_menu\')->select(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';
        return $parseStr;
    }

    /**
     * foreach标签解析 循环输出数据集
     * @access public
     * @param array $attr 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example <NavMenu:tree item="vo"></NavMenu:tree>
     * @since 1.0 <2015-10-27> SoChishun Added.
     * @example 
     *  <NavMenu:tree code="footnav">
     *  <a href="{:U($vo['link_url'])}">{$vo.title}</a>&nbsp;&nbsp;
     *  </NavMenu:tree>
     */
    public function _tree($attr, $content) {
        if (empty($attr['code'])) {
            return false;
        }
        $pid = D('XWAM/DocumentMenu')->where(array('code' => $attr['code']))->getField('id');
        $options = array('where' => array('pid' => $pid));
        $name = '$' . (empty($attr['name']) ? 'res_tree' : $attr['name']);
        $item = empty($attr['item']) ? 'vo' : $attr['item'];
        $key = !empty($attr['key']) ? $attr['key'] : 'key';
        $parseStr = '<?php ' . $name . '=D(\'XWAM/DocumentMenu\')->select_tree(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';
        return $parseStr;
    }

}
