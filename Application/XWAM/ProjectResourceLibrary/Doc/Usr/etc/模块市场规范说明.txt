[2015-9-16]
modules.php：全局模块主配置文件，由程序控制，内容是经过序列化的字符串.
数据原型：
/**
 * 模块总配置文件
 * @since 1.0 <2015-9-16> SoChishun Added.
 */
return array(
    /* 平台框架基本url设置 */
    'base_url'=>array(
        'login_url'=>'', // 登录地址
        'logout_url'=>'', // 注销地址
    ),
);

模块安装目录：
/usr
  /etc
  /tmp
  /local