-- 用户视图 2014-12-29 by sutroon
create or replace view vuser as
	select 
            u.id, u.userName, u.password, u.tradePassword, u.personalName, u.sex, u.faceUrl, u.maritalStatus, u.birthday, u.idTypeID, u.idNo, u.telphone, u.email, u.contacts, u.address, u.registIP, u.loginIP, u.loginCount, u.updatedTime, u.roleID, u.permissionRule, u.personalSign, u.customStatus, u.remark, u.userType, u.onlineState, u.state, u.createdTime, u.siteID,
            r.roleName
	from tuser u left join tuser_role r on u.roleID=r.id;

-- 商品进销存日志视图 2014-12-23 by sutroon
create or replace view vproduct_invoicing as
    select 
        i.id, i.productID, i.catalogID, i.isWarehouseProduct, i.orderID, i.`number`, i.price, i.subtotal, i.oldStock, i.stock, i.remark, i.providerID, i.logisticsID, i.operateType, i.userID, i.adminID, i.createdTime, i.siteID,
        p.productName 
    from tproduct_invoicing i left join tproduct_goods p on i.productID=p.id 
    order by i.id desc;

-- 客户表视图 2014-12-25 by sutroon
create or replace view vcustomer as
    select 
        c.id, c.serialNo, c.loginName, c.personalName, c.sex, c.faceUrl, c.maritalStatus, c.birthday, c.idType, c.idNo, c.telphone, c.address, c.interested, c.tags, c.level, c.buyCount, c.integral, c.cumulativeAmount, c.lastBuyTime, c.lastVisitTime, c.userID, c.oldUserID, c.remark, c.state, c.createdTime, c.siteID,
        u.userName, u.personalName as agentPersonalName
    from tcustomer c left join tuser u on c.userID=u.id
    order by c.id desc;

-- 客户-预约回访表视图 2014-12-25 by sutroon
create or replace view vcustomer_appointment as
    select 
        a.id, a.customerID, a.`time`, a.remark, a.`result`, a.`source`, a.`state`, a.updatedTime, a.createdTime, 
        c.siteID, c.serialNo, c.personalName, c.telphone, c.address, c.buyCount, c.cumulativeAmount, c.lastBuyTime, c.userID, c.oldUserID, 
        u.userName, u.personalName as agentPersonalName
    from tcustomer_appointment a left join tcustomer c on a.customerID=c.id left join tuser u on c.userID=u.id
    order by a.id desc;

-- 客户-调度客户日志表视图 2014-12-26 by sutroon
create or replace view vcustomer_dispatch as
    select 
        d.id, d.name, d.customerID, d.oldUserID, d.userID, d.adminID, d.remark, d.state, d.createdTime,
        u.userName, u.personalName as agentPersonalName, uold.userName as oldUserName, uadm.userName as adminName
    from tcustomer_dispatch d left join tuser u on d.userID=u.id left join tuser uold on d.oldUserID=uold.id left join tuser uadm on d.adminID=uadm.id
    order by id desc;



-- =============================================
-- Author:      sutroon
-- Create date: 2015-1-9
-- Description: 订单视图创建语句
-- =============================================
-- 订单表视图 2015-1-9 by sutroon
create or replace view vorder as
	select
	o.id, o.serialNo, o.consigneeName, o.consigneeTelphone, o.consigneeAddress, o.consigneeZip, o.consigneeEmail, o.summary, o.weight, o.itemCount, o.productAmount, o.orderAmount, o.deliveryFee, o.returnedFee, o.agencyFee, o.deliveryAmount, o.orderSettlementState, o.orderSettlementTime, o.orderPaymentID, o.deliverySettlementState, o.deliverySettlementTime, o.deliveryFeePaymentID, o.reconciliationState, o.reconciliationDate, o.escrowTradeNo, o.invoiceNeed, o.invoiceState, o.isSellerRiskConfirm, o.isSellerReturnedFine, o.isCommission, o.commissionRate, o.commissionAmount, o.buyerRemark, o.sellerRemark, o.confirmRemark, o.storekeeperRemark, o.logisticsRemark, o.commissionRemark, o.sellerID, o.buyerID, o.confirmID, o.cashierID, o.accountantID, o.adminID, o.oldSellerID, o.orderSource, o.exTags, o.state, o.confirmTime, o.createdTime, o.siteID,
        u.userName as sellerUserName, u.personalName as sellerPersonalName,
        c.serialNo as consigneeSerialNo
	from torder o left join tuser u on o.sellerID=u.id left join tcustomer c on o.buyerID=c.id;

-- 订单-物流表视图 2015-1-9 by sutroon
create or replace view vorder_logistics as
	select
	l.expressNo, l.logisticsID, l.logisticsQueryer, l.courierPhone, l.courierPhoneDisplayReson, l.courierPhoneCreatedTime, l.unpackingInspectionEnable, l.logisticsDetail, l.logisticsResult, l.logisticsInfoRemark, l.logisticsState, l.logisticsUpdateTime, l.logisticsLastTime, l.logisticsTime,
        o.id, o.serialNo, o.consigneeName, o.consigneeTelphone, o.consigneeAddress, o.consigneeZip, o.consigneeEmail, o.summary, o.productAmount, o.orderAmount, o.deliveryFee, o.returnedFee, o.agencyFee, o.deliveryAmount, o.buyerRemark, o.sellerRemark, o.confirmRemark, o.storekeeperRemark, o.logisticsRemark, o.sellerID, o.buyerID, o.confirmID, o.state, o.confirmTime, o.createdTime, o.siteID
	from torder_logistics l left join torder o on l.orderID=o.id;

-- 订单-退货表视图 2015-1-9 by sutroon
create or replace view vorder_returns as
	select
	r.orderID, r.serialNo as returnedNo, r.sellerReturnedFee, r.handling, r.handlingFee, r.handlingTime, r.returnedTime,
        o.id, o.serialNo, o.consigneeName, o.consigneeTelphone, o.consigneeAddress, o.consigneeZip, o.consigneeEmail, o.summary, o.productAmount, o.orderAmount, o.deliveryFee, o.returnedFee, o.agencyFee, o.deliveryAmount, o.buyerRemark, o.sellerRemark, o.confirmRemark, o.storekeeperRemark, o.logisticsRemark, o.sellerID, o.buyerID, o.confirmID, o.state, o.confirmTime, o.createdTime, o.siteID
	from torder_returns r left join torder o on r.orderID=o.id;
