-- 用户视图 2014-12-29 by sutroon
create or replace view vuser as
	select 
            u.id, u.userName, u.password, u.tradePassword, u.personalName, u.sex, u.faceUrl, u.maritalStatus, u.birthday, u.idTypeID, u.idNo, u.telphone, u.email, u.contacts, u.address, u.registIP, u.loginIP, u.loginCount, u.updatedTime, u.roleID, u.permissionRule, u.personalSign, u.customStatus, u.remark, u.userType, u.onlineState, u.state, u.createdTime, u.siteID,
            r.roleName
	from tuser u left join tuser_role r on u.roleID=r.id;
