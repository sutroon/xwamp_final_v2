-- 订单-物流表视图 2015-1-9 by sutroon
create or replace view vorder_logistics as
	select
	l.expressNo, l.logisticsID, l.logisticsQueryer, l.courierPhone, l.courierPhoneDisplayReson, l.courierPhoneCreatedTime, l.unpackingInspectionEnable, l.logisticsDetail, l.logisticsResult, l.logisticsInfoRemark, l.logisticsState, l.logisticsUpdateTime, l.logisticsLastTime, l.logisticsTime,
        o.id, o.serialNo, o.consigneeName, o.consigneeTelphone, o.consigneeAddress, o.consigneeZip, o.consigneeEmail, o.summary, o.productAmount, o.orderAmount, o.deliveryFee, o.returnedFee, o.agencyFee, o.deliveryAmount, o.buyerRemark, o.sellerRemark, o.confirmRemark, o.storekeeperRemark, o.logisticsRemark, o.sellerID, o.buyerID, o.confirmID, o.state, o.confirmTime, o.createdTime, o.siteID
	from torder_logistics l left join torder o on l.orderID=o.id;
