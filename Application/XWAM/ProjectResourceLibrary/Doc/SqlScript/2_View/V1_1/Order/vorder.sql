-- 订单表视图 2015-1-9 by sutroon
create or replace view vorder as
	select
	o.id, o.serialNo, o.consigneeName, o.consigneeTelphone, o.consigneeAddress, o.consigneeZip, o.consigneeEmail, o.summary, o.weight, o.itemCount, o.productAmount, o.orderAmount, o.deliveryFee, o.returnedFee, o.agencyFee, o.deliveryAmount, o.orderSettlementState, o.orderSettlementTime, o.orderPaymentID, o.deliverySettlementState, o.deliverySettlementTime, o.deliveryFeePaymentID, o.reconciliationState, o.reconciliationDate, o.escrowTradeNo, o.invoiceNeed, o.invoiceState, o.isSellerRiskConfirm, o.isSellerReturnedFine, o.isCommission, o.commissionRate, o.commissionAmount, o.buyerRemark, o.sellerRemark, o.confirmRemark, o.storekeeperRemark, o.logisticsRemark, o.commissionRemark, o.sellerID, o.buyerID, o.confirmID, o.cashierID, o.accountantID, o.adminID, o.oldSellerID, o.orderSource, o.exTags, o.state, o.confirmTime, o.createdTime, o.siteID,
        u.userName as sellerUserName, u.personalName as sellerPersonalName,
        c.serialNo as consigneeSerialNo
	from torder o left join tuser u on o.sellerID=u.id left join tcustomer c on o.buyerID=c.id;
