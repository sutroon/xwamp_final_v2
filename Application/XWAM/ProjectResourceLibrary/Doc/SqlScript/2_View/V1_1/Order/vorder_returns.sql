-- 订单-退货表视图 2015-1-9 by sutroon
create or replace view vorder_returns as
	select
	r.orderID, r.serialNo as returnedNo, r.sellerReturnedFee, r.handling, r.handlingFee, r.handlingTime, r.returnedTime,
        o.id, o.serialNo, o.consigneeName, o.consigneeTelphone, o.consigneeAddress, o.consigneeZip, o.consigneeEmail, o.summary, o.productAmount, o.orderAmount, o.deliveryFee, o.returnedFee, o.agencyFee, o.deliveryAmount, o.buyerRemark, o.sellerRemark, o.confirmRemark, o.storekeeperRemark, o.logisticsRemark, o.sellerID, o.buyerID, o.confirmID, o.state, o.confirmTime, o.createdTime, o.siteID
	from torder_returns r left join torder o on r.orderID=o.id;
