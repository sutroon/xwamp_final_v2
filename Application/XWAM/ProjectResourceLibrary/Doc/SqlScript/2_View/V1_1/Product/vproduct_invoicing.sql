-- 商品进销存日志视图 2014-12-23 by sutroon
create or replace view vproduct_invoicing as
    select 
        i.id, i.productID, i.catalogID, i.isWarehouseProduct, i.orderID, i.`number`, i.price, i.subtotal, i.oldStock, i.stock, i.remark, i.providerID, i.logisticsID, i.operateType, i.userID, i.adminID, i.createdTime, i.siteID,
        p.productName 
    from tproduct_invoicing i left join tproduct_goods p on i.productID=p.id 
    order by i.id desc;
