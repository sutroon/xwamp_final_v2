/**
* 在线充值审核
* @since 1.0 2014-9-17 by sutroon
 */
DELIMITER ;;
drop procedure if exists proc_recharge_confirm;;
create procedure proc_recharge_confirm(pstr_orderNo varchar(128),pstr_tradeNo varchar(128), pint_state int)
begin
    declare vflt_blance decimal(12,4) default 0;
    declare vflt_allowOverdraftAmount decimal(12,4);
    declare vint_daybill_id int;
    declare vstr_userName varchar(32);
    declare pint_siteID int;
    declare pflt_amount decimal(12,4);
    declare vint_state int;
    declare vint_errno smallint default 0;
    declare continue handler for sqlexception set vint_errno = 4000;
    start transaction;
    -- 1=确认收货,交易完成; 2=已付款,未发货; 3=已发货,未签收;

    select siteID, amount, state into pint_siteID, pflt_amount, vint_state from tfinance_recharge where orderNo=pstr_orderNo;
    if isnull(vint_state) then
        set vint_errno=4400; -- 记录不存在
    end if;
    if vint_state=1 or vint_state=pint_state then
        set vint_errno=4200; -- 重复确认
    end if;
    if pint_state=1 then -- 确认收货,交易完成
        select userName, blance, allowOverdraftAmount into vstr_userName, vflt_blance, vflt_allowOverdraftAmount from tuser_member where userType='ADMIN' and siteID=pint_siteID;
        set vflt_blance=vflt_blance+pflt_amount;
        update tuser_member set blance=vflt_blance where userType='ADMIN' and siteID=pint_siteID;
        update tfinance_recharge set state=pint_state, userName=vstr_userName, blance=vflt_blance where orderNo=pstr_orderNo;
        if exists(select id from tcti_daybill where billdate=CURDATE() and siteID=pint_siteID) then
            update tcti_daybill set rechargeAmount=rechargeAmount+pflt_amount,blance=blance+pflt_amount where billdate=CURDATE() and siteID=pint_siteID;
        else
            insert into tcti_daybill (billdate,rechargeAmount,blance, allowOverdraftAmount,createdTime,siteID) values (CURDATE(),pflt_amount,vflt_blance, vflt_allowOverdraftAmount,now(),pint_siteID);
        end if;
    else
        update tfinance_recharge set state=pint_state where orderNo=pstr_orderNo; -- 标记状态
    end if;

    if vint_errno <> 0 then
        rollback;
    else        
        commit;
    end if;
    SELECT vint_errno;
end;;
DELIMITER ;
