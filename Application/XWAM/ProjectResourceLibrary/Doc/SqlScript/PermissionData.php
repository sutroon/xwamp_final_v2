<?php

/*
 * 权限数据
 * <br />支持字段：[string]name:名称,[string]code:代码,[boolean]ismenu:是否显示为菜单,[boolean]enable:是否可用,[string]url:链接地址,[array]sub:子项目,[string]comment:注解
 * <br />说明：默认ismenu=true,如要特殊声明，需强制加上ismenu=false
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @remark 获取原来管理员权限编号的sql语句：select concat(group_concat(id SEPARATOR ','),'') as ids from tuser_permission where name in ('系统管理', '系统管理');
 */
return array(
    array(
        'name' => '系统管理',
        'code' => 'M1_XT',
        'sub' => array(
            array('name' => '网站设置', 'code' => 'XT_M2_WZSZ', 'sub' => array(
                    array('name' => '网站信息', 'code' => 'XT_WZSZ_WZXX', 'url' => '/index.php/XWAM/Setting/setting_edit.html'),
                )
            ),
            array(
                'name' => '留言管理',
                'code' => 'XT_M2_LYGL',
                'sub' => array(
                    array('name' => '留言管理', 'code' => 'XT_M3_LYGL', 'url' => '/index.php/XWAM/Feedback/feedback_list.html', 'sub' => array(
                            array('name' => '审核留言', 'code' => 'XT_LYGL_SHLY', 'ismenu' => false),
                        )
                    ),
                )
            ),
            array(
                'name' => '站内信',
                'code' => 'XT_M2_ZNX',
                'sub' => array(
                    array('name' => '站内信管理', 'code' => 'XT_M3_ZNXGL', 'url' => '/index.php/XWAM/PM/pm_list.html', 'sub' => array(
                            array('name' => '发送站内信', 'code' => 'XT_ZNXGL_FSZNX', 'ismenu' => false),
                            array('name' => '删除站内信', 'code' => 'XT_ZNXGL_SCZNX', 'ismenu' => false),
                            array('name' => '导出站内信', 'code' => 'XT_ZNXGL_DCZNX', 'ismenu' => false),
                        )
                    ),
                )
            ),
            array(
                'name' => '备份恢复',
                'code' => 'XT_M2_BFHF',
                'sub' => array(
                    array('name' => '数据备份', 'code' => 'XT_M3_SJBF', 'url' => '/index.php/XWAM/SysTool/data_backup.html'),
                    array('name' => '数据恢复', 'code' => 'XT_M3_SJHF', 'url' => '/index.php/XWAM/SysTool/data_backup.html'),
                    array('name' => '数据清理', 'code' => 'XT_M3_SJQL', 'url' => '/index.php/XWAM/SysTool/data_clear.html'),
                )
            )
        )
    ),
    array(
        'name' => '用户管理',
        'code' => 'M1_YH',
        'sub' => array(
            array(
                'name' => '用户管理',
                'code' => 'YH_M2_YHGL',
                'sub' => array(
                    array('name' => '管理员管理', 'code' => 'YH_M3_GLYGL', 'url' => '/index.php/XWAM/Admin/admin_list.html'),
                    array('name' => '会员管理', 'code' => 'YH_M3_HYGL', 'url' => '/index.php/XWAM/Member/member_list.html', 'sub' => array(
                            array('name' => '新增会员', 'code' => 'YH_HYGL_XZHY', 'ismenu' => false),
                            array('name' => '编辑会员', 'code' => 'YH_HYGL_BJHY', 'ismenu' => false),
                            array('name' => '删除会员', 'code' => 'YH_HYGL_SCHY', 'ismenu' => false),
                            array('name' => '编辑会员权限', 'code' => 'YH_HYGL_BJHYQX', 'ismenu' => false),
                            array('name' => '导出会员数据', 'code' => 'YH_HYGL_DCHYSJ', 'ismenu' => false),
                        )
                    ),
                )
            ),
            array(
                'name' => '角色管理',
                'code' => 'YH_M2_JSGL',
                'sub' => array(
                    array('name' => '角色管理', 'code' => 'YH_M3_JSGL', 'url' => '/index.php/XWAM/Role/role_list.html', 'sub' => array(
                            array('name' => '新增角色', 'code' => 'YH_JSGL_XZJS', 'ismenu' => false),
                            array('name' => '编辑角色', 'code' => 'YH_JSGL_BJJS', 'ismenu' => false),
                            array('name' => '删除角色', 'code' => 'YH_JSGL_SCJS', 'ismenu' => false),
                            array('name' => '编辑权限', 'code' => 'YH_JSGL_BJQX', 'ismenu' => false),
                        )
                    ),
                )
            ),
            array('name' => '部门管理',
                'code' => 'YH_M2_BMGL',
                'sub' => array(
                    array('name' => '部门管理', 'code' => 'YH_M3_BMGL', 'url' => '/index.php/XWAM/Department/department_list.html', 'sub' => array(
                            array('name' => '新增部门', 'code' => 'YH_BMGL_XZBM', 'ismenu' => false),
                            array('name' => '编辑部门', 'code' => 'YH_BMGL_BJBM', 'ismenu' => false),
                            array('name' => '删除部门', 'code' => 'YH_BMGL_SCBM', 'ismenu' => false),
                        )
                    ),
                )
            ),
        )
    ),
    array(
        'name' => '文章管理',
        'code' => 'M1_WZ',
        'sub' => array(
            array(
                'name' => '公告管理',
                'code' => 'WZ_M2_GGGL',
                'sub' => array(
                    array('name' => '公告管理', 'code' => 'WZ_M3_GGGL', 'url' => '/index.php/XWAM/Notice/notice_list.html', 'sub' => array(
                            array('name' => '新增公告', 'code' => 'WZ_GGGL_XZGG', 'ismenu' => false),
                            array('name' => '编辑公告', 'code' => 'WZ_GGGL_BJGG', 'ismenu' => false),
                            array('name' => '删除公告', 'code' => 'WZ_GGGL_SCGG', 'ismenu' => false),
                        )),
                    array('name' => '新闻管理', 'code' => 'WZ_M3_XWGL', 'url' => '/index.php/XWAM/News/news_list.html', 'sub' => array(
                            array('name' => '新增新闻', 'code' => 'WZ_XWGL_XZXW', 'ismenu' => false),
                            array('name' => '编辑新闻', 'code' => 'WZ_XWGL_BJXW', 'ismenu' => false),
                            array('name' => '删除新闻', 'code' => 'WZ_XWGL_SCXW', 'ismenu' => false),
                        )),
                    array('name' => '新闻类别管理', 'code' => 'WZ_M3_XWLBGL', 'url' => '/index.php/XWAM/NewsCategory/category_list.html', 'sub' => array(
                            array('name' => '新增类别', 'code' => 'WZ_XWLBGL_XZLB', 'ismenu' => false),
                            array('name' => '编辑类别', 'code' => 'WZ_XWLBGL_BJLB', 'ismenu' => false),
                            array('name' => '删除类别', 'code' => 'WZ_XWLBGL_SCLB', 'ismenu' => false),
                        )),
                )
            )
        )
    ),
    array(
        'name' => '客户管理',
        'code' => 'M1_KH',
        'sub' => array(
            array(
                'name' => '客户管理',
                'code' => 'KH_M2_KHGL',
                'sub' => array(
                    array('name' => '客户管理', 'code' => 'KH_M3_KHGL', 'url' => '/index.php/XWAM/Customer/customer_list.html', 'sub' => array(
                            array('name' => '新增客户', 'code' => 'KH_KHGL_XZKH', 'ismenu' => false),
                            array('name' => '编辑客户', 'code' => 'KH_KHGL_BJKH', 'ismenu' => false),
                            array('name' => '删除客户', 'code' => 'KH_KHGL_SCKH', 'ismenu' => false),
                            array('name' => '调度客户', 'code' => 'KH_KHGL_DDKH', 'ismenu' => false),
                            array('name' => '预约客户', 'code' => 'KH_KHGL_YYKH', 'ismenu' => false),
                            array('name' => '导出客户数据', 'code' => 'KH_KHGL_DCKHSJ', 'ismenu' => false),
                        )),
                )
            ),
            array('name' => '字段管理', 'code' => 'KH_M3_ZDGL', 'sub' => array(
                    array('name' => '字段管理', 'code' => 'KH_ZDGL_ZDGL', 'url' => '/index.php/XWAM/CustomerExfield/index.html'),
                )),
        )
    ),
    array(
        'name' => '订单管理',
        'code' => 'M1_DD',
        'sub' => array(
            array(
                'name' => '订单管理',
                'code' => 'DD_M2_DDGL',
                'sub' => array(
                    array('name' => '订单管理', 'code' => 'DD_M3_DDGL', 'url' => '/index.php/XWAM/Order/order_list.html', 'sub' => array(
                            array('name' => '新增订单', 'code' => 'DD_DDGL_XZDD', 'ismenu' => false),
                            array('name' => '编辑订单', 'code' => 'DD_DDGL_BJDD', 'ismenu' => false),
                            array('name' => '删除订单', 'code' => 'DD_DDGL_SCDD', 'ismenu' => false),
                            array('name' => '取消订单', 'code' => 'DD_DDGL_QXDD', 'ismenu' => false),
                            array('name' => '审核订单', 'code' => 'DD_DDGL_SHDD', 'ismenu' => false),
                            array('name' => '仓库发货', 'code' => 'DD_DDGL_CKFH', 'ismenu' => false),
                            array('name' => '订单结算', 'code' => 'DD_DDGL_DDJS', 'ismenu' => false),
                            array('name' => '导出订单数据', 'code' => 'DD_DDGL_DCDDSJ', 'ismenu' => false),
                        )),
                )
            ),
        )
    ),
    array(
        'name' => '商品管理',
        'code' => 'M1_SP',
        'sub' => array(
            array(
                'name' => '商品管理',
                'code' => 'SP_M2_SPGL',
                'sub' => array(
                    array('name' => '商品管理', 'code' => 'SP_M3_SPGL', 'url' => '/index.php/XWAM/Product/product_list.html', 'sub' => array(
                            array('name' => '新增商品', 'code' => 'SP_SPGL_XZSP', 'ismenu' => false),
                            array('name' => '编辑商品', 'code' => 'SP_SPGL_BJSP', 'ismenu' => false),
                            array('name' => '删除商品', 'code' => 'SP_SPGL_SCSP', 'ismenu' => false),
                            array('name' => '盘点调仓', 'code' => 'SP_SPGL_PDTC', 'ismenu' => false),
                            array('name' => '导出商品数据', 'code' => 'SP_SPGL_DRSPSJ', 'ismenu' => false),
                            array('name' => '导入商品数据', 'code' => 'SP_SPGL_DCSPSJ', 'ismenu' => false),
                        )),
                    array('name' => '商品类别管理', 'code' => 'SP_M3_SPLBGL', 'url' => '/index.php/XWAM/ProductCategory/category_list.html', 'sub' => array(
                            array('name' => '新增商品类别', 'code' => 'SP_SPLBGL_XZSPFL', 'ismenu' => false),
                            array('name' => '编辑商品类别', 'code' => 'SP_SPLBGL_BJSPLB', 'ismenu' => false),
                            array('name' => '删除商品类别', 'code' => 'SP_SPLBGL_SCSPLB', 'ismenu' => false),
                        )),
                )
            ),
            array(
                'name' => '商品评论管理',
                'code' => 'SP_M2_SPPLGL',
                'sub' => array(
                    array('name' => '商品评论管理', 'code' => 'SP_M3_SPPLGL', 'url' => '/index.php/XWAM/ProductReviews/reviews_list.html'),
                )
            ),
            array(
                'name' => '进销存管理',
                'code' => 'SP_M2_JXCMK',
                'sub' => array(
                    array('name' => '出库管理', 'code' => 'SP_M3_CKGL', 'url' => '/index.php/XWAM/ProductStockout/stockout_list.html', 'sub' => array(
                            array('name' => '申请内部出库', 'code' => 'SP_CKGL_SQNBCK', 'ismenu' => false),
                            array('name' => '审核内部出库', 'code' => 'SP_CKGL_SHNBCK', 'ismenu' => false),
                            array('name' => '申请采购退货', 'code' => 'SP_CKGL_SQCGTH', 'ismenu' => false),
                            array('name' => '审核采购退货', 'code' => 'SP_CKGL_SHCGTH', 'ismenu' => false),
                            array('name' => '编辑出库资料', 'code' => 'SP_CKGL_BJCKZL', 'ismenu' => false),
                            array('name' => '导出出库数据', 'code' => 'SP_CKGL_DCCKSJ', 'ismenu' => false),
                        )),
                    array('name' => '入库管理', 'code' => 'SP_M3_RKGL', 'url' => '/index.php/XWAM/ProductStockin/stockin_list.html', 'sub' => array(
                            array('name' => '退货入库', 'code' => 'SP_RKGL_THRK', 'ismenu' => false),
                            array('name' => '申请采购入库', 'code' => 'SP_RKGL_SQCGRK', 'ismenu' => false),
                            array('name' => '审核采购入库', 'code' => 'SP_RKGL_SHCGRK', 'ismenu' => false),
                            array('name' => '编辑入库资料', 'code' => 'SP_RKGL_BJRKZL', 'ismenu' => false),
                            array('name' => '导出入库数据', 'code' => 'SP_RKGL_DCRKSJ', 'ismenu' => false),
                        )),
                    array('name' => '供应商管理', 'code' => 'SP_M3_GYSGL', 'url' => '/index.php/XWAM/ProductSupplier/supplier_list.html', 'sub' => array(
                            array('name' => '新增供应商', 'code' => 'SP_GYSGL_XZGYS', 'ismenu' => false),
                            array('name' => '编辑供应商', 'code' => 'SP_GYSGL_BJGYS', 'ismenu' => false),
                            array('name' => '删除供应商', 'code' => 'SP_GYSGL_SCGYS', 'ismenu' => false),
                            array('name' => '导出供应商数据', 'code' => 'SP_GYSGL_DCGYSSJ', 'ismenu' => false),
                        )),
                )
            ),
        )
    ),
    array(
        'name' => '全局管理',
        'code' => 'M1_QJ',
        'ismenu' => false,
        'sub' => array(
            array(
                'name' => '通用设置',
                'code' => 'QJ_M2_TYSZ',
                'ismenu' => false,
                'sub' => array(
                    array('name' => '访问范围', 'code' => 'QJ_M3_FWFW', 'ismenu' => false, 'sub' => array(
                            array('name' => '访问前台', 'code' => 'QJ_FWFW_FWQT', 'ismenu' => false),
                            array('name' => '访问后台', 'code' => 'QJ_FWFW_FWHT', 'ismenu' => false),
                        )),
                    array('name' => '呼叫管理', 'code' => 'QJ_M3_HJGL', 'ismenu' => false, 'sub' => array(
                            array('name' => '隐藏电话号码', 'code' => 'QJ_HJGL_YCDHHM', 'ismenu' => false),
                        )),
                )
            ),
        )
    ),
);
