/**
数据库
@since 1.0 <2015-4-17> SoChishun Added.
@remark
    分库原则：
    1.频繁读写，根据模块分库(如：日志模块(包括所有类型日志，如用户操作日志，商品操作日志，商品进销存日志，订单日志，积分日志，财务日志等)、统计模块
    2.数据库巨大的，根据模块分库（如：日志模块、通话详情模块、消息模块、服务类文档模块）
    3.次要数据分库，如用户收藏、用户订阅等，可以拆分到独立库，或者需要效率的可以独立分库，如用户登录、频繁的查询所涉及的索引数据等
*/

-- 主库
CREATE DATABASE IF NOT EXISTS dbxcrm_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- 日志库
-- 频繁写入的日志主表都在这个库
CREATE DATABASE IF NOT EXISTS dbxcrm_log_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
-- 日志历史记录库
-- 超过限期(3个月)的日志都移到这个库,主要用于查询用,每siteID一个表.(3个月以上建议将数据导出到文件备份存档后从数据库清除,减轻压力)
CREATE DATABASE IF NOT EXISTS dbxcrm_log_history_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- 消息库
-- 数据量巨大,频繁读取(如短信、站内信、IM等，可能提供接口服务，那数据操作就更加频繁)
CREATE DATABASE IF NOT EXISTS dbxcrm_message_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
-- 消息记录库历史记录库
-- 超过限期(1个月)的记录都移到这个库,主要用于查询用,每siteID一个表.(3个月以上建议将数据导出到文件备份存档后从数据库清除,减轻压力)
CREATE DATABASE IF NOT EXISTS dbxcrm_message_history_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- 数据导入库
-- 大量插入操作的数据导入(如导入客户,导入商品,外呼号码等),导入时创建临时表,导入完成(复制到主库中)后删除临时表(临时表格式如：tcustomer_temp_{uid},tproduct_temp_{uid},tphone_temp_{uid})
CREATE DATABASE IF NOT EXISTS dbxcrm_import_temp_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- 通话详情记录库
-- 频繁写入的通话详情表都移到这个库
CREATE DATABASE IF NOT EXISTS dbxcrm_cdr_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
-- 通话详情记录历史记录库
-- 超过限期(1天)的日志都移到这个库,主要用于查询用,每siteID一个表.(3个月以上建议将数据导出到文件备份存档后从数据库清除,减轻压力)
CREATE DATABASE IF NOT EXISTS dbxcrm_cdr_history_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- 统计库
-- 频繁读写，需要分库
CREATE DATABASE IF NOT EXISTS dbxcrm_stats_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- 服务库
-- 数据量巨大,一般用于查询的文档类型记录(如：省市区数据、号码归属地数据、IP归属地数据、快递地区数据、比对数据等)都存于这个库
CREATE DATABASE IF NOT EXISTS dbxcrm_service_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;


