-- 消息记录库历史记录库
-- 超过限期(1个月)的记录都移到这个库,主要用于查询用,每siteID一个表.(3个月以上建议将数据导出到文件备份存档后从数据库清除,减轻压力)
CREATE DATABASE IF NOT EXISTS dbxcrm_message_history_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;