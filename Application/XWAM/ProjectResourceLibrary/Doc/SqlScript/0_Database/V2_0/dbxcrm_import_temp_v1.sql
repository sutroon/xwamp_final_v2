-- 数据导入库
-- 大量插入操作的数据导入(如导入客户,导入商品,外呼号码等),导入时创建临时表,导入完成(复制到主库中)后删除临时表(临时表格式如：tcustomer_temp_{uid},tproduct_temp_{uid},tphone_temp_{uid})
CREATE DATABASE IF NOT EXISTS dbxcrm_import_temp_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
