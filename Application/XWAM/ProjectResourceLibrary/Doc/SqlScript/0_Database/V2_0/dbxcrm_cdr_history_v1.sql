-- 通话详情记录历史记录库
-- 超过限期(1天)的日志都移到这个库,主要用于查询用,每siteID一个表.(3个月以上建议将数据导出到文件备份存档后从数据库清除,减轻压力)
CREATE DATABASE IF NOT EXISTS dbxcrm_cdr_history_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
