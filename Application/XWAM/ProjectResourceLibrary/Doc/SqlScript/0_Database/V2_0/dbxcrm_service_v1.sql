-- 服务库
-- 数据量巨大,一般用于查询的文档类型记录(如：省市区数据、号码归属地数据、IP归属地数据、快递地区数据、比对数据等)都存于这个库
CREATE DATABASE IF NOT EXISTS dbxcrm_service_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
