create table if not exists tfinance_payroll_statement_day(
    id int auto_increment primary key comment '主键编号',
    sale_month_id int not null comment 'sale_month表的主键编号',
    bill_date date not null comment '日期',
    user_name varchar(32) not null comment '营销员工号',
    trade_type varchar(32) not null comment '收支类型(订单提成、退单运费)',
    item_name varchar(64) not null comment '订单内容(商品名称)',
    order_amount decimal(12,2) not null default 0.00 comment '订单金额',
    commission_rate decimal(6,3) not null default 0.000 comment '提成比例',
    commission_amount decimal(12,2) not null default 0.00 comment '提成收入',
    income decimal(12,2) not null default 0.00 comment '收入金额',
    expenditure decimal(12,2) not null default 0.00 comment '支出金额',
    blance decimal(12,2) not null default 0.00 comment '余额',
    remark varchar(64) not null default '' comment '备注',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '财务_每日业绩提成报表(业绩明细)\r\n@since 1.0 <2015-3-11> sutroon <14507247@qq.com> Added.';
