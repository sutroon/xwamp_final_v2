create table if not exists tcommon_statistics(
    id int auto_increment primary key comment '主键编号',
    stat_key varchar(32) not null default '' comment '统计键名',
    int_value int not null default 0 comment '整数值',
    flt_value decimal(12,2) not null default 0.00 comment '小数值',
    remark varchar(64) not null default '' comment '备注',
    `state` smallint not null default 0 comment '状态(0=无效,1=有效)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '系统统计表(可分表)\r\n@since 1.0 <2015-3-13> sutroon <14507247@qq.com> Added.';
