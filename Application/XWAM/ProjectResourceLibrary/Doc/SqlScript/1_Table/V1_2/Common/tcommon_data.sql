create table if not exists tcommon_data (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null default '' comment '名称',
    `value` varchar(20480) not null default '' comment '值',
    `code` varchar(32) not null default '' comment '代码',
    parent_id int not null default 0 comment '父级编号',
    group_tags varchar(32) not null default '' comment '群组标签',
    link_url varchar(255) not null default '' comment '链接路径',
    remark varchar(32) not null default '' comment '备注',
    user_id int not null default 0 comment '用户编号',
    ex_tags varchar(16) not null default '' comment '扩展标签(如:SQL-DEBUG)',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用字典表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
