create table if not exists tcommon_region (
    id int auto_increment primary key comment '主键编号',
    region_name varchar(32) not null comment '地区名称',
    parent_id int not null default 0 comment '上级地址编号',
    zip varchar(8) not null default '' comment '邮政编码'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '中国地区表\r\n@since 1.0 <2014-12-8> sutroon <14507247@qq.com> Added.';
