create table if not exists tuser_payment (
    id int auto_increment primary key comment '主键编号',
    bank_name varchar(32) not null default '' comment '开户支行名称',
    bankcard_no varchar(32) not null default '' comment '银行卡卡号',
    bankcard_holder varchar(32) not null default '' comment '持卡人姓名',
    user_id int not null default 0 comment '用户编号',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_支付方式表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';
