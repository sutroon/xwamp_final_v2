create table if not exists tuser_favorite (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '标题',
    url varchar(255) not null default '' comment 'URL地址',
    object_id int not null default 0 comment '对象主键编号',
    category_id int not null default 0 comment '类别编号',
    star smallint not null default 0 comment '星级',
    remark varchar(255) not null default '' comment '备注',    
    user_id int not null default 0 comment '用户编号',
    ex_tags varchar(16) not null comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_收藏表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
