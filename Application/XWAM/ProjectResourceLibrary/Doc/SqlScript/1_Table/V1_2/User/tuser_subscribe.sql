create table if not exists tuser_subscribe (
    id int auto_increment primary key comment '主键编号',
    email varchar(32) not null default '' comment '电子邮件',
    user_id int not null default 0 comment '用户编号',
    ex_tags varchar(16) not null comment '扩展标签',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_订阅表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';
