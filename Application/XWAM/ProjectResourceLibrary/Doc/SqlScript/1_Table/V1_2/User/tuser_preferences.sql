create table if not exists tuser_preferences(
    user_id int not null primary key comment '用户表主键编号',
    page_size smallint not null default 25 comment '列表分页尺寸,默认25条',
    theme varchar(32) not null default '' comment '网站外观主题',
    lang varchar(32) not null default '' comment '语言'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_偏好设置(首选项)表\r\n@since 1.0 <2014-9-25> sutroon <14507247@qq.com> Added.';
