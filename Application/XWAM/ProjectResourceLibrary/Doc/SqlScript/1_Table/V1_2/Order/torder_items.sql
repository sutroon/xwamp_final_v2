create table if not exists torder_items (
    id int auto_increment primary key comment '主键编号',
    order_id int not null comment '订单编号',
    order_serial_no varchar(32) not null default '' comment '流水号',
    seller_user_id int not null comment '卖家用户编号',
    product_id int not null comment '商品编号',
    barcode varchar(32) not null default '' comment '仓库货号',
    `number` int not null comment '商品数量',
    price  decimal(12,2) not null comment '商品单价',
    discount_rate smallint not null default 100 comment '折扣比例(默认100%)',
    discount_amount decimal(12,2) not null comment '折扣金额',
    subtotal  decimal(12,2) not null comment '交易金额',
    category_id int not null default 0 comment '类别编号',
    unit_id int not null default 0 comment '单位编号',
    specification_id int not null default 0 comment '规格编号',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '订单_清单表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';
