/**
通用文章表
1.如果数据比较多，可以平行拆表，如tarticle_notice,tarticle_news
2.商品知识库也用这个表结构,表名称为tproduct_article,商品ID对应item_id
*/
create table if not exists tarticle (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '标题',
    category_id int not null default 0 comment '类别编号',
    content varchar(20480) not null default '' comment '内容',
    summary varchar(128) not null default '' comment '摘要',
    meta_keywords varchar(32) not null default '' comment 'meta关键词',
    meta_description varchar(128) not null default '' comment 'meta描述',
    image_url varchar(255) not null default '' comment '图片路径',
    visit_count int not null default 0 comment '浏览次数',
    reply_count int not null default 0 comment '回复次数',
    item_id int not null default 0 comment '项目编号(如商品编号等)',
    user_id int not null default 0 comment '用户编号',
    access_permission varchar(512) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密)',
    content_tags varchar(16) not null default '' comment '内容标签',
    ex_tags varchar(16) not null default '' comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=隐藏,1=显示,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime not null comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX ix_title (title),
    INDEX ix_user_id (user_id),
    INDEX ixu_item_id (item_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '文章表\r\n@since 1.0 <2014-6-22> sutroon<14507427@qq.com> Added.';
