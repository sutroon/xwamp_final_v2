
create table if not exists tfinance_account (
    id int auto_increment primary key comment '主键编号',
    user_id int not null comment '用户编号',
    total_amount  decimal(12,2) not null default 0.00 comment '账户金额',
    blance  decimal(12,2) not null default 0.00 comment '可用余额',
    frozen_blance  decimal(12,2) not null default 0.00 comment '不可用余额',
    admin_id int not null default 0 comment '管理员编号',
    remark varchar(64) not null default '' comment '备注',
    `state` smallint not null default 0 comment '账户状态(0=未审核,1=正常,4=冻结)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '变动时间',
    sys_code varchar(128) not null default '',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    KEY user_id_ix (user_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '财务_资金账户表\r\n@since 1.0 <2014-6-21> sutroon <14507247@qq.com> Added.';

create table if not exists tfinance_withdrawal (
    id int auto_increment primary key comment '主键编号',
    serial_no varchar(64) not null comment '流水号(TX[site_id]0[user_id][Ymdhis])',
    amount  decimal(12,2) not null comment '交易金额',
    blance decimal(12,2) not null comment '可用余额',
    commission  decimal(12,2) not null default 0.00 comment '手续费',
    user_id int not null comment '用户编号',
    admin_id int not null default 0 comment '管理员用户编号',
    remark_user varchar(64) not null default '' comment '用户备注',
    remark_admin varchar(64) not null default '' comment '管理员备注',
    bankcard_no varchar(32) not null default '' comment '银行卡卡号',
    bankcard_holder varchar(32) not null default '' comment '持卡人姓名',
    bank_id int not null default 0 comment '开户支行编号(对应tgeneral_data.ex_tags=BANK)',
    bank_name varchar(32) not null default '' comment '银行名称',
    trade_no varchar(64) not null default '' comment '交易流水号(如支付宝单号或银行回执单号)',
    `state` smallint not null default 0 comment '状态(0=未审核,1=成功,40=取消提现,44=审核失败)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间(提现时间)',
    update_time datetime comment '受理时间',
    sys_code varchar(128) not null default '',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    KEY user_id_ix (user_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '财务_提现表\r\n@since 1.0 <2014-6-23> sutroon <14507247@qq.com> Added.';

create table if not exists tfinance_recharge (
    id int auto_increment primary key comment '主键编号',
    serial_no varchar(64) not null comment '流水号(CZ[site_id]0[user_id][Ymdhis])',
    amount  decimal(12,2) not null comment '充值金额',
    blance decimal(12,2) not null comment '可用余额',
    recharge_type varchar(16) not null default '' comment '充值类型(XJ=现金,ZZ=转账,ZXZF=在线支付)',
    order_no varchar(128) not null default '' comment '订单编号(格式DD[site_id]0[user_id][Ymdhis])',
    trade_no varchar(64) not null default '' comment '交易流水号(如支付宝单号或银行回执单号)',
    bank_id int not null default 0 comment '开户支行编号(对应tgeneral_data.ex_tags=BANK)',
    bank_name varchar(32) not null default '' comment '银行名称',
    bankcard_no varchar(32) not null default '' comment '银行卡卡号',
    bankcard_holder varchar(32) not null default '' comment '持卡人姓名',
    user_id int not null comment '用户编号',
    admin_id int not null comment '管理员编号',
    remark_user varchar(64) not null default '' comment '用户备注',
    remark_admin varchar(64) not null default '' comment '管理员审核备注',
    `state` smallint not null default 0 comment '状态(0=等待付款,1=交易完成,2=已付款待发货,3=已发货待收货)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间(充值时间)',
    update_time datetime comment '受理时间(到账确认时间)',
    sys_code varchar(128) not null default '',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    KEY user_id_ix (user_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '财务_充值表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tfinance_transaction (
    id int auto_increment primary key comment '主键编号',
    serial_no varchar(64) not null comment '流水号(JY[site_id]0[user_id][Ymdhis])',
    amount decimal(12,2) not null comment '交易金额',
    transaction_type varchar(16) not null default '' comment '交易类型(ZZ=转账,GWFK=购物付款,GWSK=购物收款)',
    user_id_from int not null comment '付款人用户编号',
    user_id_to int not null comment '收款人用户编号',
    remark_from varchar(64) not null default '' comment '付款人备注',
    remark_to varchar(64) not null default '' comment '收款人备注',
    `state` smallint not null default 0 comment '状态(0=未审核,1=成功,40=关闭,44=失败)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '受理时间(收款确认时间)',
    sys_code varchar(128) not null default '',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '财务_普通交易表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tfinance_transaction_log (
    id int auto_increment primary key comment '主键编号',
    serial_no varchar(64) not null comment '流水号',
    amount  decimal(12,2) not null comment '交易金额',
    trade_source varchar(32) not null default '' comment '交易来源(CZ=充值,TX=提现,JY=交易)',
    trade_type varchar(16) not null default '' comment '交易类型',
    is_outlay smallint not null comment '是否支出',
    commission  decimal(12,2) not null default 0.00 comment '手续费',
    total_amount  decimal(12,2) not null default 0.00 comment '总金额',
    blance  decimal(12,2) not null comment '可用余额',
    frozen_blance  decimal(12,2) not null comment '冻结金额',
    user_id_from int not null comment '付款人用户编号',
    user_id_to int not null comment '收款人用户编号',
    remark_from varchar(64) not null default '' comment '付款人备注',
    remark_to varchar(64) not null default '' comment '收款人备注',
    `state` smallint not null default 0 comment '状态(0=未审核,1=成功,4=失败)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '受理时间',
    sys_code varchar(128) not null default '',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '财务_交易日志表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tfinance_payroll_statement_month(
    id int auto_increment primary key comment '主键编号',
    bill_date char(7) not null comment '日期,格式：2015-03(年月)',
    user_name varchar(32) not null comment '营销员工号',
    personal_name varchar(32) not null default '' comment '营销员姓名',
    sale_amount decimal(12,2) not null default 0.00 comment '销售金额',
    commission_amount decimal(12,2) not null default 0.00 comment '提成收入',
    expenditure decimal(12,2) not null default 0.00 comment '支出金额',
    bonus decimal(12,2) not null default 0.00 comment '奖金',
    leave_deduction decimal(12,2) not null default 0.00 comment '请假扣款',
    base_wage decimal(12,2) not null default 0.00 comment '基本工资',
    kpi_coefficient decimal(12,2) not null default 0.00 comment 'KPI系数',
    kpi_value decimal(12,2) not null default 0.00 comment 'KPI值',
    accrued_wage decimal(12,2) not null default 0.00 comment '应付工资(参考工资)',
    admin_name varchar(32) not null default '' comment '管理员工号',
    remark varchar(64) not null default '' comment '备注',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '财务_员工工资报表(工资单)\r\n@since 1.0 <2015-3-11> sutroon <14507247@qq.com> Added.';

create table if not exists tfinance_payroll_statement_day(
    id int auto_increment primary key comment '主键编号',
    sale_month_id int not null comment 'sale_month表的主键编号',
    bill_date date not null comment '日期',
    user_name varchar(32) not null comment '营销员工号',
    trade_type varchar(32) not null comment '收支类型(订单提成、退单运费)',
    item_name varchar(64) not null comment '订单内容(商品名称)',
    order_amount decimal(12,2) not null default 0.00 comment '订单金额',
    commission_rate decimal(6,3) not null default 0.000 comment '提成比例',
    commission_amount decimal(12,2) not null default 0.00 comment '提成收入',
    income decimal(12,2) not null default 0.00 comment '收入金额',
    expenditure decimal(12,2) not null default 0.00 comment '支出金额',
    blance decimal(12,2) not null default 0.00 comment '余额',
    remark varchar(64) not null default '' comment '备注',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '财务_每日业绩提成报表(业绩明细)\r\n@since 1.0 <2015-3-11> sutroon <14507247@qq.com> Added.';

create table if not exists tfinance_statement_month(
    id int auto_increment primary key comment '主键编号',
    bill_date char(7) not null comment '日期,格式：2015-03(年月)',
    item_name varchar(32) not null comment '收支项目',
    income decimal(12,2) not null default 0.00 comment '收入金额',
    expenditure decimal(12,2) not null default 0.00 comment '支出金额',
    accrued_wage decimal(12,2) not null default 0.00 comment '应付工资(参考工资)',
    blance decimal(12,2) not null default 0.00 comment '余额',
    remark varchar(64) not null default '' comment '备注',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '财务_每月报表\r\n@since 1.0 <2015-3-11> sutroon <14507247@qq.com> Added.';

create table if not exists tfinance_statement_day(
    id int auto_increment primary key comment '主键编号',
    stat_month_id int not null comment 'stat_month表的主键编号',
    bill_date date not null comment '日期',
    item_name varchar(32) not null comment '收支项目',
    income decimal(12,2) not null default 0.00 comment '收入金额',
    expenditure decimal(12,2) not null default 0.00 comment '支出金额',
    blance decimal(12,2) not null default 0.00 comment '余额',
    remark varchar(64) not null default '' comment '备注',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '财务_每日报表(资金明细)\r\n@since 1.0 <2015-3-11> sutroon <14507247@qq.com> Added.';

create table if not exists tfinance_bankcard (
    id int auto_increment primary key comment '主键编号',
    user_id int not  null comment '用户编号',
    card_no varchar(32) not null comment '银行卡卡号',
    card_holder varchar(32) not null comment '持卡人姓名',
    bank_id int not null comment '开户支行编号(对应tgeneral_data.ex_tags=BANK)',
    `state` smallint not null default 1 comment '状态(0=关闭,1=正常,2=默认)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'    
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '财务_银行卡表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';