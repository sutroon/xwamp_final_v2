
create table if not exists tproduct_specification (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null comment '名称',
    `value` text comment '值',
    item_id int not null default 0 comment '项目编号(如商品表的商品编号等)',
    price  decimal(12,2) not null default 0 comment '单价',
    stock int not null default 0 comment '库存量',
    ordinal smallint not null default 0 comment '排列次序',
    ex_tags varchar(16) not null comment '扩展标签',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_规格表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tproduct_category (
    id int auto_increment primary key comment '主键编号',
    category_name varchar(32) not null comment '名称',
    parent_id int not null default 0 comment '父级编号',
    type_id int not null default 0 comment '类型编号',
    item_count int not null default 0 comment '子项目数量(如相册显示相片数量等)',
    visit_count int not null default 0 comment '浏览次数',
    meta_keywords varchar(32) not null default '' comment 'meta关键词',
    meta_description varchar(128) not null default '' comment 'meta描述',
    image_url varchar(255) not null default '' comment '图片路径',
    link_url varchar(255) not null default '' comment '链接地址',
    user_id int not null default 0 comment '用户编号',
    access_permission varchar(512) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密)',
    remark varchar(32) not null default '' comment '备注',
    ex_tags varchar(16) not null comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_类别表\r\n@since 1.0 <2015-4-2> sutroon <14507247@qq.com> Added.';

create table if not exists tproduct_coupon (
    id int auto_increment primary key comment '主键编号',
    serial_no varchar(64) not null comment '序列码',
    `name` varchar(32) not null comment '优惠券名称',
    face_value  decimal(12,2) not null comment '优惠券面值',
    type_code varchar(16) not null comment '类型编号',
    quantity int not null comment '数量',
    condition_value  decimal(12,2) not null comment '使用条件',
    condition_remark text comment '使用条件说明',
    begin_time datetime comment '生效时间',
    end_time datetime comment '失效时间',
    ex_tags varchar(16) not null comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_优惠券表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tproduct_goods (
    id int auto_increment primary key comment '主键编号',
    product_name varchar(32) not null comment '商品名称',
    barcode varchar(32) not null default '' comment '仓库条码',
    model varchar(32) not null default '' comment '商品型号',
    type_id int not null default 0 comment '类型编号',
    category_id int not null default 0 comment '类别编号',
    price  decimal(12,2) not null default 0.00 comment '销售价格',
    market_price  decimal(12,2) not null default 0.00 comment '市场价格',
    cost_price  decimal(12,2) not null default 0.00 comment '成本价格',
    stock int not null default 0 comment '库存',
    warning_stock int not null default 0 comment '警告库存',
    sale_volume int not null default 0 comment '销量',
    discount_rate int not null default 100 comment '折扣率(默认100%)',
    discount_amount int not null default 0 comment '折扣金额',
    discount_price decimal(12,2) not null default 0.00 comment '折后价格',
    integral int not null default 0 comment '商品积分',
    visit_count int not null default 0 comment '浏览次数',
    reply_count int not null default 0 comment '回复次数',
    thumb_url varchar(255) not null default '' comment '缩略图路径',
    `size` varchar(32) not null default '' comment '商品尺寸(体积,格式:长cm:宽cm:高cm)',
    weight decimal(12,2) not null default 0.00 comment '重量',
    unit_id int not null default 0 comment '单位编号(tgeneral_data.ex_tags=PRODUCT-UNIT)',
    color_id int not null default 0 comment '颜色编号(tgeneral_data.ex_tags=PRODUCT-COLOR)',
    brand_id int not null default 0 comment '品牌编号(tgeneral_data.ex_tags=PRODUCT-BRAND)',
    grade_id int not null default 0 comment '品级编号(tgeneral_data.ex_tags=PRODUCT-GRADE)',
    packaging_id int not null default 0 comment '包装编号(tgeneral_data.ex_tags=PRODUCT-PACKAGING)',
    specification_id int not null default 0 comment '规格编号(tgeneral_data.ex_tags=PRODUCT-SPECIFICATION)',
    orgin_id int not null default 0 comment '产地编号(tgeneral_data.ex_tags=PRODUCT-ORGIN)',
    manufacturer_id int not null default 0 comment '厂家编号(tgeneral_data.ex_tags=PRODUCT-MANUFACTURER)',
    owner_id int not null comment '所有者用户编号',
    ex_tags varchar(16) not null default '' comment '扩展标签(GOODS=商品,ACCESSORY=配件,WAREHOUSE=仓库)',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=待上架,1=销售中,4=已下架,40=缺货中)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tproduct_details(
    product_id int primary key comment '商品编号',
    description varchar(5120) not null default '' comment '商品描述',
    meta_keywords varchar(16) not null default '' comment '搜索引擎优化关键词',
    meta_description varchar(255) not null default '' comment '搜索引擎优化描述',
    purchase_remarks  text not null default '' comment '购买说明',
    customer_services  text not null default '' comment '客户服务',
    update_time datetime comment '更新时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_详情表\r\n@since 1.0 <2014-12-13> sutroon <14507247@qq.com> Added.';

create table if not exists tproduct_timer(
    product_id int primary key comment '商品编号',
    start_time datetime comment '开始时间(用于秒杀商品、团购商品等)',
    end_time datetime comment '失效时间',
    `state` smallint not null default 0 comment '状态(0=待上架,1=销售中,4=已下架,40=缺货中)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_定时表\r\n@since 1.0 <2015-4-1> sutroon <14507247@qq.com> Added.';

create table if not exists tproduct_invoicing (
    id int auto_increment primary key comment '主键编号',
    product_id int not null comment '商品编号',
    category_id int not null default 0 comment '类别编号',
    is_warehouse_product smallint not null default 0 comment '是否仓库商品(0=销售商品,1=仓库商品)',
    order_id int not null default 0 comment '订单编号',
    `number` decimal(12,2) not null default 0 comment '商品数量',
    price  decimal(12,2) not null default 0 comment '商品价格',
    subtotal  decimal(12,2) not null default 0 comment '小计金额',
    old_stock decimal(12,2) not null default 0 comment '原有库存',
    stock decimal(12,2) not null default 0 comment '最新库存',
    remark varchar(64) not null default '' comment '备注',
    provider_id int not null default 0 comment '供应商编号',
    logistics_id int not null default 0 comment '物流编号',
    operate_type varchar(16) not null comment '操作类型(SELL-RETURN=销售退货(入库),SALE=销售出库,PURCHASE=采购入库,PURCHASE-RETURN=采购退货(出库),INTERNAL=内部出库,INVENTORY=盘点调仓)',
    user_id int not null comment '用户(内部出库经手人、销售卖家、退货入库仓管员、进货入库采购员或仓管员、盘点调仓财务员或仓管员)编号',
    admin_id int not null default 0 comment '管理员(内部出库审批人)编号',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_进销存明细表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
