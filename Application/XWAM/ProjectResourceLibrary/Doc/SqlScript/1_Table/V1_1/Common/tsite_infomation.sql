create table if not exists tsite_infomation (
    id int auto_increment primary key comment '主键编号',    
    title varchar(32) not null comment '标题',
    content varchar(20000) not null default '' comment '内容',
    summary varchar(128) not null default '' comment '摘要',
    visit_count int not null default 0 comment '浏览次数',
    user_id int not null default 0 comment '用户编号',
    ex_tags varchar(16) not null default '' comment '扩展标签(ABOUT_US=关于我们,CONTACT_US=联系我们)',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime not null comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    key idx_extags (ex_tags, site_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '站点信息表(包含关于我们、联系我们等单页资料)\r\n@since 1.0 <2015-5-19> SoChishun <14507247@qq.com> Added.';
