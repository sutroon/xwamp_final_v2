create table if not exists tuser_agent(
    user_id int not null primary key comment '用户表主键编号',
    quota_number int not null default 0 comment '配额数量',
    expire_time datetime comment '过期时间',
    update_time datetime comment '上次续费时间',
    agent_state smallint not null default 0 comment '代理状态(0=未审核,1=正常,3=冻结,4=禁用,7=注销)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_代理商属性表\r\n@since 1.0 <2014-7-11> sutroon <14507247@qq.com> Added.';
