create table if not exists tuser (
    id int auto_increment primary key comment '主键编号',
    login_name varchar(32) not null comment '用户名称(登录名)',
    password varchar(128) not null comment '登录密码(带有*的是加密过的密码,否则是明文密码)',
    user_type varchar(16) not null comment '内置用户类型(SEAT=座席,LEADER=营销组长,ADMIN=普通管理员,SUPERADMIN=超级管理员,AGENT=代理,EMPLOYEE=员工,CUSTOMER=客户)',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,3=冻结,4=禁用,7=注销,14=回收站)',
    site_id int not null default 0 comment '公司编号,在此表功能相当于parent_id,超级管理员可以给每个公司分配一个顶级管理员(site_id=0),顶级管理员可以再给本公司分配多个普通管理员,site_id=该顶级管理员的uid',
    UNIQUE KEY login_name_ix (login_name),
    KEY user_type_ix (user_type),
    KEY site_id_ix (site_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '用户表\r\n@since 1.0 <2015-4-8> sutroon <14507247@qq.com> Added.';

