create table if not exists tfinance_transaction (
    id int auto_increment primary key comment '主键编号',
    serial_no varchar(64) not null comment '流水号(JY[site_id]0[user_id][Ymdhis])',
    amount decimal(12,2) not null comment '交易金额',
    transaction_type varchar(16) not null default '' comment '交易类型(ZZ=转账,GWFK=购物付款,GWSK=购物收款)',
    user_id_from int not null comment '付款人用户编号',
    user_id_to int not null comment '收款人用户编号',
    remark_from varchar(64) not null default '' comment '付款人备注',
    remark_to varchar(64) not null default '' comment '收款人备注',
    `state` smallint not null default 0 comment '状态(0=未审核,1=成功,40=关闭,44=失败)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '受理时间(收款确认时间)',
    sys_code varchar(128) not null default '',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '财务_普通交易表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
