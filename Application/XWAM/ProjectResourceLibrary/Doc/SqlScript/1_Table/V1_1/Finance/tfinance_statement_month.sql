create table if not exists tfinance_statement_month(
    id int auto_increment primary key comment '主键编号',
    bill_date char(7) not null comment '日期,格式：2015-03(年月)',
    item_name varchar(32) not null comment '收支项目',
    income decimal(12,2) not null default 0.00 comment '收入金额',
    expenditure decimal(12,2) not null default 0.00 comment '支出金额',
    accrued_wage decimal(12,2) not null default 0.00 comment '应付工资(参考工资)',
    blance decimal(12,2) not null default 0.00 comment '余额',
    remark varchar(64) not null default '' comment '备注',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '财务_每月报表\r\n@since 1.0 <2015-3-11> sutroon <14507247@qq.com> Added.';
