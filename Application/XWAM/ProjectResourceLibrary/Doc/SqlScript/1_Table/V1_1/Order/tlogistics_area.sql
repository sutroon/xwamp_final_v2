create table if not exists tlogistics_area (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null comment '地区名称',
    parent_id int not null default 0 comment '父级编号',
    logistics_company int not null comment '物流公司编号',
    first_cost  decimal(8,2) not null comment '首重费用',
    additional_cost  decimal(8,2) not null comment '续重费用',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '物流地区表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
