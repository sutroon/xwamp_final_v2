create table if not exists tcommon_links (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null comment '链接名称',
    link_url varchar(255) not null comment '链接URL',
    `type` varchar(16) not null default '' comment '链接类型(friend=友情链接)',
    image_url varchar(255) not null default '' comment '图片URL',
    remark varchar(128) not null default '' comment '备注',
    user_id int not null default 0 comment '用户名编号',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用友情链接表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
