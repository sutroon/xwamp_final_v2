create table if not exists tfinance_bankcard (
    id int auto_increment primary key comment '主键编号',
    user_id int not  null comment '用户编号',
    card_no varchar(32) not null comment '银行卡卡号',
    card_holder varchar(32) not null comment '持卡人姓名',
    bank_id int not null comment '开户支行编号(对应tgeneral_data.ex_tags=BANK)',
    `state` smallint not null default 1 comment '状态(0=关闭,1=正常,2=默认)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'    
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '财务_银行卡表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
