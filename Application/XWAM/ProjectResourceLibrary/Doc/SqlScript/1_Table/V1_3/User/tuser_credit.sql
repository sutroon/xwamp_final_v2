create table if not exists tuser_credit (
    user_id int not null primary key comment '用户编号',
    credit  decimal(12,2) not null default 0.00 comment '主积分的值',
    ext_cerdit1  decimal(12,2) not null default 0.00 comment '扩展积分1',
    ext_cerdit2  decimal(12,2) not null default 0.00 comment '扩展积分2',
    ext_cerdit3  decimal(12,2) not null default 0.00 comment '扩展积分3',
    ext_cerdit4  decimal(12,2) not null default 0.00 comment '扩展积分4',
    ext_cerdit5  decimal(12,2) not null default 0.00 comment '扩展积分5',
    ext_cerdit6  decimal(12,2) not null default 0.00 comment '扩展积分6',
    ext_cerdit7  decimal(12,2) not null default 0.00 comment '扩展积分7',
    ext_cerdit8  decimal(12,2) not null default 0.00 comment '扩展积分8',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    modified_date datetime comment '记录修改时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_积分表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
