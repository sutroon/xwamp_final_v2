create table if not exists tproduct_specification (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null comment '名称',
    `value` text comment '值',
    item_id int not null default 0 comment '项目编号(如商品表的商品编号等)',
    price  decimal(12,2) not null default 0 comment '单价',
    stock int not null default 0 comment '库存量',
    ordinal smallint not null default 0 comment '排列次序',
    ex_tags varchar(16) not null comment '扩展标签',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_规格表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
