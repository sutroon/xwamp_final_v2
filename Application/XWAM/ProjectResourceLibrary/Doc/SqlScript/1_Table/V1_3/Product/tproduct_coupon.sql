create table if not exists tproduct_coupon (
    id int auto_increment primary key comment '主键编号',
    serial_no varchar(64) not null comment '序列码',
    `name` varchar(32) not null comment '优惠券名称',
    face_value  decimal(12,2) not null comment '优惠券面值',
    type_code varchar(16) not null comment '类型编号',
    quantity int not null comment '数量',
    condition_value  decimal(12,2) not null comment '使用条件',
    condition_remark text comment '使用条件说明',
    begin_time datetime comment '生效时间',
    end_time datetime comment '失效时间',
    ex_tags varchar(16) not null comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_优惠券表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
