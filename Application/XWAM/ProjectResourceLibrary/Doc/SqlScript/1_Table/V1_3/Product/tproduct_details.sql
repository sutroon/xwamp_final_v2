create table if not exists tproduct_details(
    product_id int primary key comment '商品编号',
    description varchar(5120) not null default '' comment '商品描述',
    meta_keywords varchar(16) not null default '' comment '搜索引擎优化关键词',
    meta_description varchar(255) not null default '' comment '搜索引擎优化描述',
    purchase_remarks  text not null default '' comment '购买说明',
    customer_services  text not null default '' comment '客户服务',
    update_time datetime comment '更新时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_详情表\r\n@since 1.0 <2014-12-13> sutroon <14507247@qq.com> Added.';
