create table if not exists torder_shoppingcar(
    id int auto_increment primary key comment '主键编号',
    buyer_user_id int not null comment '买家用户编号',
    product_id int not null comment '商品编号',
    `number` int not null comment '商品数量',
    discount_rate smallint not null default 100 comment '折扣比例(默认100%)',
    discount_amount decimal(12,2) not null comment '折扣金额',
    subtotal  decimal(12,2) not null comment '交易金额',
    `state` smallint not null default 0 comment '状态',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '订单_购物车表\r\n@since 1.0 <2014-12-19> sutroon <14507247@qq.com> Added.';
