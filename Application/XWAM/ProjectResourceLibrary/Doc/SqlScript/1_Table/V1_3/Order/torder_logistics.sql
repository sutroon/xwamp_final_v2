create table if not exists torder_logistics(
    order_id int primary key comment '订单表主键编号',
    express_no varchar(32) not null default '' comment '快递单号',
    logistics_queryer varchar(64) not null default '' comment '物流响应责任人(快递员姓名或快递官方号码400811111)',
    courier_phone varchar(32) not null default '' comment '快递员电话',
    courier_phone_display_reson varchar(32) not null default '' comment '显示快递员电话原因(如:号码不符|电话未接|电话关机|号码不符,通知客户取货)',
    courier_phone_create_time datetime comment '录入快递员电话时间',
    logistics_detail varchar(4096) not null default '' comment '物流配送详情',
    logistics_result varchar(128) not null default '' comment '物流配送最后情况',
    logistics_info_remark varchar(64) not null default '' comment '物流跟踪备注',
    logistics_state smallint not null default 1 comment '物流状态(1:已发货,20:已签收,21:已签收但未更新,40:拒签,41:已拒签但未更新,44:已退货)',
    logistics_update_time datetime comment '物流更新时间。跟物流最后时间不一样。有延迟。此时间才是做为签收算业绩的时间',
    logistics_last_time datetime comment '物流最后时间',
    logistics_time datetime comment '发货时间',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '订单_订单物流表\r\n@since 1.0 <2015-1-8> sutroon <14507247@qq.com> Added.';
