create table if not exists torder_returns(
    order_id int primary key comment '订单表主键编号',
    serial_no varchar(64) not null comment '退货单号(同快递单号)',
    seller_returned_fee decimal(8,2) not null default 0.00 comment '退货员工承担的运费',
    handling varchar(32) not null default '' comment '处理方式(PUPAWAY=退货入库,REDELIVERY=重新发货,RECLAIM-REDELIVERY=不要求归还并重新发货, REFUND=退款, COMPENSATION=不退货并赔偿)',
    handling_fee decimal(8,2) not null default 0.00 comment '处理费用',
    handling_time datetime comment '处理时间',
    returned_time datetime comment '退货时间',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '订单_退货表\r\n@since 1.0 <2015-1-8> sutroon <14507247@qq.com> Added.';
