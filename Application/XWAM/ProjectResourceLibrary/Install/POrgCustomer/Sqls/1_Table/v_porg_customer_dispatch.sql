-- 视图字段构建语句 select concat(', c.',COLUMN_NAME) from information_schema.`COLUMNS` where TABLE_SCHEMA='db_xwam_v1' and TABLE_NAME='t_porg_customer';

-- 客户预约表视图 2016-1-27
create or replace view v_porg_customer_dispatch as
select
  c.serial_no
, c.name
, c.sex
, c.telphone
, c.mobile
, c.address
, c.buy_count
, c.total_amount
, c.buy_time
, c.review_time
, c.interested
, c.site_id
, d.id
, d.title
, d.customer_id
, d.old_user_name
, d.new_user_name
, d.admin_name
, d.dispatch_remark
, d.status
, d.create_time
from t_porg_customer_dispatch d left join t_porg_customer c on d.customer_id=c.id order by id desc;
