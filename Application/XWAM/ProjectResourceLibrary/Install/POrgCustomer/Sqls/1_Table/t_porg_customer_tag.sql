drop table if exists t_porg_customer_tag;
create table if not exists t_porg_customer_tag(
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '标签名称',
    user_name varchar(16) not null default '' comment '工号',
    sort smallint not null default 0 comment '排列次序',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_user_name (user_name),
    UNIQUE INDEX idxu_title (title)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '客户标签表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
