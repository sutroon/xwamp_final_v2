/**
 * 座席CTI控制器
 * @since 1.0 2014-12-4
 */
function agent_cti_manager_init() {

    // 点击监听按钮
    $('#agent_cti_manager_toolbar .extension-monitor').click(function () {
        return false;
    })
    
    /**
     * 点击保持恢复按钮
     * @example <a href="{:U('XCall/CtiManager/seat_callpark','opt=Y&uid=5')}" class="icon-pause extension-callpark">保持</a>
     * @example <a href="{:U('XCall/CtiManager/seat_callpark','opt=N&uid=5')}" class="icon-transferees_history extension-callpark">恢复</a>
     */
    $('#agent_cti_manager_toolbar .extension-callpark').click(function () {
        extension_jsonp_event($(this).attr('href'), 'extension_jsonp_handler');
        return false;
    })

    /**
     * 点击挂机按钮
     * @example <a href="{:U('CtiManager/seat_handup&uid=5')}" class="icon-stop extension-handup">挂断</a>
     */
    $('#agent_cti_manager_toolbar .extension-handup').click(function () {
        extension_jsonp_event($(this).attr('href'), 'extension_jsonp_handler');
        return false;
    })
    
    /**
     * 点击呼出按钮
     * @example <input type="text" placeholder="电话号码" /><a href="{:U('CtiManager/seat_dialout_save','phone=varphone&uid=5')}" class="icon-dial extension-dialout">拨号</a>  
     */
    $('#agent_cti_manager_toolbar .extension-dialout').click(function () {
        var phone = $(this).prev().val();
        if (!phone) {
            alert('号码未填写!');
            return false;
        }
        var url = $(this).attr('href').replace('varphone', phone);
        extension_jsonp_event(url, 'extension_dialout_jsonp');
        return false;
    })
    
    /**
     * 点击会议按钮
     * @example <a href="{:U('XCall/CtiManager/seat_meeting_edit&uid=5')}" class="icon-meeting extension-meeting">会议</a>
     */
    $('#agent_cti_manager_toolbar .extension-meeting').click(function () {
        return false;
    })

    /**
     * 点击示闲示忙按钮
     * @example <a href="{:U('CtiManager/seat_queuePause','opt=BUSY&uid=5')}" class="icon-notservice extension-pause">示忙</a>
     * @example <a href="{:U('CtiManager/seat_queuePause','opt=FREE&uid=5')}" class="icon-phone_ctrl extension-pause">示闲</a>
     */
    $('#agent_cti_manager_toolbar .extension-pause').click(function () {
        extension_jsonp_event($(this).attr('href'), 'extension_jsonp_handler');
        return false;
    })

    /**
     * 点击签入签出按钮
     * @example <a href="{:U('CtiManager/seat_sign','opt=LOGIN&uid=5')}" class="door extension-sign">签入</a>
     * @example <a href="{:U('CtiManager/seat_sign','opt=LOGINOUT&uid=5')}" class="icon-exit extension-sign">签出</a>
     */
    $('#agent_cti_manager_toolbar .extension-sign').click(function () {
        extension_jsonp_event($(this).attr('href'), 'extension_jsonp_handler');
        return false;
    })
    // 点击刷新状态按钮
    $('#agent_cti_manager_toolbar .extension-refresh-state').click(function () {
        extension_get_state();
        return false;
    })
    extension_get_state();

    /**
     * 座席来电弹屏
     * @returns {undefined}
     * @since 2014-12-16 by sutroon;
     * @example <input type="hidden" id="extension-screen-popup-url" value="{:U('XCall/ApiCallRecord/seat_screen_popup','exten=300&uid=5')}"/>
     */
    function extension_screen_popup() {
        extension_jsonp_event($('#extension-screen-popup-url').val(), 'extension_screen_popup_jsonp');
    }
    extension_screen_popup();
    setInterval(extension_screen_popup, 2000);
}

/**
 * 获取座席状态
 * @returns {undefined}
 * @example <a href="{:U('CtiManager/seat_state','uid=5')}" class="icon-reload extension-refresh-state">刷新</a>
 */
function extension_get_state() {
    var url = $('#agent_cti_manager_toolbar .extension-refresh-state').attr('href');
    extension_jsonp_event(url, 'extension_get_state_jsonp');
}

/**
 * 获取座席状态处理方法
 * @param {type} data
 * @returns {undefined}
 */
function extension_get_state_jsonp(data) {
    $('#extension-status').text(data);
}

/**
 * 来电弹屏处理方法
 * @param json data
 * @returns {undefined}
 * @since 1.0 2014-12-16 by sutroon
 * @example <a id="call-reminder" href="{:U('Customer/customer_edit','tel=vartel')}" style="position: absolute; left:40%; top:0px; background-color: #F60; padding: 8px 60px; color:#FFF; display: none;" target="main" data-type="{$call_remainder}" data-show="page" onclick="$(this).hide();" data-url="{:U('Customer/customer_edit','tel=vartel')}">您有新来电</a>
 */
function extension_screen_popup_jsonp(data) {
    if (!data) {
        return;
    }
    if (!data.line) {
        return; // 如果没有电话号码则返回,用于修正手拨电话联系弹出两次窗口的问题 2015-1-2 by sutroon
    }
    var $a = $('#call-reminder');
    var url = $a.data('url');
    var show = $a.data('show');
    var type = $a.data('type'); // 1=跳转客户;2=消息提醒
    if (!url) {
        alert('链接无效');
        return;
    }
    url = url.replace('vartel', data.line);
    if ('2' == type) {
        $a.attr('href', url).show();
        return;
    }
    if (undefined == show || 'dialog' == show) {
        show = 'dialog';
    } else {
        show = 'page';
    }
    if ('page' == show) {
        $('#main').attr('src', "about:blank").attr('src', url).load(function () {
            setIframeHeight();
        });
    } else {
        open_dialog(url, {}, undefined, 'customer-popup', 5);
    }
}

/**
 * 座席外呼处理方法
 * @param json data
 * @returns {undefined}
 * @since 1.0 2014-12-16 by sutroon
 * @example <input type="text" placeholder="电话号码" /><a href="{:U('CtiManager/seat_dialout_save','phone=varphone&uid=5')}" class="icon-dial extension-dialout">拨号</a>
 */
function extension_dialout_jsonp(data) {
    if (data.success == '1') {
        var tel = $('#agent_cti_manager_toolbar .extension-dialout').prev().val();
        extension_screen_popup_jsonp({'line': tel});
    } else {
        $.somessager.alert('错误', data.message);
    }
}

/**
 * jsonp事件
 * @param string url url
 * @param string handler 处理方法,如 extension_screen_popup_jsonp
 * @returns {undefined}
 * @since 1.0 2014-12-16 by sutroon
 */
function extension_jsonp_event(url, handler) {
    if (!url) {
        return;
    }
    $.ajax({url: url.replace('.html', '/jsonp/' + handler + '.html'), dataType: "jsonp", jsonp: handler, beforeSend: function () {
            ajax_show_loading(false);
        }});
}

/**
 * 座席通用jsonp处理方法
 * @param {type} data
 * @returns {undefined}
 * @since 1.0 2014-12-16 by sutroon
 */
function extension_jsonp_handler(data) {
    if (data.success == '1') {
        extension_get_state();
        $.somessager.alert('消息', '操作成功!');
    } else {
        $.somessager.alert('错误', data.message);
    }
}