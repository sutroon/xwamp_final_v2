<?php

/*
 * 权限生成工具
 * 支持字段：[string]title:标题,[string]name:名称,[string]code:代码,[string]type:节点类型(M=菜单,O=操作,E=页面元素),[boolean]enable:是否可用,[string]url:链接地址,[array]children:子项目,[string]comment:注解
 * 说明：默认ismenu=true,如要特殊声明，需强制加上ismenu=false
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @since 4.0 <2015-9-19> SoChishun 重构以适合RBAC自动拦截规则
 */
return array(
    /* 系统模块 */
    array(
        'title' => '系统',
        'code' => 'M1_XT', // 一级菜单不要加厂商代号 2015-10-19 SoChishun Added.
        'children' => array(
            array(
                'title' => '系统设置',
                'code' => 'POrg_XT_M2_XTSZ',
                'children' => array(
                    array('name' => 'SiteConfig-index', 'level' => '2', 'title' => '网站设置', 'code' => 'POrg_XT_M3_WZSZ', 'url' => 'SiteConfig/site_config'),
                    array('name' => 'VarConfig-index', 'level' => '2', 'title' => '变量配置', 'code' => 'POrg_XT_M3_BLPZ', 'url' => 'VarConfig/var_config'),
                ),
            ),
            array(
                'title' => '站务管理',
                'code' => 'POrg_XT_M2_ZQGL',
                'children' => array(
                    array('name' => 'PM-pm_list', 'level' => '2', 'title' => '站内信管理', 'code' => 'POrg_XT_M3_ZNXGL', 'url' => 'PM/pm_list'),
                    array('name' => 'VarConfig-index', 'level' => '2', 'title' => '问题反馈', 'code' => 'POrg_XT_M3_WTFK', 'url' => 'VarConfig/var_config'),
                ),
            ),
            array(
                'title' => '网站安全',
                'code' => 'POrg_XT_M2_WZAQ',
                'children' => array(
                    array('name' => 'SiteBackup-index', 'level' => '2', 'title' => '数据备份', 'code' => 'POrg_XT_M3_SJBF', 'url' => 'SiteBackup/index'),
                    array('name' => 'SiteReduction-index', 'level' => '2', 'title' => '还原数据', 'code' => 'POrg_XT_M3_HYSJ', 'url' => 'SiteReduction/index'),
                    array('name' => 'LogAnalysis-index', 'level' => '2', 'title' => '日志分析', 'code' => 'POrg_XT_M3_RZFX', 'url' => 'LogAnalysis/index'),
                ),
            ),
        )
    ),
);
