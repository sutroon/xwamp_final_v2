drop table if exists t_porg_feedback;
create table if not exists t_porg_feedback (
    id int auto_increment primary key comment '主键编号',
    category_id int not null default 0 comment '类别编号',
    pid int not null default 0 comment '父级编号(留言回复对应的主题编号)',
    serial_no varchar(64) not null default '' comment '流水号(FK[site_id]0[user_id][Ymdhis])',
    title varchar(32) not null default '' comment '标题',
    content varchar(1024) not null default '' comment '内容',
    message varchar(128) not null default '' comment '悄悄话(只有管理员可见)',
    contacts varchar(16) not null default '' comment '联系人',
    email varchar(32) not null default '' comment '电子邮件',
    mobile varchar(32) not null default '' comment '手机号码',
    telphone varchar(32) not null default '' comment '电话号码',
    reply_count int not null default 0 comment '回复数量',
    user_name varchar(32) not null default '' comment '创建人用户名',
    sort smallint not null default 0 comment '排列次序',
    remark varchar(64) not null default '' comment '备注',
    status smallint not null default 0 comment '状态(0:未审核;1:已审核;2:已受理;4:关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_title (title),
    INDEX idx_pid (pid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '留言反馈表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
