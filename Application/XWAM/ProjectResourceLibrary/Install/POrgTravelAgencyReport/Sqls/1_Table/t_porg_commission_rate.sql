drop table if exists t_porg_commission_rate;
create table if not exists t_porg_commission_rate (
    id int auto_increment primary key comment '主键编号',
    travel_agency_id int not null comment '旅行社编号',
    title varchar(64) not null default '' comment '配置标题',
    parking_fee decimal(5,2) not null default 0 comment '停车费',
    personnel_fee decimal(5,2) not null default 0 comment '人头费',
    sichou_commission_rate int not null default 0 comment '丝绸佣金比例',
    sijin_commission_rate int not null default 0 comment '丝巾佣金比例',
    baihuo_commission_rate int not null default 0 comment '百货佣金比例',
    remark varchar(32) not null default '' comment '配置说明',
    user_name varchar(32) not null default '' comment '创建人用户名',
    status smallint not null default 0 comment '状态',
    sort smallint not null default 0 comment '排序',
    create_time timestamp not null default current_timestamp comment '创建时间',
    store_id int not null comment '分店编号',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_travel_agency_id (travel_agency_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '旅行社佣金比例配置表\r\n@since 1.0 <2016-05-28> SoChishun <14507247@qq.com> Added.';

