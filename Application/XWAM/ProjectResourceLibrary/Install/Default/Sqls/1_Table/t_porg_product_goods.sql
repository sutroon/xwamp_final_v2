drop table if exists t_porg_product_goods;
create table if not exists t_porg_product_goods (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '商品名称',
    barcode varchar(32) not null default '' comment '仓库条码',
    model varchar(32) not null default '' comment '商品型号',
    type_id int not null default 0 comment '类型编号',
    category_id int not null default 0 comment '类别编号',
    price  decimal(12,2) not null default 0.00 comment '销售价格',
    market_price  decimal(12,2) not null default 0.00 comment '市场价格',
    purchase_quantity decimal(12,2) not null default 0.00 comment '进货数量(入库数量)',
    purchase_amount decimal(12,2) not null default 0.00 comment '进货金额(入库金额)',
    sale_quantity decimal(12,2) not null default 0.00 comment '销售数量(出库数量)',
    sale_amount decimal(12,2) not null default 0.00 comment '销售金额(出库金额)',
    gross_profit decimal(12,2) not null default 0.00 comment '毛利润金额',
    cost_price  decimal(12,2) not null default 0.00 comment '成本均价',
    stock decimal(12,2) not null default 0.00 comment '库存',
    warning_stock decimal(12,2) not null default 0.00 comment '警告库存',
    discount_rate int not null default 100 comment '折扣率(默认100%)',
    discount_amount int not null default 0 comment '折扣金额',
    discount_price decimal(12,2) not null default 0.00 comment '折后价格',
    integral int not null default 0 comment '商品积分',
    visit_count int not null default 0 comment '浏览次数',
    reply_count int not null default 0 comment '回复次数',
    picture_url varchar(255) not null default '' comment '缩略图路径',
    summary varchar(128) not null default '' comment '摘要内容',
    `size` varchar(32) not null default '' comment '商品尺寸(体积,格式:长cm:宽cm:高cm)',
    weight decimal(12,2) not null default 0.00 comment '重量',
    unit_id int not null default 0 comment '单位编号(tgeneral_data.ex_tags=PRODUCT-UNIT)',
    color_id int not null default 0 comment '颜色编号(tgeneral_data.ex_tags=PRODUCT-COLOR)',
    brand_id int not null default 0 comment '品牌编号(tgeneral_data.ex_tags=PRODUCT-BRAND)',
    grade_id int not null default 0 comment '品级编号(tgeneral_data.ex_tags=PRODUCT-GRADE)',
    packaging_id int not null default 0 comment '包装编号(tgeneral_data.ex_tags=PRODUCT-PACKAGING)',
    specification_id int not null default 0 comment '规格编号(tgeneral_data.ex_tags=PRODUCT-SPECIFICATION)',
    orgin_id int not null default 0 comment '产地编号(tgeneral_data.ex_tags=PRODUCT-ORGIN)',
    manufacturer_id int not null default 0 comment '厂家编号(tgeneral_data.ex_tags=PRODUCT-MANUFACTURER)',
    user_name varchar(32) not null default '' comment '创建人用户名',
    ex_tags varchar(16) not null default '' comment '扩展标签(GOODS=商品,ACCESSORY=配件,WAREHOUSE=仓库)',
    sort smallint not null default 0 comment '排列次序',
    status smallint not null default 0 comment '状态(0=待上架,1=销售中,4=已下架,40=缺货中)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime not null default 0 comment '更新时间',
    store_id int not null default 0 comment '分店编号',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
