drop table if exists t_porg_attendance_card_time;
create table if not exists t_porg_attendance_card_time (
    attendance_no varchar(16),
    time_hms time,
    INDEX idx_attendance_no (attendance_no)
)  ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '考勤打卡时间整理表\r\n@since 1.0 <2016-3-2> SoChishun Added.';