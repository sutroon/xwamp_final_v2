drop table if exists t_porg_area;
create table if not exists t_porg_area (
  id int auto_increment primary key comment '主键编号',
  area_name varchar(32) not null comment '地区名称',
  pid int not null default 0 comment '上级地址编号',
  zip_code varchar(8) not null default '' comment '邮政编码',
  INDEX idx_area_name (area_name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 comment '号码归属地表 1.0 2016-1-22 by sutroon\r\n2.0 2016-1-23 SoChishun 重构.';