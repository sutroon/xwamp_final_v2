<?php

/*
 * 权限生成工具
 * 支持字段：[string]title:标题,[string]name:名称,[string]code:代码,[string]type:节点类型(M=菜单,O=操作,E=页面元素),[boolean]enable:是否可用,[string]url:链接地址,[array]children:子项目,[string]comment:注解
 * 说明：默认ismenu=true,如要特殊声明，需强制加上ismenu=false
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @since 4.0 <2015-9-19> SoChishun 重构以适合RBAC自动拦截规则
 */
return array(
    /* 内容模块 */
    array(
        'title' => '内容',
        'code' => 'M1_NR', // 一级菜单不要加厂商代号 2015-10-19 SoChishun Added.
        'children' => array(
            array(
                'title' => '内容管理',
                'code' => 'POrg_NR_M2_NRGL',
                'children' => array(
                    array('name' => 'Channels-channel_list', 'level' => '2', 'title' => '站点栏目', 'code' => 'POrg_NR_M3_ZDLM', 'url' => 'Channels/channel_list'),
                    array('name' => 'Notices-document_list', 'level' => '2', 'title' => '公告管理', 'code' => 'POrg_NR_M3_ZDLM', 'url' => 'Notices/document_list'),
                    array('name' => 'Knowledge-document_list', 'level' => '2', 'title' => '知识库', 'code' => 'POrg_NR_M3_ZDLM', 'url' => 'Knowledge/document_list'),
                    array('name' => 'NewsCategory-category_list', 'level' => '2', 'title' => '友情链接', 'code' => 'POrg_NR_M3_YQLJGL', 'url' => 'Links/links_list'),
                    array('name' => 'Advertising-advert_list', 'level' => '2', 'title' => '媒体广告', 'code' => 'POrg_NR_M3_MTGGGL', 'url' => 'Advertising/advert_list'),
                ),
            ),
        )
    ),
);
