drop table if exists t_porg_user_favorite;
create table if not exists t_porg_user_favorite (
    id int auto_increment primary key comment '主键编号',
    record_id int not null default 0 comment '对象主键编号',
    category_id int not null default 0 comment '收藏类别编号',
    star smallint not null default 0 comment '星级',
    remark varchar(255) not null default '' comment '备注',    
    user_name varchar(32) not null default '' comment '创建人用户名',
    sort smallint not null default 0 comment '排列次序',
    `status` smallint not null default 0 comment '状态(0:隐藏;1:显示)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    index idx_user_name (user_name),
    index idx_record_id (record_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户收藏表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
