drop table if exists t_porg_pm;
create table if not exists t_porg_pm (
    id int auto_increment primary key comment '主键编号',
    category_id int not null default 0 comment '类别编号',
    from_user varchar(16) not null comment '寄件人',
    to_user varchar(128) not null comment '收件人(多个之间以逗号隔开,如:user1,user2)',
    from_alias varchar(32) not null default '' comment '寄件人别名',
    subject varchar(32) not null default '' comment '消息标题',
    body varchar(2048) not null default '' comment '消息正文',
    `status` smallint not null default 0 comment '状态(0=未发送/草稿,1=发送成功,4=发送失败,2=正在发送)',
    read_status char(1) not null default 'N' comment '是否已读(Y:已读;N:未读)',
    delete_status varchar(1) not null default 'N' comment '是否删除',
    star smallint not null default 0 comment '标注星级',
    read_time datetime comment '阅读时间',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间(发送时间)',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_from_user (from_user,status),
    INDEX idx_to_user (to_user,delete_status)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '站内信表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.\r\n@since 1.1 <2015-12-2> SoChishun 新增star,read_status';
