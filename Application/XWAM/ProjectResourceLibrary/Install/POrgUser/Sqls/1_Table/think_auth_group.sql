drop table if exists think_auth_group;
create table if not exists think_auth_group (
    id int auto_increment primary key comment '主键编号',
    title varchar(30) not null default '' comment '',
    display_title varchar(30) not null default '' comment '',
    code varchar(32) not null default '' comment '角色代码',
    rules varchar(800) not null default '' comment '权限节点编号',
    remark varchar(32) not null default '' comment '备注',
    sort smallint not null default 0 comment '排列次序',
    `status` smallint not null default 1 comment '',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment='用户组(角色)表\r\n@since 1.0 <2016-4-5> SoChishun <14507247@qq.com> Added.';