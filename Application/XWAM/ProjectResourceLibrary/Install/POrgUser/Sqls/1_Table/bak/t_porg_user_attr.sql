create table t_porg_user_attr (
    id int auto_increment primary key comment '主键编号',
    type_id int not null comment '用户类型编号', 
    title varchar(20) not null default '' comment '属性名称(姓名,手机号码,联系地址,性别,QQ,安全问题,回答等)',
    -- 字段属性 --
    field_name varchar(16) not null default '' comment '字段名称',
    field_type varchar(16) not null default 'varchar' comment '字段类型',
    field_length varchar(8) not null default '' comment '字段长度',
    field_decimals smallint not null default 0 comment '小数长度',
    field_nullable char(1) not null default 'N' comment '是否允许null',
    field_default varchar(32) not null default '' comment '默认值',
    field_comment varchar(128) not null default '' comment '字段注释',
    -- /字段属性 --
    -- 内容属性 --
    is_required char(1) not null default 'N' comment '是否必填',
    searchable char(1) not null default 'N' comment '是否允许搜索',
    options_content varchar(128) not null default '' comment '配置内容(json)',
    options_value varchar(255) not null default '' comment '配置值',
    options_content_type varchar(20) not null default '' comment '配置内容数据类型(C:字符;N:数字;T:文本;A:数组;E:枚举,email)值类型',
    input_data_type varchar(20) not null default '' comment '输入验证类型(输入项:输入内容不限制;仅限输入数字;仅限输入字符;仅限输入数字和字符;选择项:单选项;多选项;时间项:日期(年月日);即时通讯项:QQ;MSN;旺旺;Skype)',
    group_name varchar(16) not null default '' comment '属性分组(default:系统默认;concat:联系方式)',
    sort smallint not null default 0 comment '排序',
    status smallint not null default 1 comment '状态',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户属性表\r\n@since 1.0 <2015-10-28> SoChishun <14507247@qq.com> Added.\r\n';

-- insert into t_porg_user_attr (id, title) value ('客户编号',);
-- insert into t_porg_user_attr (id, title) value ('购买意向',);
-- insert into t_porg_user_attr (id, title) value ('营销结果',);
-- insert into t_porg_user_attr (id, title) value ('自定义标签',);
-- insert into t_porg_user_attr (id, title) value ('消费次数',);
-- insert into t_porg_user_attr (id, title) value ('营销结果',);
