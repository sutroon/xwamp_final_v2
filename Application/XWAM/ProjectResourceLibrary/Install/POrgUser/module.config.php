<?php

/**
 * 应用模块配置文件
 * <br />注意：本文件包含许可授权等重要信息,在生产环境中必须文件级加密处理(在打包助手打包的时候会自动加密)
 * @since 1.0 <2015-8-28> SoChishun Added.
 */
return array(
    /* 环境最低需求配置 */
    'ENV_REQUIRE' => array(
        'THINKPHP_VERSION' => '3.2.3', // ThinkPHP 版本需求
        'PLATFORM_VERSION' => '1.0.0' // 平台 版本需求(平台在部署的时候会自动检测PHP版本和MySQL版本需求)
    ),
    /* 模块信息配置 */
    'MODULE_INFO' => array(
        'VERSION' => '1.0.0', // 版本号,应用模块升级的时候使用
        'VENDOR_CODE' => 'POrg', // 厂商代号
        'NAME' => 'POrgSystem', // 应用模块名称(英文)
        'TITLE' => '官方系统管理模块', // 应用模块标题
        'DESC' => '包含网站基本设置、变量配置和导航管理等子模块', // 应用模块简介
        'VENDOR_NAME' => '官方', // 应用模块提供商名称
        'LICENCE' => 'FREE', // 应用模块许可证授权方式(FREE=免费,PAY=付费)
        'PAY_CHANNEL_URL' => '', // 付费通道URL
        'RELEASE_TIME'=>'2015-10-19', // 发行时间
    ),
    /* 安装信息配置 */
    'INSTALL' => array(
        /* 1_数据库安装脚本, 路径必需是以应用模块为根目录 */
        '1_SQL' => array(
            '1_TABLE' => 'Install/Sqls/1_Table/',
            '2_VIEW' => '',
            '3_Routine' => '',
            '4_Data' => 'Install/Sqls/4_Data/',
            '5_Patches' => '',
        ),
        /* 2_权限安装脚本, 可以是数组或文件地址 (注意：权限代号必需加厂商代号作为前缀!) */
        '2_PERMISSION' => 'Install/Data/Permission_auth.php',
        /* 3_动态安装代码 */
        '3_SCRIPT_FILE' => 'InstallController.class.php',
    ),
    /* 卸载信息配置 */
    'UNINSTALL' => array(
        /* 卸载数据库对象 */
        '1_SQL' => array(
            '1_Table' => array('p1_t_log_user_login', 'p1_t_log_user_operate', 'p1_t_user', 'p1_t_user_department', 'p1_t_user_preferences', 'p1_t_user_role'),
            '2_View' => array(),
            '3_Procedure' => array(),
            '4_Function' => array(),
            '5_Sqls' => array(),
        ),
        /* 2_动态卸载代码 */
        '2_SCRIPT_FILE' => 'UninstallController.class.php',
    ),
);
