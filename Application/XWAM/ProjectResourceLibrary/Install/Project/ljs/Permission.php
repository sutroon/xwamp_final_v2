<?php

/*
 * 权限生成工具
 * <br />运行方式：http://localhost:8801/Tool/Incubator/SuPermissionSql_PHP_1_0/SoPermissionSql.php
 * 支持字段：[string]title:标题,[string]name:名称,[string]code:代码,[string]type:节点类型(M=菜单,O=操作,E=页面元素),[boolean]enable:是否可用,[string]url:链接地址,[array]children:子项目,[string]comment:注解
 * 说明：默认ismenu=true,如要特殊声明，需强制加上ismenu=false
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @since 4.0 <2015-9-19> SoChishun 重构以适合RBAC自动拦截规则
 */
return array(
    /* 系统模块 */
    array(
        'title' => '系统',
        'code' => 'M1_XT', // 一级菜单不要加厂商代号 2015-10-19 SoChishun Added.
        'children' => array(
            array(
                'title' => '系统设置',
                'code' => 'XT_M2_XTSZ',
                'children' => array(
                    array('name' => 'SiteConf.site_edit', 'level' => '2', 'title' => '网站设置', 'code' => 'XT_M3_WZSZ', 'url' => 'SiteConfig/site_edit'),
                    array('name' => 'SystemConf.conf_list', 'level' => '2', 'title' => '系统变量配置', 'code' => 'XT_M3_BLPZ', 'url' => 'SystemConf/conf_list'),
                    array('name' => 'CustomConf.conf_list', 'level' => '2', 'title' => '用户变量配置', 'code' => 'XT_M3_BLPZ', 'url' => 'CustomConf/conf_list'),
                ),
            ),
            array(
                'title' => '站务管理',
                'code' => 'XT_M2_ZQGL',
                'children' => array(
                    array('name' => 'Feedback.index', 'level' => '2', 'title' => '访客留言', 'code' => 'XT_M3_FKLY', 'url' => 'Feedback/var_config'),
                ),
            ),
        )
    ),
    /* 用户模块 */
    array(
        'title' => '用户',
        'code' => 'M1_YH',
        'children' => array(
            array(
                'title' => '用户管理',
                'code' => 'YH_M2_YHGL',
                'children' => array(
                    array('name' => 'UserAdmin.user_list', 'level' => '2', 'title' => '管理员管理', 'code' => 'YH_M3_GLYGL', 'url' => 'UserAdmin/user_list',
                        'O' => array(
                            array('name' => 'UserAdmin.user_edit', 'level' => '3', 'title' => '新增管理员', 'code' => 'YH_YHGL_XZGLY', 'enable' => false),
                            array('name' => 'UserAdmin.user_edit', 'level' => '3', 'title' => '编辑管理员', 'code' => 'YH_YHGL_BJGLY', 'enable' => false),
                            array('name' => 'UserAdmin.user_delete', 'level' => '3', 'title' => '删除管理员', 'code' => 'YH_YHGL_SCGLY', 'enable' => false),
                        ),
                    ),
                    array('name' => 'Role.index', 'level' => '2', 'title' => '角色管理', 'code' => 'YH_M3_JSGL', 'url' => 'Role/role_list'),
                ),
            ),
        )
    ),
    /* 商品模块 */
    array(
        'title' => '商品',
        'code' => 'M1_SP',
        'children' => array(
            array(
                'title' => '商品管理',
                'code' => 'SP_M2_SPGL',
                'children' => array(
                    array('name' => 'ProductGoods', 'level' => '2', 'title' => '商品管理', 'code' => 'SP_M3_SPGL', 'url' => 'ProductGoods/product_goods_list',
                        'O' => array(
                            array('name' => 'ProductGoods.add', 'level' => '3', 'title' => '新增商品', 'code' => 'SP_SPGL_XZSP'),
                            array('name' => 'ProductGoods.edit', 'level' => '3', 'title' => '编辑商品', 'code' => 'SP_SPGL_BJSP'),
                            array('name' => 'ProductGoods.delete', 'level' => '3', 'title' => '删除商品', 'code' => 'SP_SPGL_SCSP'),
                        ),),
                ),
            ),
            array(
                'title' => '商品类别管理',
                'code' => 'SP_M2_SPLBGL',
                'children' => array(
                    array('name' => 'ProductCategory', 'level' => '2', 'title' => '商品类别管理', 'code' => 'SP_M3_SPLBGL', 'url' => 'ProductCategory/category_list',
                        'O' => array(
                            array('name' => 'ProductCategory.add', 'level' => '3', 'title' => '新增类别', 'code' => 'SP_SPLBGL_XZLB'),
                            array('name' => 'ProductCategory.edit', 'level' => '3', 'title' => '编辑类别', 'code' => 'SP_SPLBGL_BJLB'),
                            array('name' => 'ProductCategory.delete', 'level' => '3', 'title' => '删除类别', 'code' => 'SP_SPLBGL_SCLB'),
                        ),),
                ),
            ),
        )
    ),
    /* 内容模块 */
    array(
        'title' => '内容',
        'code' => 'M1_NR',
        'children' => array(
            array(
                'title' => '内容管理',
                'code' => 'POrg_NR_M2_NRGL',
                'children' => array(
                    array('name' => 'Channels.channel_list', 'level' => '2', 'title' => '站点栏目', 'code' => 'NR_M3_ZDLM', 'url' => 'Channels/channel_list'),
                    array('name' => 'DocumentMenu.menu_list', 'level' => '2', 'title' => '导航菜单', 'code' => 'NR_M3_DHCD', 'url' => 'DocumentMenu/menu_list'),
                    array('name' => 'NewsCategory.category_list', 'level' => '2', 'title' => '友情链接', 'code' => 'NR_M3_YQLJGL', 'url' => 'Links/links_list'),
                    array('name' => 'Advertising.advert_list', 'level' => '2', 'title' => '媒体广告', 'code' => 'NR_M3_MTGGGL', 'url' => 'Advertising/advert_list'),
                    array('name' => 'Notices.document_list', 'level' => '2', 'title' => '公告管理', 'code' => 'NR_M3_ZDLM', 'url' => 'Notices/document_list'),
                ),
            ),
        )
    ),
);
