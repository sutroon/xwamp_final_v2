<?php

/*
 * XWAMP后台管理系统权限节点数据
 * 
 * 支持字段：
 *  [string]title:标题
 *  [string]name:名称
 *  [string]code:代码
 *  [string]type:节点类型 (M(Menu)=菜单,O(Operate)=操作,F(File)=文件,E(Element)=页面元素),
 *  [boolean]enable:是否可用,
 *  [string]url:链接地址,
 *  [array]children:子项目,
 *  [string]comment:注解
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @since 4.0 <2015-9-19> SoChishun 重构以适合RBAC自动拦截规则
 */
return array(
    /* 系统模块 */
    array(
        'title' => '系统',
        'code' => 'M1_XT', // 一级菜单不要加厂商代号 2015-10-19 SoChishun Added.
        'children' => array(
            array(
                'title' => '系统设置',
                'code' => 'XT_M2_XTSZ',
                'children' => array(
                    array('name' => 'CustomConf.conf_list', 'level' => '2', 'title' => '字典配置', 'code' => 'XT_M3_BLPZ', 'url' => 'CustomConf/conf_list'),
                ),
            ),
        )
    ),
    /* 用户模块 */
    array(
        'title' => '用户',
        'code' => 'M1_YH',
        'children' => array(
            array(
                'title' => '用户管理',
                'code' => 'YH_M2_YHGL',
                'children' => array(
                    array('name' => 'UserAdmin.user_list', 'level' => '2', 'title' => '用户管理', 'code' => 'YH_M3_GLYGL', 'url' => 'UserAdmin/user_list'),
                    array('name' => 'Role.index', 'level' => '2', 'title' => '角色管理', 'code' => 'YH_M3_JSGL', 'url' => 'Role/role_list'),
                ),
            ),
        )
    ),
    /* 商品模块 */
    array(
        'title' => '内容',
        'code' => 'M1_SP',
        'children' => array(
            array(
                'title' => '内容管理',
                'code' => 'SP_M2_SPGL',
                'children' => array(
                    array('name' => 'ProductGoods', 'level' => '2', 'title' => '微信菜单管理', 'code' => 'SP_M3_SPGL', 'url' => 'ProductGoods/product_goods_list'),
                    array('name' => 'ProductGoods', 'level' => '2', 'title' => '微信消息管理', 'code' => 'SP_M3_SPGL', 'url' => 'ProductGoods/product_goods_list'),
                ),
            ),
        )
    ),
    /* 内容模块 */
    array(
        'title' => '内容',
        'code' => 'M1_NR',
        'children' => array(
            array(
                'title' => '内容管理',
                'code' => 'POrg_NR_M2_NRGL',
                'children' => array(
                    array('name' => 'Channels.channel_list', 'level' => '2', 'title' => '菜单栏管理', 'code' => 'NR_M3_ZDLM', 'url' => 'Channels/channel_list'),
                    array('name' => 'DocumentMenu.menu_list', 'level' => '2', 'title' => '文章管理', 'code' => 'NR_M3_DHCD', 'url' => 'DocumentMenu/menu_list'),
                    array('name' => 'DocumentMenu.menu_list', 'level' => '2', 'title' => '文件管理', 'code' => 'NR_M3_DHCD', 'url' => 'DocumentMenu/menu_list'),
                ),
            ),
        )
    ),
);
