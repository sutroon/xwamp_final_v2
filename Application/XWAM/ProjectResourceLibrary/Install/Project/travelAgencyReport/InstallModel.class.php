<?php

namespace XWAM\Model;

/**
 * 主平台环境检测模型类
 * <br />检测PHP版本、MySQL版本，自动安装平台依赖数据表
 *
 * @since 1.0 <2016-4-22> SoChishun <14507247@qq.com> Added.
 * @since 2.0 <2016-5-10> SoChishun 重构, SysDBModel 重命名为 InstallModel.
 */
class InstallModel {

    /**
     * 检测数据库是否存在
     * @param type $site_id
     * @return type
     * @since 1.0 <2016-4-22> SoChishun <14507247@qq.com> Added.
     * @since 2.0 <2016-5-10> SoChishun 重构.
     */
    public static function check_db_exists($site_id = 0) {
        if (!in_array('mysql', get_loaded_extensions())) {
            die('系统不支持mysql数据库扩展，请联系管理员开启!');
        }
        $link = mysql_connect(C('DB_HOST'), C('DB_USER'), C('DB_PWD')) or die('error:' . mysql_error());
        mysql_query('set names ' . C('DB_CHARSET')) or die('error:' . mysql_error());
        //$db_name = 'db_xcrm_u' . $site_id;
        $db_name = C('DB_NAME');
        $result = mysql_query("show databases like '$db_name';", $link) or die('error:' . mysql_error());
        $row = mysql_fetch_array($result);
        if (!$row) {
            mysql_query("CREATE DATABASE IF NOT EXISTS $db_name DEFAULT CHARSET utf8 COLLATE utf8_general_ci;") or die('error:' . mysql_error()); // 创建数据库
        }
        mysql_free_result($result);
        mysql_close($link);
        return $row ? true : false;
    }

    // 2016-4-22
    public static function ensure_database($site_id, $user_name) {
        $advm = new \Think\Model\AdvModel();
        // 执行创建数据表的脚本
        $table_sqls = array(
// think_auth_department
            "create table if not exists think_auth_department( id int auto_increment primary key, title varchar(32) not null, pid int not null default 0, `code` varchar(32) not null default '', rules varchar(128) not null default '', sort smallint not null default 0, remark varchar(32) not null default '', user_name varchar(32) not null default '', `status` smallint not null default 1, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_pid (pid) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// think_auth_group
            "create table if not exists think_auth_group ( id int auto_increment primary key, title varchar(30) not null default '', display_title varchar(30) not null default '', code varchar(32) not null default '', rules varchar(800) not null default '', remark varchar(32) not null default '', sort smallint not null default 0, `status` smallint not null default 1, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0 ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// think_auth_rule
            "create table if not exists think_auth_rule ( id int auto_increment primary key, `name` varchar(64) not null default '', title varchar(50) not null, code varchar(32) not null default '', pid smallint(6) unsigned not null, addon varchar(32) not null default '', `level` tinyint(1) unsigned not null, `type` varchar(16) not null default '', url varchar(64) not null default '', `condition` varchar(128) not null default '', remark varchar(32) not null default '', sort smallint not null default 0, status smallint not null default '0', create_time timestamp not null default CURRENT_TIMESTAMP, INDEX ix_code (code), INDEX ix_pid (pid) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_plvs_order
            "create table if not exists t_plvs_order ( id int auto_increment primary key, store_id int not null, time_date date not null, travel_agency_id int not null, license_plate_number varchar(8) not null, card_number varchar(32) not null, person_count int not null, tourist_origin varchar(255) not null default '', tourist_destination varchar(255) not null default '', tourist_type_id int not null, interpretation_room_id varchar(32) not null default '', sichou_amount decimal(9,2) not null default 0.00, sijin_amount decimal(9,2) not null default 0.00, baihuo_amount decimal(9,2) not null default 0.00, cash_amount decimal(9,2) not null default 0.00, credit_amount decimal(9,2) not null default 0.00, product_amount decimal(9,2) not null default 0.00, sale_type varchar(16) not null default '', deposit_date date not null default 0, commission_amount decimal(8,2) not null default 0.00, settlement_amount decimal(9,2) not null default 0.00, interpreter_id varchar(32) not null default '', interpret_performance decimal(9,2) not null default 0.00, guide_id varchar(255) not null default '', commission_rate_id int not null default 0, parking_fee decimal(8,2) not null default 0, personnel_fee decimal(8,2) not null default 0, sichou_commission_fee decimal(8,2) not null default 0, sijin_commission_fee decimal(8,2) not null default 0, baihuo_commission_fee decimal(8,2) not null default 0, fee_subtotal decimal(8,2) not null default 0, settlement_status smallint not null default 0, user_name varchar(32) not null default '', remark varchar(255) not null default '', create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_branch_store_id (store_id), INDEX idx_time_date (time_date), INDEX idx_travel_agency_id (travel_agency_id), INDEX idx_user_name (user_name) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
// t_porg_commission_discount
            "create table if not exists t_porg_commission_discount ( id int auto_increment primary key, time_date date not null, sale_type varchar(16) not null default '', store_id int not null, travel_agency_id int not null, commission_rate_id int not null default 0, parking_fee decimal(8,2) not null default 0, personnel_fee decimal(8,2) not null default 0, sichou_commission_fee decimal(8,2) not null default 0, sijin_commission_fee decimal(8,2) not null default 0, baihuo_commission_fee decimal(8,2) not null default 0, subtotal decimal(8,2) not null default 0, user_name varchar(32) not null default '', status smallint not null default 0, create_time timestamp not null default current_timestamp, site_id int not null default 0, update_time datetime, INDEX idx_time_date (time_date), INDEX idx_branch_store_id (`store_id`), INDEX idx_travel_agency_id (`travel_agency_id`), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_commission_rate
            "create table if not exists t_porg_commission_rate ( id int auto_increment primary key, travel_agency_id int not null, title varchar(64) not null default '', parking_fee decimal(5,2) not null default 0, personnel_fee decimal(5,2) not null default 0, sichou_commission_rate int not null default 0, sijin_commission_rate int not null default 0, baihuo_commission_rate int not null default 0, remark varchar(32) not null default '', user_name varchar(32) not null default '', status smallint not null default 0, sort smallint not null default 0, create_time timestamp not null default current_timestamp, site_id int not null default 0, INDEX idx_travel_agency_id (travel_agency_id) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_custom_conf
            "create table if not exists t_porg_custom_conf ( id int auto_increment primary key, title varchar(32) not null default '', `group` varchar(32) not null default '', content varchar(128) not null default '', lcontent varchar(12800) not null default '', remark varchar(32) not null default '', user_name varchar(32) not null default '', status smallint not null default 0, sort smallint not null default 0, create_time timestamp not null default current_timestamp, store_id int not null default 0, site_id int not null default 0, update_time datetime, INDEX idx_title (`title`), INDEX `idx_group` (`group`), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_notices
            "create table if not exists t_porg_notices ( id int auto_increment primary key, title varchar(32) not null, category_id int not null default 0, meta_keywords varchar(32) not null default '', meta_description varchar(128) not null default '', picture_url varchar(255) not null default '', visit_count int not null default 0, content varchar(20480) not null default '', user_name varchar(32) not null default '', content_tags varchar(16) not null default '', sort smallint not null default 0, `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime not null default 0, site_id int not null default 0, INDEX idx_title (title), INDEX idx_category_id (category_id) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_product_category
            "create table if not exists t_porg_product_category ( id int auto_increment primary key, title varchar(32) not null, pid int not null default 0, item_count int not null default 0, visit_count int not null default 0, meta_keywords varchar(32) not null default '', meta_description varchar(128) not null default '', image_url varchar(255) not null default '', link_url varchar(255) not null default '', user_name varchar(32) not null default '', access varchar(512) not null default '', remark varchar(32) not null default '', sort smallint not null default 0, status smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, store_id int not null default 0, site_id int not null default 0, INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_product_goods
            "create table if not exists t_porg_product_goods ( id int auto_increment primary key, title varchar(32) not null, barcode varchar(32) not null default '', model varchar(32) not null default '', type_id int not null default 0, category_id int not null default 0, price decimal(12,2) not null default 0.00, market_price decimal(12,2) not null default 0.00, purchase_quantity decimal(12,2) not null default 0.00, purchase_amount decimal(12,2) not null default 0.00, sale_quantity decimal(12,2) not null default 0.00, sale_amount decimal(12,2) not null default 0.00, gross_profit decimal(12,2) not null default 0.00, cost_price decimal(12,2) not null default 0.00, stock decimal(12,2) not null default 0.00, warning_stock decimal(12,2) not null default 0.00, discount_rate int not null default 100, discount_amount int not null default 0, discount_price decimal(12,2) not null default 0.00, integral int not null default 0, visit_count int not null default 0, reply_count int not null default 0, picture_url varchar(255) not null default '', summary varchar(128) not null default '', `size` varchar(32) not null default '', weight decimal(12,2) not null default 0.00, unit_id int not null default 0, color_id int not null default 0, brand_id int not null default 0, grade_id int not null default 0, packaging_id int not null default 0, specification_id int not null default 0, orgin_id int not null default 0, manufacturer_id int not null default 0, user_name varchar(32) not null default '', ex_tags varchar(16) not null default '', sort smallint not null default 0, status smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime not null default 0, store_id int not null default 0, site_id int not null default 0, INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_product_invoicing
            "create table if not exists t_porg_product_invoicing ( id int auto_increment primary key, product_id int not null, category_id int not null default 0, is_warehouse smallint not null default 0, order_id int not null default 0, `number` decimal(12,2) not null default 0, price decimal(12,2) not null default 0, subtotal decimal(12,2) not null default 0, old_stock decimal(12,2) not null default 0, new_stock decimal(12,2) not null default 0, remark varchar(64) not null default '', provider_id int not null default 0, logistics_id int not null default 0, direction_type char(1) not null default '', operate_type varchar(16) not null default '', user_name varchar(32) not null default '', admin_name varchar(32) not null default '', create_time timestamp not null default CURRENT_TIMESTAMP, store_id int not null default 0, site_id int not null default 0, INDEX idx_product_id (product_id), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_site_conf
            "create table if not exists t_porg_site_conf ( id int auto_increment primary key, site_title varchar(32) not null, site_code varchar(16) not null, site_app_name varchar(32) not null default 'XWAMP', site_app_version varchar(16) not null default '1.0.0.0', site_status char(1) not null default 'Y', site_closed_announcement varchar(255) not null default '', site_logo_url varchar(255) not null default '', site_icp_code varchar(32) not null default '', site_summary varchar(128) not null default '', site_home_url varchar(128) not null default 'index', site_login_url varchar(128) not null default 'login', site_db_sharding char(1) not null default '', site_sub_path varchar(16) not null default '/xcrm', site_category_id int not null default 0, site_company_name varchar(32) not null default '', visit_count int not null default 0, session_prefix varchar(32) not null default '', encrypt_key varchar(16) not null default 'sue123!@#', ui_login_captcha_status char(1) not null default 'Y', ui_aside_status char(1) not null default 'Y', content_draft_aotosave_interval smallint not null default 0, content_draftbox_status char(1) not null default 'N', content_list_rows smallint not null default 25, content_reply_list_rows smallint not null default 25, visit_allow_register char(1) not null default 'Y', data_backup_path varchar(255) not null default '', data_backup_part_size varchar(16) not null default '2M', admin_email varchar(32) not null default '', admin_name varchar(32) not null default '', admin_telphone varchar(32) not null default '', admin_mobile varchar(32) not null default '', admin_address varchar(255) not null default '', cti_screenpop_mode char(1) not null default 'M', cti_screenpop_duplicate_expire smallint not null default 180, user_name varchar(32) not null default '', sort smallint not null default 0, ex_tags varchar(16) not null default '', create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime, status smallint not null default 0, INDEX idx_site_title (site_title), INDEX idx_site_category_id (site_category_id), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_store_conf
            "create table if not exists t_porg_store_conf ( id int auto_increment primary key, title varchar(32) not null default '', sale_title1 varchar(32) not null default '', sale_title2 varchar(32) not null default '', sale_title3 varchar(32) not null default '', settlement_title varchar(32) not null default '', remark varchar(32) not null default '', user_name varchar(32) not null default '', status smallint not null default 0, sort smallint not null default 0, create_time timestamp not null default current_timestamp, site_id int not null default 0, update_time datetime, INDEX idx_title (`title`), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_system_conf
            "create table if not exists t_porg_config ( id int auto_increment primary key, `name` varchar(32) not null, title varchar(32) not null default '', `type` char(1) not null default 'C', `group` char(1) not null default '', prop char(1) not null default 'C', content varchar(128) not null default '', lcontent varchar(12800) not null default '', remark varchar(32) not null default '', user_name varchar(32) not null default '', status smallint not null default 0, sort smallint not null default 0, create_time timestamp not null default current_timestamp, site_id int not null default 0, update_time datetime, UNIQUE INDEX idxu_name (`name`), INDEX `idx_group` (`group`), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_travel_agency_conf
            "create table if not exists t_porg_travel_agency_conf ( id int auto_increment primary key, title varchar(32) not null default '', remark varchar(32) not null default '', user_name varchar(32) not null default '', status smallint not null default 0, sort smallint not null default 0, create_time timestamp not null default current_timestamp, store_id int not null default 0, site_id int not null default 0, update_time datetime not null default 0, INDEX idx_title (`title`), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_user
            "create table if not exists t_porg_user ( id int auto_increment primary key, user_name varchar(32) not null, password varchar(128) not null, type_id smallint not null default 0, type_name varchar(16) not null default '', role_ids varchar(32) not null default '', department_ids varchar(128) not null default '', branch_ids varchar(128) not null default '', status smallint not null default 0, site_id int not null default 0, unreadmsg smallint not null default 0, trade_password varchar(128) not null default '', `name` varchar(32) not null default '', sex char(1) not null default 'F', face_url varchar(255) not null default '', marital_status char(1) not null default 'N', birthday datetime, id_type varchar(16) not null default '', id_no varchar(32) not null default '', education varchar(16) not null default '', company_name varchar(16) not null default '', company_type varchar(16) not null default '', attendance_no varchar(32) not null default '', vocation varchar(32) not null default '', `position` varchar(32) not null default '', extension varchar(16) not null default '', telphone varchar(18) not null default '', mobile varchar(18) not null default '', email varchar(32) not null default '', fax varchar(32) not null default '', qq varchar(16) not null default '', taobao varchar(32) not null default '', country varchar(32) not null default '', province varchar(32) not null default '', city varchar(32) not null default '', area varchar(32) not null default '', street varchar(128) not null default '', zip varchar(8) not null default '', level_id smallint not null default 0, advance decimal(20,3) not null default 0.000, advance_freeze decimal(20,3) not null default 0.000, point int unsigned not null default 0, point_freeze int unsigned not null default 0, point_history int unsigned not null default 0, score_rate decimal(5,3) not null default 0, experience int not null default 0, theme varchar(16) not null default '', lang varchar(8) not null default '', page_size smallint not null default 0, personal_sign varchar(128) not null default '', custom_title varchar(16) not null default '', screenpopup_mode char(1) not null default '', remark varchar(32) not null default '', regist_refer varchar(16) not null default 'local', regist_ip varchar(16) not null default '', pw_answer varchar(32) not null default '', pw_question varchar(32) not null default '', login_ip varchar(16) not null default '', login_count int not null default 0, login_time datetime, login_time_temp datetime, online_status smallint not null default 0, online_active_time datetime, sort smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime, UNIQUE INDEX idxu_user_name (user_name, id), INDEX idx_name (`name`), INDEX idx_mobile (mobile), INDEX idx_email (email) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 PARTITION by HASH(id) PARTITIONS 10;",
        );
        $advm->patchQuery($table_sqls);
        // 自动创建管理员帐号 2016-4-22
        $data_sqls = array(
            "insert into t_porg_user (user_name, password, type_name, site_id, regist_ip, role_ids, status) values ('sa','123456', 'SYSTEMADMIN', 0, '127.0.0.1', 1, 1),('$user_name','123456', 'ADMIN', $site_id, '127.0.0.1', 2, 1);",
            "insert into think_auth_group (title, rules, site_id) values ('系统管理员','*',$site_id),('总管','*',$site_id),('经理','*',$site_id),('录单员','1,2,3,4,8,9,11,13,14,15,18,19,20,21,24,25,26,28,29,30,32,33,34,36,37,38',$site_id);",
        );
        $advm->patchQuery($data_sqls);
        // 权限规则
        $rule_sqls = array(
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (1,'','门店','0','','M1_MD',1,'','F',0,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (2,'','门店管理','1','','POrg_MD_M2_MDGL',2,'','M',0,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (3,'StoreDaySales','日销售表','2','','MD_M3_RXSB',3,'StoreDaySales/daysales_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (4,'StoreDaySales.add','新增日销售表','3','','MD_RXSB_XZRXSB',4,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (5,'StoreDaySales.edit','编辑日销售表','3','','MD_RXSB_BJRXSB',5,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (6,'StoreDaySales.delete','删除日销售表','3','','MD_RXSB_SCRXSB',6,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (7,'CommissionDiscount','返佣表','2','','MD_M3_FYB',7,'CommissionDiscount/commission_discount_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (8,'ProductGoods','商品管理','2','','MD_M3_SPGL',8,'ProductGoods/product_goods_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (9,'ProductGoods.add','新增商品','8','','MD_SPGL_XZSP',9,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (10,'ProductGoods.edit','编辑商品','8','','MD_SPGL_BJSP',10,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (11,'ProductGoods.setting','商品类别管理','8','','MD_SPGL_LBGL',11,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (12,'ProductGoods.delete','删除商品','8','','MD_SPGL_SCSP',12,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (13,'ProductInvoicing','出入库管理','2','','MD_M3_CRKGL',13,'ProductInvoicing/invoicing_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (14,'ProductInvoicing.add','新增出库','13','','MD_CRKGL_XZCK',14,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (15,'ProductInvoicing.add','新增入库','13','','MD_CRKGL_XZRK',15,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (16,'ProductInvoicing.delete','删除出库','13','','MD_CRKGL_SCCK',16,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (17,'ProductInvoicing.delete','删除入库','13','','MD_CRKGL_SCRK',17,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (18,'DataAnalysis','数据分析','2','','MD_M3_SJFX',18,'DataAnalysis/data_analysis_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (19,'TravelAgencyConf','旅行社配置','2','','PZ_M3_LXS',19,'TravelAgencyConf/travel_agency_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (20,'TravelAgencyConf.add','新增旅行社','19','','PZ_PZGL_XZLXS',20,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (21,'TravelAgencyConf.edit','编辑旅行社','19','','PZ_PZGL_BJLXS',21,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (22,'TravelAgencyConf.setting','配置佣金比例','19','','PZ_PZGL_PZLXS',22,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (23,'TravelAgencyConf.delete','删除旅行社','19','','PZ_PZGL_SCLXS',23,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (24,'InterpreterConf','讲解员配置','2','','PZ_M3_JJY',24,'InterpreterConf/interpreter_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (25,'InterpreterConf.add','新增讲解员','24','','PZ_PZGL_XZJJY',25,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (26,'InterpreterConf.edit','编辑讲解员','24','','PZ_PZGL_BJJJY',26,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (27,'InterpreterConf.delete','删除讲解员','24','','PZ_PZGL_SCJJY',27,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (28,'InterpretationRoomConf','讲解厅配置','2','','PZ_M3_JJT',28,'InterpretationRoomConf/interpretation_room_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (29,'InterpretationRoomConf.add','新增讲解厅','28','','PZ_PZGL_XZJJT',29,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (30,'InterpretationRoomConf.edit','编辑讲解厅','28','','PZ_PZGL_BJJJT',30,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (31,'InterpretationRoomConf.delete','删除讲解厅','28','','PZ_PZGL_SCJJT',31,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (32,'GuideConf','导游员配置','2','','PZ_M3_DYY',32,'GuideConf/guide_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (33,'GuideConf.add','新增导游员','32','','PZ_PZGL_XZDYY',33,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (34,'GuideConf.edit','编辑导游员','32','','PZ_PZGL_BJDYY',34,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (35,'GuideConf.delete','删除导游员','32','','PZ_PZGL_SCDYY',35,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (36,'TouristTypeConf','客源类型配置','2','','PZ_M3_KYLX',36,'TouristTypeConf/tourist_type_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (37,'TouristTypeConf.add','新增客源类型','36','','PZ_PZGL_XZKYLX',37,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (38,'TouristTypeConf.edit','编辑客源类型','36','','PZ_PZGL_BJKYLX',38,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (39,'TouristTypeConf.delete','删除客源类型','36','','PZ_PZGL_SCKYLX',39,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (40,'','配置','0','','M1_PZ',40,'','M',0,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (41,'','配置管理','40','','POrg_PZ_M2_PZGL',41,'','M',0,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (42,'StoreConf','分店配置','41','','PZ_M3_FD',42,'StoreConf/store_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (43,'StoreConf.add','新增分店','42','','PZ_PZGL_XZFD',43,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (44,'StoreConf.edit','编辑分店','42','','PZ_PZGL_BJFD',44,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (45,'StoreConf.setting','设置分店配置','42','','PZ_PZGL_PZFD',45,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (46,'StoreConf.delete','删除分店','42','','PZ_PZGL_SCFD',46,'','O',3,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (47,'','员工','0','','M1_YG',47,'','M',0,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (48,'','员工管理','47','','POrg_YG_M2_YGGL',48,'','M',0,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (49,'UserEmployee','员工管理','48','','POrg_YG_M3_YGGL',49,'UserEmployee/user_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (50,'Role-index','角色管理','48','','YH_M3_JSGL',50,'Role/role_list','M',2,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (51,'','公告','0','','M1_GG',51,'','M',0,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (52,'','公告管理','51','','POrg_GG_M2_GGGL',52,'','M',0,1);",
            "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (53,'Notices','公告管理','52','','POrg_GG_M3_GGGL',53,'Notices/document_list','M',2,1);",
        );
        $advm->patchQuery($rule_sqls);
        return true;
    }

    /**
     * 建立站点基本资料
     * @param type $site_id
     * @param type $user_name
     * @return boolean
     * @since 1.0 2016-5-12 SoChishun Added.
     */
    static function ensure_site($site_id, $user_name) {
        $advm = new \Think\Model\AdvModel();
        if ($advm->table('t_porg_user')->where(array('site_id' => $site_id))->count() > 0) {
            return false; // 已经初始化过了就不要再初始化
        }
        // 自动创建管理员帐号 2016-4-22
        $data_sqls = array(
            "insert into t_porg_user (user_name, password, type_name, site_id, regist_ip, role_ids, status) values ('sa','123456', 'SYSTEMADMIN', 0, '127.0.0.1', 1, 1),('$user_name','123456', 'ADMIN', $site_id, '127.0.0.1', 2, 1);",
            "insert into think_auth_group (title, rules, site_id) values ('系统管理员','*',$site_id),('总管','*',$site_id),('录单员','*',$site_id),('录单员','1,2,3,4,8,9,11,13,14,15,18,19,20,21,24,25,26,28,29,30,32,33,34,36,37,38',$site_id);",
        );
        $advm->patchQuery($data_sqls);
        return true;
    }

}
