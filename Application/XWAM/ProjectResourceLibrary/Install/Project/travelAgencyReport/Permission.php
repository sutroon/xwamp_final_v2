<?php

/*
 * 权限生成工具
 * <br />运行方式：http://localhost:8801/Tool/Incubator/SuPermissionSql_PHP_1_0/SoPermissionSql.php
 * 支持字段：[string]title:标题,[string]name:名称,[string]code:代码,[string]type:节点类型(M=菜单,O=操作,E=页面元素),[boolean]enable:是否可用,[string]url:链接地址,[array]children:子项目,[string]comment:注解
 * 说明：默认ismenu=true,如要特殊声明，需强制加上ismenu=false
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @since 4.0 <2015-9-19> SoChishun 重构以适合RBAC自动拦截规则
 */
return array(
    /* 门店模块 */
    array(
        'title' => '门店',
        'code' => 'M1_MD',
        'type' => 'F',
        'children' => array(
            array(
                'title' => '门店管理',
                'code' => 'POrg_MD_M2_MDGL',
                'children' => array(
                    array('name' => 'StoreDaySales', 'level' => '2', 'title' => '日销售表', 'code' => 'MD_M3_RXSB', 'url' => 'StoreDaySales/daysales_list',
                        'O' => array(
                            array('name' => 'StoreDaySales.add', 'level' => '3', 'title' => '新增日销售表', 'code' => 'MD_RXSB_XZRXSB'),
                            array('name' => 'StoreDaySales.edit', 'level' => '3', 'title' => '编辑日销售表', 'code' => 'MD_RXSB_BJRXSB'),
                            array('name' => 'StoreDaySales.delete', 'level' => '3', 'title' => '删除日销售表', 'code' => 'MD_RXSB_SCRXSB'),
                        ),),
                    array('name' => 'CommissionDiscount', 'level' => '2', 'title' => '返佣表', 'code' => 'MD_M3_FYB', 'url' => 'CommissionDiscount/commission_discount_list'),
                    array('name' => 'ProductGoods', 'level' => '2', 'title' => '商品管理', 'code' => 'MD_M3_SPGL', 'url' => 'ProductGoods/product_goods_list',
                        'O' => array(
                            array('name' => 'ProductGoods.add', 'level' => '3', 'title' => '新增商品', 'code' => 'MD_SPGL_XZSP'),
                            array('name' => 'ProductGoods.edit', 'level' => '3', 'title' => '编辑商品', 'code' => 'MD_SPGL_BJSP'),
                            array('name' => 'ProductGoods.setting', 'level' => '3', 'title' => '商品类别管理', 'code' => 'MD_SPGL_LBGL'),
                            array('name' => 'ProductGoods.delete', 'level' => '3', 'title' => '删除商品', 'code' => 'MD_SPGL_SCSP'),
                        ),),
                    array('name' => 'ProductInvoicing', 'level' => '2', 'title' => '出入库管理', 'code' => 'MD_M3_CRKGL', 'url' => 'ProductInvoicing/invoicing_list',
                        'O' => array(
                            array('name' => 'ProductInvoicing.add', 'level' => '3', 'title' => '新增出库', 'code' => 'MD_CRKGL_XZCK'),
                            array('name' => 'ProductInvoicing.add', 'level' => '3', 'title' => '新增入库', 'code' => 'MD_CRKGL_XZRK'),
                            array('name' => 'ProductInvoicing.delete', 'level' => '3', 'title' => '删除出库', 'code' => 'MD_CRKGL_SCCK'),
                            array('name' => 'ProductInvoicing.delete', 'level' => '3', 'title' => '删除入库', 'code' => 'MD_CRKGL_SCRK'),
                        ),
                    ),
                    array('name' => 'DataAnalysis', 'level' => '2', 'title' => '数据分析', 'code' => 'MD_M3_SJFX', 'url' => 'DataAnalysis/data_analysis_list'),
                    array('name' => 'TravelAgencyConf', 'level' => '2', 'title' => '旅行社配置', 'code' => 'PZ_M3_LXS', 'url' => 'TravelAgencyConf/travel_agency_list',
                        'O' => array(
                            array('name' => 'TravelAgencyConf.add', 'level' => '3', 'title' => '新增旅行社', 'code' => 'PZ_PZGL_XZLXS'),
                            array('name' => 'TravelAgencyConf.edit', 'level' => '3', 'title' => '编辑旅行社', 'code' => 'PZ_PZGL_BJLXS'),
                            array('name' => 'TravelAgencyConf.setting', 'level' => '3', 'title' => '配置佣金比例', 'code' => 'PZ_PZGL_PZLXS'),
                            array('name' => 'TravelAgencyConf.delete', 'level' => '3', 'title' => '删除旅行社', 'code' => 'PZ_PZGL_SCLXS'),
                        ),
                    ),
                    array('name' => 'InterpreterConf', 'level' => '2', 'title' => '讲解员配置', 'code' => 'PZ_M3_JJY', 'url' => 'InterpreterConf/interpreter_list',
                        'O' => array(
                            array('name' => 'InterpreterConf.add', 'level' => '3', 'title' => '新增讲解员', 'code' => 'PZ_PZGL_XZJJY'),
                            array('name' => 'InterpreterConf.edit', 'level' => '3', 'title' => '编辑讲解员', 'code' => 'PZ_PZGL_BJJJY'),
                            array('name' => 'InterpreterConf.delete', 'level' => '3', 'title' => '删除讲解员', 'code' => 'PZ_PZGL_SCJJY'),
                        ),
                    ),
                    array('name' => 'InterpretationRoomConf', 'level' => '2', 'title' => '讲解厅配置', 'code' => 'PZ_M3_JJT', 'url' => 'InterpretationRoomConf/interpretation_room_list',
                        'O' => array(
                            array('name' => 'InterpretationRoomConf.add', 'level' => '3', 'title' => '新增讲解厅', 'code' => 'PZ_PZGL_XZJJT'),
                            array('name' => 'InterpretationRoomConf.edit', 'level' => '3', 'title' => '编辑讲解厅', 'code' => 'PZ_PZGL_BJJJT'),
                            array('name' => 'InterpretationRoomConf.delete', 'level' => '3', 'title' => '删除讲解厅', 'code' => 'PZ_PZGL_SCJJT'),
                        ),
                    ),
                    array('name' => 'GuideConf', 'level' => '2', 'title' => '导游员配置', 'code' => 'PZ_M3_DYY', 'url' => 'GuideConf/guide_list',
                        'O' => array(
                            array('name' => 'GuideConf.add', 'level' => '3', 'title' => '新增导游员', 'code' => 'PZ_PZGL_XZDYY'),
                            array('name' => 'GuideConf.edit', 'level' => '3', 'title' => '编辑导游员', 'code' => 'PZ_PZGL_BJDYY'),
                            array('name' => 'GuideConf.delete', 'level' => '3', 'title' => '删除导游员', 'code' => 'PZ_PZGL_SCDYY'),
                        ),
                    ),
                    array('name' => 'TouristTypeConf', 'level' => '2', 'title' => '客源类型配置', 'code' => 'PZ_M3_KYLX', 'url' => 'TouristTypeConf/tourist_type_list',
                        'O' => array(
                            array('name' => 'TouristTypeConf.add', 'level' => '3', 'title' => '新增客源类型', 'code' => 'PZ_PZGL_XZKYLX'),
                            array('name' => 'TouristTypeConf.edit', 'level' => '3', 'title' => '编辑客源类型', 'code' => 'PZ_PZGL_BJKYLX'),
                            array('name' => 'TouristTypeConf.delete', 'level' => '3', 'title' => '删除客源类型', 'code' => 'PZ_PZGL_SCKYLX'),
                        ),
                    ),
                ),
            ),
        )
    ),
    /* 配置模块 */
    array(
        'title' => '配置',
        'code' => 'M1_PZ',
        'children' => array(
            array(
                'title' => '配置管理',
                'code' => 'POrg_PZ_M2_PZGL',
                'children' => array(
                    array('name' => 'StoreConf', 'level' => '2', 'title' => '分店配置', 'code' => 'PZ_M3_FD', 'url' => 'StoreConf/store_list',
                        'O' => array(
                            array('name' => 'StoreConf.add', 'level' => '3', 'title' => '新增分店', 'code' => 'PZ_PZGL_XZFD'),
                            array('name' => 'StoreConf.edit', 'level' => '3', 'title' => '编辑分店', 'code' => 'PZ_PZGL_BJFD'),
                            array('name' => 'StoreConf.setting', 'level' => '3', 'title' => '设置分店配置', 'code' => 'PZ_PZGL_PZFD'),
                            array('name' => 'StoreConf.delete', 'level' => '3', 'title' => '删除分店', 'code' => 'PZ_PZGL_SCFD'),
                        ),
                    ),
                ),
            ),
        ),
    ),
    /* 员工模块 */
    array(
        'title' => '员工',
        'code' => 'M1_YG',
        'children' => array(
            array(
                'title' => '员工管理',
                'code' => 'POrg_YG_M2_YGGL',
                'children' => array(
                    array('name' => 'UserEmployee', 'level' => '2', 'title' => '员工管理', 'code' => 'POrg_YG_M3_YGGL', 'url' => 'UserEmployee/user_list'),
                    array('name' => 'Role-index', 'level' => '2', 'title' => '角色管理', 'code' => 'YH_M3_JSGL', 'url' => 'Role/role_list'),
                ),
            ),
        )
    ),
    array(
        'title' => '公告',
        'code' => 'M1_GG',
        'children' => array(
            array(
                'title' => '公告管理',
                'code' => 'POrg_GG_M2_GGGL',
                'children' => array(
                    array('name' => 'Notices', 'level' => '2', 'title' => '公告管理', 'code' => 'POrg_GG_M3_GGGL', 'url' => 'Notices/document_list'),
                ),
            ),
        )
    ),
);
