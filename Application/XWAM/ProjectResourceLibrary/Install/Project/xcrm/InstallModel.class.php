<?php

namespace XWAM\Model;

/**
 * 主平台环境检测模型类
 * <br />检测PHP版本、MySQL版本，自动安装平台依赖数据表
 *
 * @since 1.0 <2016-4-22> SoChishun <14507247@qq.com> Added.
 * @since 2.0 <2016-5-10> SoChishun 重构, SysDBModel 重命名为 InstallModel.
 */
class InstallModel {

    /**
     * 检测数据库是否存在
     * @param type $site_id
     * @return type
     * @since 1.0 <2016-4-22> SoChishun <14507247@qq.com> Added.
     * @since 2.0 <2016-5-10> SoChishun 重构.
     */
    public static function check_db_exists($site_id = 0) {
        if (!in_array('mysql', get_loaded_extensions())) {
            die('系统不支持mysql数据库扩展，请联系管理员开启!');
        }
        $link = mysql_connect(C('DB_HOST'), C('DB_USER'), C('DB_PWD')) or die(mysql_error());
        mysql_query('set names ' . C('DB_CHARSET'));
        //$db_name = 'db_xcrm_u' . $site_id;
        $db_name = C('DB_NAME');
        $result = mysql_query("show databases like '$db_name';", $link) or die(mysql_error());
        $row = mysql_fetch_array($result);
        if (!$row) {
            mysql_query("CREATE DATABASE IF NOT EXISTS $db_name DEFAULT CHARSET utf8 COLLATE utf8_general_ci;") or die(mysql_error()); // 创建数据库
        }
        mysql_free_result($result);
        mysql_close($link);
        return $row ? true : false;
    }

    // 2016-4-22
    public static function ensure_database($site_id, $user_name) {
        $advm = new \Think\Model\AdvModel();
        // 执行创建数据表的脚本
        $table_sqls = array(
// t_porg_area
            "create table if not exists t_porg_area ( id int auto_increment primary key, area_name varchar(32) not null, pid int not null default 0, zip_code varchar(8) not null default '', INDEX idx_area_name (area_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_number_regional
            "create table if not exists t_porg_number_regional ( id int auto_increment primary key, area_name varchar(32) not null, segment_number varchar(16) not null, segment_number_len3 char(3) not null default '', segment_number_length smallint not null default 0, number_length smallint not null, sort int not null default 0, status smallint not null default 0, INDEX idx_area_name (area_name), INDEX idx_segment_number_length (segment_number_length), INDEX idx_segment_number_len3 (segment_number_len3) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_callrecord
            "create table if not exists t_porg_callrecord( id int auto_increment primary key, line varchar(32) not null default '', exten varchar(16) not null default '', customer_no varchar(32) not null default '', customer_name varchar(16) not null default '', caller_no varchar(16) not null default '', called_no varchar(16) not null default '', start_time datetime not null default 0, end_time datetime not null default 0, duration int not null default 0, billsec int not null default 0, direction smallint not null default 0, recording_file varchar(255) not null default '', is_shared char(1) not null default 'N', create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_exten (exten), INDEX idx_line (line) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_customer
            "create table if not exists t_porg_customer ( id int auto_increment primary key, serial_no varchar(32) not null default '', `name` varchar(16) not null, sex varchar(6) not null default '', face_url varchar(255) default '', marital_status varchar(9) not null default '未知', birthday datetime, id_type varchar(16) not null default '', id_no varchar(32) not null default '', telphone varchar(32) not null default '', mobile varchar(32) not null default '', address varchar(200) not null default '', zip varchar(8) not null default '', vip_level varchar(16) not null default '', buy_count int not null default 0, integral int not null default 0, total_amount decimal(9,2) not null default 0.00, buy_time datetime, review_time datetime, interested varchar(4) not null default '', contact_result varchar(16) not null default '', `source` varchar(16) not null default '', user_name varchar(16) not null default '', old_user_name varchar(16) not null default '', referrer_user_id int not null default 0, labels varchar(32) not null default '', is_shared char(1) not null default 'N', `status` smallint not null default 0, remark varchar(512) not null default '', create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_name (`name`), INDEX idxu_telphone (telphone), UNIQUE INDEX idx_serial_no (serial_no), INDEX idx_user_name (user_name) ) ENGINE=MyISAM default CHARSET=utf8;",
// t_porg_customer_addr
            "create table if not exists t_porg_customer_addr ( id int auto_increment primary key, customer_id int not null, `name` varchar(32) not null, country varchar(32) not null default '', province varchar(32) not null default '', city varchar(32) not null default '', area varchar(32) not null default '', street varchar(200) not null default '', zip varchar(8) not null default '', telphone varchar(32) not null default '', mobile varchar(32) not null default '', is_default char(1) not null default 0, sort smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_customer_id (customer_id), INDEX idxu_telphone_street (telphone,street) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_customer_appointment
            "create table if not exists t_porg_customer_appointment( id int auto_increment primary key, customer_id int not null, appointment_type varchar(8) not null default '', appointment_time datetime, appointment_remark varchar(160) not null default '', review_time datetime, review_result varchar(180) not null default '', review_remark varchar(160) not null default '', `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idxu_main (customer_id,appointment_type,appointment_time) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_customer_attr
            "create table if not exists t_porg_customer_attr ( id int auto_increment primary key, type_id int not null default 0, group_name varchar(16) not null default '', is_system char(1) not null default 'Y', is_editable char(1) not null default 'Y', title varchar(16) not null default '', help_text varchar(16) not null default '', field_name varchar(16) not null default '', field_type varchar(16) not null default 'varchar', field_length varchar(8) not null default '', field_decimals char(1) not null default '', field_default varchar(32) not null default '', field_comment varchar(128) not null default '', field_script varchar(128) not null default '', is_form char(1) not null default 'N', is_list char(1) not null default 'N', is_search char(1) not null default 'N', is_import char(1) not null default 'N', is_export char(1) not null default 'N', is_popup char(1) not null default 'N', options_content varchar(128) not null default '', options_input_type varchar(16) not null default '', sort smallint not null default 0, status smallint not null default 1, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_site_id (site_id) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_customer_dispatch
            "create table if not exists t_porg_customer_dispatch( id int auto_increment primary key, title varchar(32) not null default '', customer_id int not null, old_user_name varchar(16) not null, new_user_name varchar(16) not null, admin_name varchar(16) not null, dispatch_remark varchar(32) not null default '', status smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, INDEX idx_customer_id (customer_id), INDEX idx_title (title), INDEX idx_new_user_name (new_user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_customer_tag
            "create table if not exists t_porg_customer_tag( id int auto_increment primary key, title varchar(32) not null, user_name varchar(16) not null default '', sort smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_user_name (user_name), UNIQUE INDEX idxu_title (title) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_advertising
            "create table if not exists t_porg_advertising ( id int auto_increment primary key, location varchar(32) not null, channel varchar(32) not null, `name` varchar(32) not null, title varchar(64) not null, code varchar(16) not null, `type` char(3) not null default 'B', `count` smallint not null default 1, image_width smallint not null default 0, image_height smallint not null default 0, file_type varchar(32) not null default 'jpg,gif,png', file_size smallint not null default 50, user_name varchar(32) not null default '', remark varchar(32) not null default '', `status` smallint not null default 1, create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime not null default 0, site_id int not null default 0, UNIQUE INDEX idxu_code (`code`), INDEX idx_title (title), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_advertising_content
            "create table if not exists t_porg_advertising_content ( id int auto_increment primary key, advertising_id int not null, title varchar(32) not null default '', file_name varchar(32) not null default '', file_path varchar(255) not null default '', file_size smallint not null, file_type varchar(32) not null default '', file_ext varchar(8) not null default '', image_width int not null default 0, image_height int not null default 0, link_url varchar(255) not null default '', visit_count int not null default 0, click_count int not null default 0, is_period_time smallint not null default 0, duration_time smallint not null default 0, duration_unit char(1) not null default '', begin_time datetime, end_time datetime, params varchar(64) not null default '', remark varchar(32) not null default '', sort smallint not null default 0, `status` smallint not null default 1, create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime not null default 0, INDEX idx_title (title), INDEX idx_advertising_id (advertising_id) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_document
            "create table if not exists t_porg_document ( id int auto_increment primary key, title varchar(32) not null, category_id int not null default 0, summary varchar(128) not null default '', author varchar(32) not null default '', `source` varchar(255) not null default '', meta_keywords varchar(32) not null default '', meta_description varchar(128) not null default '', picture_url varchar(255) not null default '', visit_count int not null default 0, reply_count int not null default 0, user_name varchar(32) not null default '', access varchar(512) not null default '', content_tags varchar(16) not null default '', ex_tags varchar(16) not null default '', sort smallint not null default 0, `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime not null default 0, site_id int not null default 0, INDEX idx_title (title), INDEX idx_category_id (category_id), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_document_category
            "create table if not exists t_porg_document_category ( id int auto_increment primary key, title varchar(32) not null, pid int not null default 0, type_id int not null default 0, item_count int not null default 0, visit_count int not null default 0, picture_url varchar(255) not null default '', link_url varchar(255) not null default '', user_name varchar(32) not null default '', access varchar(255) not null default '', remark varchar(32) not null default '', sort smallint not null default 0, `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_pid (pid) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_document_category_content
            "create table if not exists t_porg_document_category_content ( category_id int primary key, summary varchar(128) not null default '', content varchar(20480) not null default '' ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_document_category_link
            "create table if not exists t_porg_document_category_link ( category_id int primary key, url varchar(128) not null default '', target varchar(8) not null default '' ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_document_content
            "create table if not exists t_porg_document_content ( document_id int primary key, content varchar(20480) not null default '' ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_file
            "create table if not exists t_porg_file ( id int auto_increment primary key, title varchar(32) not null default '', picture_url varchar(255) not null default '', category_id int not null default 0, file_name varchar(32) not null default '', file_path varchar(255) not null default '', file_size varchar(32) not null default '', file_type varchar(32) not null default '', file_ext varchar(8) not null default '', file_width int not null default 0, file_height int not null default 0, summary varchar(255) not null default '', visit_count int not null default 0, reply_count int not null default 0, download_count int not null default 0, access varchar(128) not null default '', user_name varchar(32) not null default '', params varchar(64) not null default '', remark varchar(32) not null default '', ex_tags varchar(16) not null default '', sort smallint not null default 0, `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_knowledge
            "create table if not exists t_porg_knowledge ( id int auto_increment primary key, title varchar(32) not null, category_id int not null default 0, visit_count int not null default 0, star smallint not null default 0, content varchar(20480) not null default '', user_name varchar(32) not null default '', is_shared smallint not null default 0, content_tags varchar(32) not null default '', sort smallint not null default 0, `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime not null default 0, site_id int not null default 0, INDEX idx_title (title), INDEX idx_content_tags (content_tags), INDEX idx_category_id (category_id), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_knowledge_category
            "create table if not exists t_porg_knowledge_category ( id int auto_increment primary key, title varchar(32) not null, pid int not null default 0, user_name varchar(32) not null default '', remark varchar(32) not null default '', sort smallint not null default 0, `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_pid (pid), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_link
            "create table if not exists t_porg_link ( id int auto_increment primary key, title varchar(32) not null, link_url varchar(255) not null, `type` smallint not null default 1, image_url varchar(255) not null default '', file_size varchar(32) not null default '', remark varchar(128) not null default '', user_name varchar(32) not null, sort smallint not null default 0, `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_notices
            "create table if not exists t_porg_notices ( id int auto_increment primary key, title varchar(32) not null, category_id int not null default 0, meta_keywords varchar(32) not null default '', meta_description varchar(128) not null default '', picture_url varchar(255) not null default '', visit_count int not null default 0, content varchar(20480) not null default '', user_name varchar(32) not null default '', content_tags varchar(16) not null default '', sort smallint not null default 0, `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime not null default 0, site_id int not null default 0, INDEX idx_title (title), INDEX idx_category_id (category_id) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_config
            "create table if not exists t_porg_config ( id int auto_increment primary key, `name` varchar(32) not null, title varchar(32) not null default '', `type` char(1) not null default 'C', `group` char(1) not null default '', prop char(1) not null default 'C', content varchar(128) not null default '', lcontent varchar(12800) not null default '', remark varchar(32) not null default '', user_name varchar(32) not null default '', status smallint not null default 0, sort smallint not null default 0, create_time timestamp not null default current_timestamp, site_id int not null default 0, update_time datetime, UNIQUE INDEX idxu_name (`name`), INDEX `idx_group` (`group`), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_site_conf
            "create table if not exists t_porg_site_conf ( id int auto_increment primary key, site_title varchar(32) not null, site_code varchar(16) not null, site_app_name varchar(32) not null default 'XWAMP', site_app_version varchar(16) not null default '1.0.0.0', site_status char(1) not null default 'Y', site_closed_announcement varchar(255) not null default '', site_logo_url varchar(255) not null default '', site_icp_code varchar(32) not null default '', site_summary varchar(128) not null default '', site_home_url varchar(128) not null default 'index', site_login_url varchar(128) not null default 'login', site_db_sharding char(1) not null default '', site_sub_path varchar(16) not null default '/xcrm', site_category_id int not null default 0, site_company_name varchar(32) not null default '', visit_count int not null default 0, session_prefix varchar(32) not null default '', encrypt_key varchar(16) not null default 'sue123!@#', ui_login_captcha_status char(1) not null default 'Y', ui_aside_status char(1) not null default 'Y', content_draft_aotosave_interval smallint not null default 0, content_draftbox_status char(1) not null default 'N', content_list_rows smallint not null default 25, content_reply_list_rows smallint not null default 25, visit_allow_register char(1) not null default 'Y', data_backup_path varchar(255) not null default '', data_backup_part_size varchar(16) not null default '2M', admin_email varchar(32) not null default '', admin_name varchar(32) not null default '', admin_telphone varchar(32) not null default '', admin_mobile varchar(32) not null default '', admin_address varchar(255) not null default '', cti_screenpop_mode char(1) not null default 'M', cti_screenpop_duplicate_expire smallint not null default 180, user_name varchar(32) not null default '', sort smallint not null default 0, ex_tags varchar(16) not null default '', create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime, status smallint not null default 0, INDEX idx_site_title (site_title), INDEX idx_site_category_id (site_category_id), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_site_content
            "create table if not exists t_porg_site_content ( id int auto_increment primary key, site_id int not null, `name` varchar(32) not null, title varchar(64) not null default '', content varchar(20000) not null default '', create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime not null default 0, INDEX idx_site_id (site_id), INDEX idx_name (`name`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// think_auth_department
            "create table if not exists think_auth_department( id int auto_increment primary key, title varchar(32) not null, pid int not null default 0, `code` varchar(32) not null default '', rules varchar(128) not null default '', sort smallint not null default 0, remark varchar(32) not null default '', `status` smallint not null default 1, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_pid (pid) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// think_auth_group
            "create table if not exists think_auth_group ( id int auto_increment primary key, title varchar(30) not null default '', display_title varchar(30) not null default '', code varchar(32) not null default '', rules varchar(800) not null default '', remark varchar(32) not null default '', sort smallint not null default 0, `status` smallint not null default 1, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0 ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// think_auth_rule
            "create table if not exists think_auth_rule ( id int auto_increment primary key, `name` varchar(64) not null default '', title varchar(50) not null, code varchar(32) not null default '', pid smallint(6) unsigned not null, addon varchar(32) not null default '', `level` tinyint(1) unsigned not null, `type` varchar(16) not null default '', url varchar(64) not null default '', `condition` varchar(128) not null default '', remark varchar(32) not null default '', sort smallint not null default 0, status smallint not null default '0', create_time timestamp not null default CURRENT_TIMESTAMP, INDEX ix_code (code), INDEX ix_pid (pid) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_pm
            "create table if not exists t_porg_pm ( id int auto_increment primary key, category_id int not null default 0, from_user varchar(16) not null, to_user varchar(128) not null, from_alias varchar(32) not null default '', subject varchar(32) not null default '', body varchar(2048) not null default '', `status` smallint not null default 0, read_status char(1) not null default 'N', delete_status varchar(1) not null default 'N', star smallint not null default 0, read_time datetime, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_from_user (from_user,status), INDEX idx_to_user (to_user,delete_status) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_pm_category
            "create table if not exists t_porg_pm_category ( id int auto_increment primary key, title varchar(32) not null, code varchar(16) not null default '', pid int not null default 0, user_name varchar(32) not null default '', remark varchar(32) not null default '', sort smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_pid (pid), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_user
            "create table if not exists t_porg_user ( id int auto_increment primary key, user_name varchar(32) not null, password varchar(128) not null, type_id smallint not null default 0, type_name varchar(16) not null default '', role_ids varchar(32) not null default '', department_ids varchar(32) not null default '', status smallint not null default 0, site_id int not null default 0, unreadmsg smallint not null default 0, trade_password varchar(128) not null default '', `name` varchar(32) not null default '', sex char(1) not null default 'F', face_url varchar(255) not null default '', marital_status char(1) not null default 'N', birthday datetime, id_type varchar(16) not null default '', id_no varchar(32) not null default '', education varchar(16) not null default '', company_name varchar(16) not null default '', company_type varchar(16) not null default '', vocation varchar(32) not null default '', `position` varchar(32) not null default '', extension varchar(16) not null default '', telphone varchar(18) not null default '', mobile varchar(18) not null default '', email varchar(32) not null default '', fax varchar(32) not null default '', qq varchar(16) not null default '', taobao varchar(32) not null default '', country varchar(32) not null default '', province varchar(32) not null default '', city varchar(32) not null default '', area varchar(32) not null default '', street varchar(128) not null default '', zip varchar(8) not null default '', level_id smallint not null default 0, advance decimal(20,3) not null default 0.000, advance_freeze decimal(20,3) not null default 0.000, point int unsigned not null default 0, point_freeze int unsigned not null default 0, point_history int unsigned not null default 0, score_rate decimal(5,3) not null default 0, experience int not null default 0, theme varchar(16) not null default '', lang varchar(8) not null default '', page_size smallint not null default 0, personal_sign varchar(128) not null default '', custom_title varchar(16) not null default '', screenpopup_mode char(1) not null default '', remark varchar(32) not null default '', regist_refer varchar(16) not null default 'local', regist_ip varchar(16) not null default '', pw_answer varchar(32) not null default '', pw_question varchar(32) not null default '', login_ip varchar(16) not null default '', login_count int not null default 0, login_time datetime, login_time_temp datetime, online_status smallint not null default 0, online_active_time datetime, sort smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime, UNIQUE INDEX idxu_user_name (user_name), INDEX idx_name (`name`), INDEX idx_mobile (mobile), INDEX idx_email (email) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
// t_porg_user_addr
            "create table if not exists t_porg_user_addr ( id int auto_increment primary key, user_name varchar(32) not null default '', `name` varchar(32) not null, country varchar(32) not null default '', province varchar(32) not null default '', city varchar(32) not null default '', area varchar(32) not null default '', street varchar(255) not null default '', zip varchar(8) not null default '', telphone varchar(32) not null default '', mobile varchar(32) not null default '', is_default char(1) not null default 0, sort smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_user_cti
            "create table if not exists t_porg_user_cti ( id int auto_increment primary key, user_id int not null, extension varchar(16) not null, secret varchar(16) not null default '', queue varchar(8) not null default '', trunk varchar(16) not null default '', trunk_mode varchar(12) not null default '', caller_no varchar(16) not null default '', leader_model varchar(8) not null default '', forwarding_extension varchar(16) not null default '', forwarding_state smallint not null default 0, max_task int not null default 0, keep_call_history_days int not null default 0, keep_record_history_days int not null default 0, out_fee_rate int not null default 0, in_fee_rate int not null default 0, sms_fee_rate int not null default 0, overdraft_amount decimal(12,4) not null default 0.0000, blance decimal(12,4) not null default 0.0000, pay_type smallint not null default 1, rec_state smallint not null default 1, dtmfmode varchar(255) not null default '', canreinvite varchar(32) not null default '', dial_opts_cb smallint not null default 0, dial_opts varchar(32) not null default '', screenpopup_mode char(1) not null default 'F', status smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, sys_code varchar(128) not null default '', INDEX idx_user_id (user_id), INDEX idx_extension (extension) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_user_favorite
            "create table if not exists t_porg_user_favorite ( id int auto_increment primary key, record_id int not null default 0, category_id int not null default 0, star smallint not null default 0, remark varchar(255) not null default '', user_name varchar(32) not null default '', sort smallint not null default 0, `status` smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, index idx_user_name (user_name), index idx_record_id (record_id) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_user_payment
            "create table if not exists t_porg_user_payment ( id int auto_increment primary key, bank_name varchar(32) not null default '', bank_card_no varchar(32) not null default '', bank_card_holder varchar(32) not null default '', user_name varchar(32) not null default '', usage_count int not null default 0, sort smallint not null default 0, create_time timestamp not null default CURRENT_TIMESTAMP, site_id int not null default 0, INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
// t_porg_user_subscribe
            "create table if not exists t_porg_user_subscribe ( id int auto_increment primary key, email varchar(32) not null default '', user_name varchar(32) not null default '', subscribe varchar(800) not null default '', create_time timestamp not null default CURRENT_TIMESTAMP, update_time datetime, site_id int not null default 0, INDEX idx_email (email), INDEX idx_user_name (user_name) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
        );
        $advm->patchQuery($table_sqls);
        $view_sqls = array(
// v_porg_customer_appointment
            "create or replace view v_porg_customer_appointment as select c.serial_no , c.name , c.sex , c.telphone , c.address , c.buy_count , c.total_amount , c.buy_time , c.user_name , c.labels , c.create_time , c.site_id , a.id , a.customer_id , a.appointment_type , a.appointment_time , a.appointment_remark , a.review_time , a.review_result , a.review_remark , a.status from t_porg_customer_appointment a left join t_porg_customer c on a.customer_id=c.id order by id desc;",
// v_porg_customer_dispatch
            "create or replace view v_porg_customer_dispatch as select c.serial_no , c.name , c.sex , c.telphone , c.mobile , c.address , c.buy_count , c.total_amount , c.buy_time , c.review_time , c.interested , c.site_id , d.id , d.title , d.customer_id , d.old_user_name , d.new_user_name , d.admin_name , d.dispatch_remark , d.status , d.create_time from t_porg_customer_dispatch d left join t_porg_customer c on d.customer_id=c.id order by id desc;",
        );
        $advm->patchQuery($view_sqls);
        // 自动创建管理员帐号 2016-4-22
        $data_sqls = array(
            "insert into t_porg_user (user_name, password, type_name, site_id, regist_ip, role_ids, status) values ('sa','123456', 'SYSTEMADMIN', 0, '127.0.0.1', 1, 1),('admin','123456', 'SITEADMIN', 0, '127.0.0.1', 1, 1),('$user_name','123456', 'ADMIN', $site_id, '127.0.0.1', 2, 1);",
            "insert into think_auth_group (title, rules, site_id) values ('系统管理员','*',$site_id),('站点创始人','*',$site_id),('坐席','18,19,20,21,22,23,24,26,27,28,29,30,31,34,35,36,37,38,40,41',$site_id);",
        );
        $advm->patchQuery($data_sqls);
        // 权限规则
        $rule_sqls = array(
            "INSERT INTO `think_auth_rule` VALUES (1, '', '系统', 'M1_XT', 0, '', 0, 'M', '', '', '', 1, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (2, '', '系统设置', 'POrg_XT_M2_XTSZ', 1, '', 0, 'M', '', '', '', 2, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (3, 'SiteConfig-index', '网站设置', 'POrg_XT_M3_WZSZ', 2, '', 2, 'M', 'SiteConfig/site_edit', '', '', 3, 1, '2016-4-8 17:09:25');",
            //"INSERT INTO `think_auth_rule` VALUES (4, 'VarConfig-index', '变量配置', 'POrg_XT_M3_BLPZ', 2, '', 2, 'M', 'VarConfig/var_config', '', '', 4, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (5, '', '站务管理', 'POrg_XT_M2_ZQGL', 1, '', 0, 'M', '', '', '', 5, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (6, 'PM-pm_list', '站内信管理', 'POrg_XT_M3_ZNXGL', 5, '', 2, 'M', 'PM/pm_list', '', '', 6, 1, '2016-4-8 17:09:25');",
            //"INSERT INTO `think_auth_rule` VALUES (7, 'VarConfig-index', '问题反馈', 'POrg_XT_M3_WTFK', 5, '', 2, 'M', 'VarConfig/var_config', '', '', 7, 1, '2016-4-8 17:09:25');",
            //"INSERT INTO `think_auth_rule` VALUES (8, '', '网站安全', 'POrg_XT_M2_WZAQ', 1, '', 0, 'M', '', '', '', 8, 1, '2016-4-8 17:09:25');",
            //"INSERT INTO `think_auth_rule` VALUES (9, 'SiteBackup-index', '数据备份', 'POrg_XT_M3_SJBF', 8, '', 2, 'M', 'SiteBackup/index', '', '', 9, 1, '2016-4-8 17:09:25');",
            //"INSERT INTO `think_auth_rule` VALUES (10, 'SiteReduction-index', '还原数据', 'POrg_XT_M3_HYSJ', 8, '', 2, 'M', 'SiteReduction/index', '', '', 10, 1, '2016-4-8 17:09:25');",
            //"INSERT INTO `think_auth_rule` VALUES (11, 'LogAnalysis-index', '日志分析', 'POrg_XT_M3_RZFX', 8, '', 2, 'M', 'LogAnalysis/index', '', '', 11, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (12, '', '用户', 'M1_YH', 0, '', 0, 'M', '', '', '', 12, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (13, '', '用户管理', 'POrg_YH_M2_YHGL', 12, '', 0, 'M', '', '', '', 13, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (14, 'UserAdmin-user_list', '管理员管理', 'YH_M3_GLYGL', 13, '', 2, 'M', 'UserAdmin/user_list', '', '', 14, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (15, 'UserSeat-user_list', '坐席管理', 'YH_M3_ZXGL', 13, '', 2, 'M', 'UserSeat/user_list', '', '', 15, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (16, 'Department-department_list', '部门管理', 'YH_M3_BMGL', 13, '', 2, 'M', 'Department/department_list', '', '', 16, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (17, 'Role-index', '角色管理', 'YH_M3_JSGL', 13, '', 2, 'M', 'Role/role_list', '', '', 17, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (18, '', '客户', 'M1_KH', 0, '', 0, 'M', '', '', '', 18, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (19, '', '客户管理', 'POrg_KH_M2_KHGL', 18, '', 0, 'M', '', '', '', 19, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (20, 'Customer-customer_list', '客户管理', 'YH_M3_KHGL', 19, '', 2, 'M', 'Customer/customer_list', '', '', 20, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (21, 'Customer-customer_edit', '新增客户', 'KH_KHGL_XZKH', 20, '', 3, 'O', '', '', '', 21, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (22, 'Customer-customer_edit', '编辑客户', 'KH_KHGL_BJKH', 20, '', 3, 'O', '', '', '', 22, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (23, 'Customer-customer_recycle', '放入回收站', 'KH_KHGL_FRHSZ', 20, '', 3, 'O', '', '', '', 23, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (24, 'Customer-customer_delete', '删除客户', 'KH_KHGL_SCKH', 20, '', 3, 'O', '', '', '', 24, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (25, 'Customer-customer_dispatch', '调度客户', 'KH_KHGL_DDKH', 20, '', 3, 'O', '', '', '', 25, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (26, 'CustomerInterested-customer_list', '意向客户', 'KH_M3_BJKHGL', 19, '', 2, 'M', 'CustomerInterested/customer_list', '', '', 26, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (27, 'CustomerAppointment-customer_lis', '预约客户', 'KH_M3_YYKHGL', 19, '', 2, 'M', 'CustomerAppointment/customer_list', '', '', 27, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (28, 'CustomerDispatch-customer_list', '调度客户', 'KH_M3_DDKHGL', 19, '', 2, 'M', 'CustomerDispatch/customer_list', '', '', 28, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (29, 'CustomerOverdue-customer_list', '超期预警', 'KH_M3_CQKHGL', 19, '', 2, 'M', 'CustomerOverdue/customer_list', '', '', 29, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (30, 'CustomerPublic-customer_list', '公共池', 'KH_M3_GGCGL', 19, '', 2, 'M', 'CustomerPublic/customer_list', '', '', 30, 1, '2016-4-8 17:09:25');",
            "INSERT INTO `think_auth_rule` VALUES (31, 'CustomerRecycle-customer_list', '回收站', 'KH_M3_HSZGL', 19, '', 2, 'M', 'CustomerRecycle/customer_list', '', '', 31, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (32, '', '客户配置', 'POrg_KH_M2_KHPZ', 18, '', 0, 'M', '', '', '', 32, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (33, 'CustomerAttr-attr_list', '字段管理', 'KH_M3_ZDGL', 32, '', 2, 'M', 'CustomerAttr/attr_list', '', '', 33, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (34, '', '录音管理', 'POrg_KH_M2_LYGL', 18, '', 0, 'M', '', '', '', 34, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (35, 'CallRecord-record_list', '通话记录', 'KH_M3_THJLGL', 34, '', 2, 'M', 'CallRecord/record_list', '', '', 35, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (36, 'CallRecordShare-share_list', '录音分享', 'KH_M3_LYFXGL', 34, '', 2, 'M', 'CallRecordShare/record_list', '', '', 36, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (37, '', '内容', 'M1_NR', 0, '', 0, 'M', '', '', '', 37, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (38, '', '内容管理', 'POrg_NR_M2_NRGL', 37, '', 0, 'M', '', '', '', 38, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (39, 'Notices-document_list', '公告管理', 'POrg_NR_M3_ZDLM', 38, '', 2, 'M', 'Notices/document_list', '', '', 39, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (40, 'Knowledge-document_list', '知识库', 'POrg_NR_M3_ZDLM', 38, '', 2, 'M', 'Knowledge/document_list', '', '', 40, 1, '2016-4-8 17:09:26');",
            "INSERT INTO `think_auth_rule` VALUES (41, 'Help-index', '帮助文档', 'POrg_NR_M3_BZWD', 38, '', 2, 'M', 'Help/index', '', '', 41, 1, '2016-4-20 15:39:03');",
        );
        $advm->patchQuery($rule_sqls);
        return true;
    }

    /**
     * 建立站点基本资料
     * @param type $site_id
     * @param type $user_name
     * @return boolean
     * @since 1.0 2016-5-12 SoChishun Added.
     */
    static function ensure_site($site_id, $user_name) {
        $advm = new \Think\Model\AdvModel();
        if ($advm->table('t_porg_user')->where(array('site_id' => $site_id))->count() > 0) {
            return false; // 已经初始化过了就不要再初始化
        }
        // 自动创建管理员帐号 2016-4-22
        $data_sqls = array(
            "insert into t_porg_user (user_name, password, type_name, site_id, regist_ip, role_ids, status) values ('$user_name','123456', 'ADMIN', $site_id, '127.0.0.1', 2, 1);",
            "insert into think_auth_group (title, rules, site_id) values ('系统管理员','*',$site_id),('站点创始人','*',$site_id),('坐席','18,19,20,21,22,23,24,26,27,28,29,30,31,34,35,36,37,38,40,41',$site_id);",
        );
        $advm->patchQuery($data_sqls);
        return true;
    }

}
