<?php
/**
 * 数据表创建脚本辅助工具
 * @version 1.0 2016-5-10 SoChishun Added.
 */
$ver = '1.0';
$modules = true ? array('POrgDocument', 'POrgSystem', 'POrgUser','POrgProduct') : array();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>数据表创建脚本辅助工具</title>
        <style type="text/css">
            body{font-size:12px;}
        </style>
    </head>
    <body>
        <a href="?action=view">视图</a>
        <a href="?action=script">脚本</a>
        <div>
            <textarea cols="150" rows="36"><?php
                $action = isset($_GET['action']) ? $_GET['action'] : 'view';
                if ('script' == $action) {
                    echo 'array(', PHP_EOL;
                }
                // 2016-5-10 sochishun added.
                $i = 0;
                $dir = opendir('./');
                while (false !== ($file = readdir($dir))) {
                    if (!is_dir($file) || '.' == $file || '..' == $file) {
                        continue;
                    }
                    if ($modules && !in_array($file, $modules)) {
                        continue;
                    }
                    $path2 = './' . $file . '/Sqls/1_Table';
                    $dir2 = opendir($path2);
                    while (false != ($file2 = readdir($dir2))) {
                        $path3 = $path2 . '/' . $file2;
                        if (is_dir($path3)) {
                            continue;
                        }
                        $str = file_get_contents($path3);
                        if (!$str) {
                            continue;
                        }
                        $i++;
                        $str = preg_replace("/ comment '[^']*'/", '', $str); // comment '...'
                        $str = preg_replace("/ comment='[^']*'/", '', $str); // comment='...'
                        $str = preg_replace("/--[^\n]+/", '', $str); // -- ...
                        $str = preg_replace("/\/\*[^\/]+\//", '', $str); // /*...*/
                        $str = preg_replace("/drop[^;]+;/", '', $str); // drop table ...;
                        if ('script' == $action) {
                            $str = preg_replace('/\n/', '', trim($str)); // 清除字段间断行符号
                            $str = preg_replace('/[\s]+/', ' ', trim($str)); // 多个空格合并成一个
                            echo '\'' . substr($file2, 0, strlen($file2) - 4) . '\' => "', $str, '",', PHP_EOL;
                        } else {
                            echo $str;
                        }
                    }
                    closedir($dir2);
                }
                closedir($dir);

                if ('script' == $action) {
                    echo ');';
                }
                ?></textarea>
        </div>
        <div>共 <?php echo $i ?> 个对象</div>
    </body>
</html>
