HTML编码备注：
Upload目录地址：__ROOT__/Upload/FileTemplate/help.doc
Jquery目录地址：__PUBLIC__/Script/jquery-1.11.1.min.js

数据层：Model/UserModel 用于定义数据相关的自动验证和自动完成和数据存取接口
逻辑层：Logic/UserLogic 用于定义用户相关的业务逻辑
服务层：Service/UserService 用于定义用户相关的服务接口等

UserController负责外部交互响应，通过URL请求响应，例如 http://serverName/User/index,而 UserEvent 负责内部的事件响应，并且只能在内部调用： A('User','Event');

缓存系统开发摘要：

1、菜单缓存：
	菜单缓存基于角色编号,保存为main_nav_r{roleID].php, 因此，对个别工号进行特殊权限勾选的时候，必需保存为扩展角色（角色是分组的）
	更新菜单的时候，重新生成缓存，用户登录后，直接读取缓存，不读数据库，也不对数据进行损耗服务器性能的无谓加工
2、频繁访问数据缓存：
	如siteID对应的用户数据、统计数据等
	
2015-5-19 项目由Crm改为Suwam(Super Utility Web Application Management超实用网页应用管理系统)

2015-6-29 Suwam不够大气，改为WBM (website backstage  management)

2015-7-22 WBM挺别扭，改为XWAM (Extension Web Application Management)

﻿项目目录
每个站点一个独立数据库,数据库规范：
    1.数据库名称：db_{站点名称},如 db_xywy
    2.数据库结构源 xwam

====缓存规范====
缓存引擎：中小数据量用文件缓存, 大数据量用MonoDB缓存
MySQL做好索引
1、场景：用户登录的时候，提交表单后，等待很长时间才能验证是否成功
    方案：将用户名和密码做缓存处理

=================
[2015-9-15]
Conf 
    1. 只包含基本配置，如数据库连接、模板、语言设定
    2. 主配置文件通过USER外置配置支持多个用户切换
    3. Addon的配置会覆盖Conf的配置，包括数据库连接、语言设定
主平台包含RBAC模块(权限节点表、角色/用户组表;角色模块等)

[2015-11-7]
XWAM开发阶段如何与多网站匹配？
方案：
1. 每个网站独立数据库, 匹配哪个网站，只要修改XWAM/Conf/conf.php中的数据库名称就可以了
2. 每个网站的模块配置都在XWAM/Conf/user_{sitename}.php文件设置,比如有的不需要商品模块,就可以隐藏之
