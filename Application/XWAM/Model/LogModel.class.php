<?php

namespace XWAM\Model;

/**
 * 通用事件日志数据库模型
 *
 * @since 1.0 <2015-4-14> SoChishun <14507247@qq.com> Added.
 */
class LogModel extends AppbaseModel {

    // 禁止字段检测
    protected $autoCheckFields = false;

    /**
     * 事件动作-新增
     */
    const ACTION_ADD = 'ADD';

    /**
     * 事件动作-编辑
     */
    const ACTION_EDIT = 'EDIT';

    /**
     * 事件动作-删除
     */
    const ACTION_DELETE = 'DELETE';

    /**
     * 事件动作-查询
     */
    const ACTION_SEARCH = 'SEARCH';

    /**
     * 事件对象-存储过程
     */
    const OBJECT_PROC = 'PROC';

    /**
     * 事件对象-函数
     */
    const OBJECT_FUNC = 'FUNC';

    /**
     * 事件对象-数据表
     */
    const OBJECT_TABEL = 'TABEL';

    /**
     * 事件对象-视图
     */
    const OBJECT_VIEW = 'VIEW';

    /**
     * 事件对象-页面
     */
    const OBJECT_PAGE = 'PAGE';

    /**
     * 事件级别-信息
     */
    const LEVEL_INFO = 'INFO';

    /**
     * 事件级别-错误
     */
    const LEVEL_ERROR = 'ERROR';

    /**
     * 事件级别-调试
     */
    const LEVEL_DEBUG = 'DEBUG';

    /**
     * 事件级别-警告
     */
    const LEVEL_WARINIG = 'WARNING';

    /**
     * 事件级别-攻击
     */
    const LEVEL_ATTACK = 'ATTACK';

    /**
     * 记录用户登录日志
     * @param boolean $status 登录状态
     * @param string $user_name 登录名
     * @param string $password 登录密码
     * @param string $msg 消息
     * @param int $is_login 登录类型(1=登入,2=登出)
     * @since 1.0 <2015-6-10> SoChishun Added.
     * @example 
     *      1.登入:log_user_login(false, $userName, $pwd, '状态有误[' . $data['state'] . ']');
     *      2.登出：log_user_login(true, $login_data['userName'], '', '', 2);
     */
    public function log_user_login($status, $user_name, $password, $msg = '', $is_login = 1) {
        $data = array(
            'user_name' => $user_name,
            'ip' => get_client_ip(),
            'status' => intval($status),
            'is_login' => $is_login,
            'remark' => sprintf('name=%s, pwd=%s %s', $user_name, $password, $msg),
        );
        if ('DB' == C('USER_LOG_TYPE')) {
            $this->table('t_porg_log_user_login')->add($data);
        }
    }

    /**
     * 记录用户操作日志
     * @param type $status 事件状态(1=成功,4=失败)
     * @param type $desc // 描述(如:充值100元),占位符{%}可自动转为字串"成功"或"失败"
     * @param type $user_name 用户名称
     * @param type $action // 事件动作(如:充值、查询、登录、新增、编辑、删除)
     * @param type $level // 事件级别(如:信息、错误、警告、攻击、调试)
     * @since 1.0 <2015-6-10> SoChishun Added.
     * @example log_user_operate(1==$code, '删除管理员账户{%} '.$msg, $admin_id, LogModel::ACTION_DELETE)
     */
    public function log_user_operate($status, $desc, $user_name, $action, $level = '') {
        $data = array(
            'user_name' => $user_name,
            'ip' => get_client_ip(),
            'action' => $action, // 事件动作(如:充值、查询、登录、新增、编辑、删除)
            'object' => '', // 事件对象(如:存储过程、表名、视图名、页面)
            'source' => $_SERVER['REQUEST_URI'], // 事件来源(如:网址,PROCEDURE)
            'level' => $level ? $level : static::LEVEL_INFO, // 事件级别(如:信息、错误、警告、攻击、调试)
            'desc' => false === strpos($desc, '{%}') ? $desc : str_replace('{%}', $status ? '成功' : '失败', $desc), // 描述(如:充值100元)
            'status' => intval($status), // 事件状态(1=成功,4=失败)
        );
        if ('DB' == C('USER_LOG_TYPE')) {
            $this->table('t_porg_log_user_operate')->add($data);
        }
    }

}
