<?php

namespace XWAM\Model;

/**
 * 模型基础类
 *
 * @since 1.0 <2016-5-30> sutroon <14507247@qq.com> Added.
 */
interface ICategoryModel{
    function get_idtext_list();
    function get_idtext_kvlist();
}
