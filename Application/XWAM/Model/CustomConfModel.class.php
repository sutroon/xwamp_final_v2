<?php

namespace XWAM\Model;

/**
 * Description of ConfigModel
 *
 * @since 1.0 <2016-5-26> SoChishun <14507247@qq.com> Added.
 */
class CustomConfModel extends AppbaseModel {

    protected $tableName = 't_porg_custom_conf';

    /**
     * 获取配置列表
     * @param number $store_id
     * @param type $group
     * @return type
     * @since 1.0 <2015-10-20> SoChishun Added.
     */
    function get_list($store_id, $group = '') {
        $list = $this->where(array('store_id' => $store_id, 'group' => $group, 'status' => 1))->cache($group . $store_id . '_list', 10)->order('sort, id')->select();
        return $list;
    }

    /**
     * 获取id_text列表
     * @param number $store_id
     * @param string $group
     * @param boolean $key_is_id 键名是否是ID(默认false)
     * @return type
     * @since 1.0 2016-5-27 SoChishun Added.
     */
    function get_idtext_list($store_id, $group, $key_is_id = false) {
        $list = array();
        if ($key_is_id) {
            $list = $this->where(array('store_id' => $store_id, 'group' => $group, 'status' => 1))->cache($group . $store_id . '_idtext_kvlist', 10)->order('sort, id')->getField('id, title as text');
        } else {
            $list = $this->where(array('store_id' => $store_id, 'group' => $group, 'status' => 1))->cache($group . $store_id . '_idtext_list', 10)->order('sort, id')->field('id,title as text')->select();
        }
        return $list;
    }

    /**
     * 保存单个配置
     * @return type
     */
    function save_conf() {
        $validator = array(
            array('title', 'require', '标题无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('conf_group', 'require', '分组无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('status', 'number', '状态有误!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function remove_conf($id) {
        $result = $this->where(array('id' => array('in', $id)))->delete();
        return $this->returnMsg($result);
    }

}
