<?php

namespace XWAM\Model;

/**
 * ProductInvoicingModel 类
 *
 * @since 1.0 <2016-5-28> SoChishun <14507247@qq.com> Added.
 */
class ProductInvoicingModel extends AppbaseModel {

    protected $tableName = 't_porg_product_invoicing';

    // 保存入库 2016-5-30
    function save_invoicing_in() {
        $rules = array(
            array('product_id', 'number', '商品无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('store_id', 'number', '分店无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('number', 'currency', '数量无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('price', 'currency', '价格无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('old_stock', 'double', '库存无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        $product_id = $this->data['product_id'];
        $number = floatval($this->data['number']);
        $price = floatval($this->data['price']);
        $amount = $number * $price;
        $this->subtotal = $amount;
        $this->new_stock = floatval($this->old_stock + $number);
        $result = $this->add();
        if (false !== $result) {
            // 更新商品表：进货数量, 进货金额, 当前库存
            $this->execute("update t_porg_product_goods set purchase_quantity=purchase_quantity+$number, purchase_amount=purchase_amount+$amount, stock=stock+$number where id=$product_id");
        }
        return $this->returnMsg($result);
    }

    // 保存出库 2016-5-30
    function save_invoicing_out() {
        $rules = array(
            array('product_id', 'number', '商品无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('store_id', 'number', '分店无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('number', 'currency', '数量无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('price', 'currency', '价格无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('old_stock', 'double', '库存无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        $product_id = $this->data['product_id'];
        $number = floatval($this->data['number']);
        $price = floatval($this->data['price']);
        $amount = $number * $price;
        $this->subtotal = $amount;
        $this->new_stock = floatval($this->old_stock - $number);
        $result = $this->add();
        if (false !== $result) {
            // 更新商品表：进货数量, 进货金额, 当前库存
            $this->execute("update t_porg_product_goods set sale_quantity=sale_quantity+$number, sale_amount=sale_amount+$amount, stock=stock-$number where id=$product_id");
        }
        return $this->returnMsg($result);
    }

    function delete_invoicing($id) {
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

}
