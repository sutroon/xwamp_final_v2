<?php

namespace XWAM\Model;

/**
 * CustomerTagModel 类
 *
 * @since VER:1.0; DATE:2016-4-14; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class CustomerTagModel extends AppbaseModel {

    protected $tableName = 't_porg_customer_tag';

    /**
     * 添加标签
     * @param type $labels
     * @param type $user_name
     * @since 1.0 2016-4-14 SoChishun Added.
     */
    public function add_tag($labels, $user_name, $splitor = ' ') {
        $atags = explode($splitor, $labels);
        $sql = '';
        foreach ($atags as $stag) {
            if (!$stag) {
                continue;
            }
            if ($sql) {
                $sql.=',';
            }
            $sql.="('$stag','$user_name')";
        }
        $sql = 'insert into t_porg_customer_tag (title, user_name) values ' . $sql;
        $this->execute($sql);
    }

}
