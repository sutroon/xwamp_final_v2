<?php

namespace XWAM\Model;

/**
 * PMModel 类
 *
 * @since 1.0 <2015-12-2> SoChishun <14507247@qq.com> Added.
 */
class PMModel extends AppbaseModel {

    protected $tableName = 't_porg_pm';

    private function parse_box($user_name, $box = 'INBOX') {
        $where = array();
        switch ($box) {
            case 'INBOX': // 收件箱
                $where['to_user'] = $user_name;
                $where['delete_status'] = 'N';
                break;
            case 'BINBOX': // 回收站
                $where['to_user'] = $user_name;
                $where['delete_status'] = 'Y';
                break;
            case 'OUTBOX': // 发件箱
                $where['from_user'] = $user_name;
                $where['status'] = 1;
                break;
            case 'DRAFTS':// 草稿箱
                $where['from_user'] = $user_name;
                $where['status'] = 0;
                break;
        }
        return $where;
    }

    function get_list($user_name, $box = 'INBOX') {
        $where = $this->parse_box($user_name, $box);
        return $this->where($where)->select();
    }

    function get_paging_list2(&$pager, $user_name, $box = 'INBOX') {
        $where = $this->parse_box($user_name, $box);
        $options = array('table' => $this->tableName, 'where' => $where);
        return parent::get_paging_list2($pager, $options);
    }

    function save_pm($user_name) {
        $validator = array(
            array('to_user', 'require', '收件人无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('subject', 'require', '主题无效!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('body', 'require', '内容无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->create_time)) {
            unset($this->data['create_time']);
        }
        if (empty($this->id)) {
            $this->from_user = $user_name;
            if (empty($this->create_time)) {
                $this->status = 1; // 已发送
            }
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function delete_pm($id = '') {
        $result = $this->where(array('id' => array('in', $id)))->delete();
        return $this->returnMsg($result);
    }

    function change_read_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField('read_status', $status);
        return $this->returnMsg($result);
    }

    function read_count($user_name) {
        $where = array(
            'to_user' => $user_name,
            'read_status' => 0,
            'delete_status' => 'N',
        );
        return $this->where($where)->count('id');
    }

}
