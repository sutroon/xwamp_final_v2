<?php

namespace XWAM\Model;

/**
 * 部门数据库模型
 *
 * @since 1.0.0 <2016-1-14> SoChishun <14507247@qq.com> Added.
 */
class DepartmentModel extends AppbaseModel {

    /**
     * 数据表名称
     * @var string
     */
    protected $tableName = 'think_auth_department';

    /**
     * 是否批量验证
     * @var boolean
     */
    protected $patchValidate = true;

    /**
     * 获取id_text列表
     * @param boolean $key_is_id 键名是否是ID(默认false)
     * @return type
     * @since 1.0 2016-6-1 SoChishun Added.
     */
    function get_idtext_list($key_is_id = false) {
        $list = array();
        if ($key_is_id) {
            $list = $this->where(array('status' => 1))->cache('department_idtext_kvlist', 10)->order('sort, id')->getField('id, title as text');
        } else {
            $list = $this->where(array('status' => 1))->cache('department_idtext_list', 10)->order('sort, id')->field('id,title as text')->select();
        }
        return $list;
    }

    public function save_rule($department_id, $rules) {
        $result = $this->where(array('id' => $department_id))->setField('rules', implode(',', $rules));
        return $this->returnMsg($result);
    }

    public function delete_department($id = '') {
        if (!$id) {
            return $this->returnMsg(false, '参数无效');
        }
        // 判断是否有子项目
        $list = $this->query('select a.title from think_auth_department a inner join think_auth_department b on b.pid=a.id where b.pid in (' . $id . ')');
        $names = false;
        if ($list) {
            foreach ($list as $row) {
                $names.=',' . $row['title'];
            }
        }
        if ($names) {
            return $this->returnMsg(false, '以下项目有子项目,请先转移子项目:' . "\n" . ltrim($names, ','));
        }

        // 判断是否有引用
        $user_department_ids = $this->table('t_porg_user')->where(array('department_ids' => $id))->getField('department_ids', true);
        if ($user_department_ids) {
            $names = $this->where(array('id' => array('in', $user_department_ids)))->getField('title', true);
            return $this->returnMsg(false, '以下项目正在被用户使用,无法删除:' . "\n" . implode(',', $names));
        }
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

    function select_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title, pid, code, rules, sort, status, create_time', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function select_json_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title as text', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function save_department() {
        $rules = array(
            array('title', 'require', '名称无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 复制部门
     * @return type
     * @since 1.0 <2016-1-14> SoChishun Added.
     */
    function copy_department($id) {
        $table = $this->tableName;
        $this->execute("insert into $table (title,pid,`code`,rules,status,site_id) select concat(title,'_new'),pid,`code`,rules,status,site_id from $table where id=$id");
        return $this->returnMsg(true);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

}
