<?php

namespace XWAM\Model;

/**
 * Description of OrderModel
 *
 * @since 1.0 <2016-5-28> SoChishun <14507247@qq.com> Added.
 */
class CommissionDiscountModel extends AppbaseModel {

    protected $tableName = 't_porg_commission_discount';

    /**
     * 生成报表
     * <p>
     * 每天每家门店每个旅行社可能有多趟旅游团，每天每门店每家旅行社一条记录<br />
     * 每条佣金记录点击进去，可以查看每趟旅游团佣金
     * </p>
     * @param type $store_id
     * @return type
     */
    function generate_report($store_id) {
        $where_s0 = array('store_id' => $store_id, 'status' => 0);
        $this->where($where_s0)->delete(); // 未结算的全部删除以便重新统计
        $n = $this->table('t_plvs_order')->where(array('store_id' => $store_id, 'settlement_status' => 0, 'commission_rate_id' => array('gt', 0)))->count();
        if ($n < 1) {
            return; // 没有需要统计的
        }
        // 更新日结销售表佣金统计
        $this->execute('update t_plvs_order o inner join t_porg_commission_rate r on o.commission_rate_id=r.id set o.parking_fee=r.parking_fee, o.personnel_fee=o.person_count * r.personnel_fee, o.sichou_commission_fee=o.settlement_amount * r.sichou_commission_rate / 100, o.sijin_commission_fee=o.sijin_amount * r.sijin_commission_rate / 100, o.baihuo_commission_fee=o.baihuo_amount * r.baihuo_commission_rate / 100 where o.store_id=' . $store_id . ' and o.settlement_status < 1 and o.commission_rate_id > 0');
        // 更新日结销售表小计字段
        $this->execute('update t_plvs_order set fee_subtotal=parking_fee+personnel_fee+sichou_commission_fee+sijin_commission_fee+baihuo_commission_fee where store_id=' . $store_id . ' and settlement_status < 1 and commission_rate_id > 0');

        // 每天每家门店每个旅行社可能有多趟旅游团，每天每门店每家旅行社每结算类型一条记录(支持一天多次结算)
        $this->execute('insert into t_porg_commission_discount (time_date,sale_type,travel_agency_id,store_id) select DISTINCT time_date, sale_type, travel_agency_id, store_id from t_plvs_order where store_id=' . $store_id . ' and settlement_status=0 and commission_rate_id > 0;');

        // 关联日销售表与佣金表的对应关系
        $this->execute('update t_plvs_order o left join t_porg_commission_discount d on o.travel_agency_id=d.travel_agency_id and o.store_id=d.store_id and o.time_date=d.time_date and o.sale_type=d.sale_type set o.commission_discount_id=d.id where o.store_id=' . $store_id . ' and o.settlement_status < 1 and o.commission_rate_id > 0 and d.status=0');

        // 更新佣金表统计字段
        $this->execute('update t_porg_commission_discount d inner join (select store_id,travel_agency_id,time_date,sale_type,sum(parking_fee) as parking_fee, sum(personnel_fee) as personnel_fee, sum(sichou_commission_fee) as sichou_commission_fee, sum(sijin_commission_fee) as sijin_commission_fee, sum(baihuo_commission_fee) as baihuo_commission_fee, sum(fee_subtotal) as fee_subtotal from t_plvs_order where store_id=' . $store_id . ' and settlement_status < 1 and commission_rate_id > 0 group by store_id,travel_agency_id,time_date,sale_type) as o on d.store_id=o.store_id and d.travel_agency_id=o.travel_agency_id and d.time_date=o.time_date and d.sale_type=o.sale_type set d.parking_fee=o.parking_fee, d.personnel_fee=o.personnel_fee, d.sichou_commission_fee=o.sichou_commission_fee, d.sijin_commission_fee=o.sijin_commission_fee, d.baihuo_commission_fee=o.baihuo_commission_fee, d.subtotal=o.fee_subtotal where d.store_id=' . $store_id . ' and d.status < 1');
    }

    /**
     * 重置表数据
     * 2016-6-7 SoChishun Added.
     */
    function truncate() {
        $this->execute('update t_plvs_order set settlement_status=0');
        $this->execute('truncate table t_porg_commission_discount');
    }

}
