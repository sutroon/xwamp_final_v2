<?php

namespace XWAM\Model;

/**
 * Description of UserEmployeeModel
 *
 * @since 1.0 2016-5-26 SoChishun Added.
 */
class UserEmployeeModel extends AppbaseModel {

    protected $tableName = 't_porg_user';
    protected $patchValidate = true;

    /**
     * 修改密码
     * @param string|array $where 查询条件
     * @param string $pwd 新密码
     * @return false|int 成功则返回受影响数量,失败返回false
     * @since 1.0 2015-3-6 by sutroon
     */
    public function update_password($where, $pwd) {
        $result = $M->where($where)->setField('password', $pwd);
        $condition = is_array($where) ? sofunc_array_implode(',', $where) : $where;
        $log = new LogModel();
        $log->log_user_operate(false !== $result, "[修改密码%] $condition, password=$pwd", 0, LogModel::ACTION_EDIT);
        return $result;
    }

    /**
     * 保存用户
     * @return array
     * @since 1.0 2015-3-6 by sutroon
     */
    public function save_user() {
        $validator = array(
            array('user_name', 'require', '用户名无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('user_name', 'check_user_name', '用户名已被注册!', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
            array('password', 'require', '密码无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('site_id', 'number', '站点编号无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('status', 'number', '状态有误!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        //图片上传
        $msg = \Common\Controller\UploadHandlerController::upload(array('savePath' => 'Face/', 'skipEmpty' => true, 'exts' => array('jpg', 'jpeg', 'png', 'gif')));
        if (is_array($msg)) {
            $this->data['face_url'] = $msg['face_url']['filepath'];
        }
        $data = $this->data;
        if (empty($this->id)) {
            unset($this->data['id']);
            $this->type_name = 'EMPLOYEE';
            $result = $this->add();
        } else {
            $this->update_time=date('Y-m-d H:i:s');
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    // 检测用户名是否重复 2016-4-8 SoChishun Added.
    function check_user_name($val) {
        $n = $this->where(array('user_name' => trim($val)))->count('id');
        return $n < 1;
    }

    /**
     * 删除用户
     * @param string $id 主键编号,多个之间以逗号隔开
     * @param int $admin_id 操作员编号
     * @return array
     * @since 1.0 <2015-6-10> SoChishun Added.
     */
    public function delete_user($id, $admin_id) {
        if (!$admin_id) {
            return $this->returnMsg(false, '用户无效');
        }
        if (!$id) {
            return $this->returnMsg(false, '编号无效');
        }
        $result = $this->delete($id);
        $log = new LogModel();
        $log->log_user_operate($status, "删除用户账户($id){%} $msg,操作员:$admin_id", $admin_id, LogModel::ACTION_DELETE);
        return $this->returnMsg($result);
    }

    // 2016-5-31
    function save_conf_setting() {
        $validator = array(
            array('field', 'require', '字段无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('uid', 'number', '员工无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        $aid = I('id');
        if (!$aid) {
            return $this->returnMsg(false, '未勾选任何项目!');
        }
        $result = $this->where(array('id' => $this->data['uid']))->setField($this->data['field'], implode(',', $aid));
        return $this->returnMsg($result);
    }

}
