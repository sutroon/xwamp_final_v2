<?php

namespace XWAM\Model;

/**
 * Description of CommissionRateModel
 *
 * @since 1.0 <2016-6-3> SoChishun <14507247@qq.com> Added.
 */
class CommissionRateModel extends AppbaseModel {

    protected $tableName = 't_porg_commission_rate';

    /**
     * 获取id_text列表
     * @param number $travel_id
     * @param boolean $key_is_id 键名是否是ID(默认false)
     * @return type
     * @since 1.0 2016-5-27 SoChishun Added.
     */
    function get_idtext_list($travel_id, $key_is_id = false) {
        $list = array();
        $where['status'] = 1;
        if (is_null($travel_id)) {
            $travel_id = 0;
        } else {
            $where['travel_agency_id'] = $travel_id;
        }
        if ($key_is_id) {
            $list = $this->where($where)->cache('commission_rate_' . $travel_id . '_idtext_kvlist', 10)->order('sort, id')->getField('id, title as text');
        } else {
            $list = $this->where($where)->cache('commission_rate_' . $travel_id . '_idtext_list', 10)->order('sort, id')->field('id,title as text')->select();
        }
        return $list;
    }

    /**
     * 保存单个配置
     * @return type
     */
    function save_conf() {
        $validator = array(
            array('title', 'require', '标题无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('travel_agency_id', 'number', '旅行社无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('status', 'number', '状态有误!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function remove_conf($id) {
        $result = $this->where(array('id' => array('in', $id)))->delete();
        return $this->returnMsg($result);
    }

}
