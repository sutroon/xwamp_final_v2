<?php

namespace XWAM\Model;

/**
 * CustomerAppointmentModel 类
 *
 * @since VER:1.0; DATE:2016-1-27; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class CustomerAppointmentModel extends AppbaseModel {

    protected $tableName = 't_porg_customer_appointment';

    /**
     * 分页查询
     * @param string $pager
     * @param array $afields
     * @param integer $site_id
     * @param array $asearch
     * @return array
     * @since 1.0 2016-5-12 SoChishun Added.
     */
    function paging_select(&$pager, &$afields, $site_id, $asearch) {
        $m_attr = new CustomerAttrModel();
        $afields = $m_attr->get_customer_list_fields($site_id, array('serial_no' => '客户ID', 'name' => '姓名', 'telphone' => '电话', 'sex' => '性别', 'address' => '地址', 'user_name' => '工号'));
        $afields['appo'] = array('id' => '', 'customer_id' => '', 'appointment_time' => '预约时间', 'appointment_remark' => '预约备注', 'review_time' => '回访时间', 'review_result' => '回访结果', 'status' => '回访状态');
        if ($afields['custom']) {
            $table = $this->tableName . ' a left join t_porg_customer c on a.customer_id=c.id left join t_porg_customer_s' . $site_id . ' s on c.id=s.customer_id';
            $field = 'c.' . implode(',c.', array_keys($afields['system'])) . ',s.' . implode(',s.', array_keys($afields['custom'])) . ',a.' . implode(',a.', array_keys($afields['appo']));
        } else {
            $table = $this->tableName . ' a left join t_porg_customer c on a.customer_id=c.id';
            $field = 'c.' . implode(',c.', array_keys($afields['system'])) . ',a.' . implode(',a.', array_keys($afields['appo']));
        }
        return $this->get_paging_list($pager, array('table' => $table, 'field' => $field, 'where' => $asearch['where'], 'order' => 'a.id desc'), array('page_params' => $asearch['search']));
    }

    // 保存预约记录 2016-1-27
    function save_appointment() {
        $rules = array(
            array('appointment_time', 'require', '预约时间无效!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('customer_id', 'require', '客户编号无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function delete_customer($id) {
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

}
