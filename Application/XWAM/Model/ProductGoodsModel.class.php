<?php

namespace XWAM\Model;

/**
 * ProductGoodsModel 类
 *
 * @since 1.0 <2016-5-28> SoChishun <14507247@qq.com> Added.
 */
class ProductGoodsModel extends AppbaseModel {

    protected $tableName = 't_porg_product_goods';

    /**
     * 获取id_text列表
     * @param type $site_id
     * @param type $key_is_id
     * @return type
     * @since 1.0 2016-5-30 SoChishun Added.
     */
    public function get_idtext_list($site_id, $key_is_id = false) {
        $list = array();
        if ($key_is_id) {
            $list = $this->where(array('site_id' => $site_id))->cache('productgoods_idtext_kvlist_' . $site_id, 10)->getField('id, title as text');
        } else {
            $list = $this->where(array('site_id' => $site_id))->cache('productgoods_idtext_list_' . $site_id, 10)->field('id, title as text')->select();
        }
        return $list;
    }

    function save_goods() {
        $rules = array(
            array('title', 'require', '标题无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('category_id', 'number', '类别无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('store_id', 'number', '分店无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('price', 'currency', '价格无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        //图片上传
        $msg = \Common\Controller\UploadHandlerController::upload(array('savePath' => 'ProductGoods/', 'skipEmpty' => true));
        if (is_array($msg) && isset($msg['picture_url'])) {
            $this->data['picture_url'] = $msg['picture_url']['filepath'];
        }
        if (isset($this->data['create_time']) && empty($this->data['create_time'])) {
            unset($this->data['create_time']);
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    // 2016-7-1
    function find_content($id, $code) {
        $data = $this->table('t_porg_product_content')->where(array('product_id' => $id, 'code' => $code))->find();
        if (!$data) {
            $data = array('product_id' => $id, 'code' => $code);
            $this->table('t_porg_product_content')->add($data);
        }
        return $data;
    }

    function save_content() {
        $rules = array(
            array('product_id', 'number', '商品编号无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('code', 'require', '内容代码无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        $where = array('product_id' => $this->data['product_id'], 'code' => $this->data['code']);
        $result = $this->table('t_porg_product_content')->where($where)->setField('content', $this->data['content']);
        return $this->returnMsg($result);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_goods($id) {
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

}
