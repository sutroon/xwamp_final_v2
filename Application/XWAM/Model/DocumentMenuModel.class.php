<?php

namespace XWAM\Model;

/**
 * DocumentMenuModel 类
 *
 * @since 1.0 <2016-6-17> SoChishun <14507247@qq.com> Added.
 */
class DocumentMenuModel extends AppbaseModel {

    protected $tableName = 't_porg_document_menu';
    protected $patchValidate = true;

    /**
     * 返回树形列表
     * <p>
     * 注意加查询条件: $where['pid']=0
     * </p>
     * @param array $select_options 数据库查询选项 (where, order, limit, field等)
     * @param array $data_options 业务数据选项(array('checked_values' => array(), 'pid' => 0, 'pid_field' => 'pid', 'where_first' => '', 'children_key' => 'children'))
     * @return array
     */
    function select_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title, pid, link_url, code, sort, access, status, create_time', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function select_json_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title as text', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function save_menu($user_name) {
        $rules = array(
            array('title', 'require', '名称无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('link_url', 'require', '链接地址无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (!$this->pid) {
            $this->pid = 0;
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 复制菜单
     * @return type
     * @since 1.0 <2016-1-14> SoChishun Added.
     */
    function copy_menu($id) {
        $table = $this->tableName;
        $this->execute("insert into $table (title, pid, user_name, site_id) select concat(title,'_new'), pid, user_name, site_id from $table where id=$id");
        return $this->returnMsg(true);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_menu($id) {
        if ($this->where(array('pid' => $id))->count() > 0) {
            return $this->returnMsg(false, '该菜单下有子菜单,请先转移或删除子菜单!');
        }
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

}
