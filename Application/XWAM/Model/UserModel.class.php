<?php

namespace XWAM\Model;

/**
 * Description of UserModel
 *
 * @author Su 2015-3-5
 */
class UserModel extends AppbaseModel {

    protected $tableName = 't_porg_user';
    protected $patchValidate = true;


    /**
     * 重置密码
     * @param string|array $where 查询条件
     * @param string $pwd 新密码
     * @return false|int 成功则返回受影响数量,失败返回false
     * @since 1.0 2015-3-6 by sutroon
     */
    public function update_password($uid, $pwd) {
        $result = $this->where(array('id'=>$uid))->setField('password', $pwd);
        $log = new LogModel();
        $log->log_user_operate(false !== $result, "[修改密码%] $uid, password=$pwd", 0, LogModel::ACTION_EDIT);
        return $result;
    }

    /**
     * 保存密码
     * @return type
     * @since 1.0 2016-6-7 SoChishun Added.
     */
    function save_password() {
        $validator = array(
            array('uid', 'number', '用户无效!', self::MUST_VALIDATE, 'regex', self::MODEL_UPDATE),
            array('password', 'require', '密码无效!', self::MUST_VALIDATE, 'regex', self::MODEL_UPDATE),
            array('repassword', 'require', '确认密码无效!', self::MUST_VALIDATE, 'regex', self::MODEL_UPDATE),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if ($this->password != $this->repassword) {
            return $this->returnMsg(false, '两次密码输入不一致!');
        }
        $result = $this->where(array('id' => $this->uid))->setField('password', $this->password);
        return $this->returnMsg($result);
    }

    /**
     * 设置用户列表的角色和部门名称
     * 角色名称：role_names, 部门名称：department_names
     * @param array $list
     * @since VER:1.0; DATE:2016-1-18; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
     */
    

    /**
     * 设置用户列表的角色和部门名称
     * 角色名称：role_names, 部门名称：department_names
     * @param array $list
     * @since VER:1.0; DATE:2016-1-18; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
     */
    function set_fields_text($list) {
        if (!$list) {
            return $list;
        }
        // 角色
        $m_role = null;
        $roles = false;
        // 部门
        $m_department = null;
        $departments = false;
        $aout = array();
        foreach ($list as $row) {
            // 婚姻状态
            if (isset($row['marital_status'])) {
                $row['marital_status_text'] = sofn_get_state_text('marital', $row['marital_status']);
            }
            // 性别
            if (isset($row['sex'])) {
                $row['sex_text'] = sofn_get_state_text('sex', $row['sex']);
            }
            // 在线状态
            if (isset($row['online_status'])) {
                $row['online_status_text'] = sofn_get_state_text('online-state', $row['online_status']);
            }
            // 用户状态
            if (isset($row['status'])) {
                $row['status_text'] = sofn_get_state_text('user-state', $row['status']);
            }
            // 角色
            if (isset($row['role_id'])) {
                if (!$m_role) {
                    $m_role = new RoleModel();
                    $roles = $m_role->getField('id, title, display_title');
                }
                $row['role_code'] = '';
                $row['role_name'] = '';
                $role_ids = explode(',', $row['role_id']);
                if ($role_ids) {
                    foreach ($role_ids as $role_id) {
                        if (array_key_exists($role_id, $roles)) {
                            if ($row['role_name']) {
                                $row['role_name'].=', ';
                            }
                            $row['role_code'].=$roles[$role_id]['code'];
                            $row['role_name'].=empty($roles[$role_id]['display_title']) ? $roles[$role_id]['title'] : $roles[$role_id]['display_title'];
                        }
                    }
                }
            }
            // 部门
            if ($row['department_id']) {
                if (!$m_department) {
                    $m_department = new DepartmentModel();
                    $departments = $m_department->getField('id,title,code');
                }
                $row['department_code'] = '';
                $row['department_name'] = '';
                $department_ids = explode(',', $row['department_id']);
                if ($department_ids) {
                    foreach ($department_ids as $department_id) {
                        if (array_key_exists($department_id, $departments)) {
                            if ($row['department_name']) {
                                $row['department_name'].=', ';
                            }
                            $row['department_code'].=$departments[$department_id]['code'];
                            $row['department_name'].=$departments[$department_id]['title'];
                        }
                    }
                }
            }
            $aout[] = $row;
        }
        return $aout;
    }

    /**
     * 更改在线状态
     * @param type $uid
     * @param type $status
     * @return type
     * @since 1.0 <2015-10-28> SoChishun Added.
     */
    function change_online_status($uid, $status) {
        $result = $this->where(array('id' => $uid))->setField(array('online_status' => $status));
        return $this->returnMsg($result);
    }

    /**
     * 保存用户角色
     * @param type $uid
     * @param type $role_ids
     * @return type
     */
    function save_role($uid, $role_ids) {
        $result = $this->where(array('id' => $uid))->setField(array('role_ids' => $role_ids));
        return $this->returnMsg($result);
    }

    function save_department($uids, $department_ids) {
        $result = $this->where(array('id' => array('in', $uids)))->setField(array('department_ids' => $department_ids));
        return $this->returnMsg($result);
    }

}
