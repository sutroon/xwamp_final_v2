<?php

namespace XWAM\Model;

/**
 * Description of OrderModel
 *
 * @since 1.0 <2016-5-26> SoChishun <14507247@qq.com> Added.
 */
class OrderModel extends AppbaseModel {

    protected $tableName = 't_plvs_order';

    /**
     * 保存订单
     * @return type
     */
    function save_order() {
        if ('定金' == I('sale_type')) {
            $validator = array(
                array('time_date', 'require', '日期无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('store_id', 'number', '分店无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('travel_agency_id', 'number', '旅行社无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('tourist_type_id', 'number', '客源类型无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('product_amount', 'currency', '总额无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('deposit_date', 'require', '定金日期无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('status', 'number', '状态有误!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            );
        } else {
            $validator = array(
                array('time_date', 'require', '日期无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('store_id', 'number', '分店无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('travel_agency_id', 'number', '旅行社无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('person_count', 'number', '人数无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('tourist_type_id', 'number', '客源类型无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('product_amount', 'currency', '总额无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
                array('status', 'number', '状态有误!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            );
        }
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        $data = $this->data;
        $noemptyfields = array('person_count', 'tourist_type_id', 'interpretation_room_id', 'commission_amount', 'settlement_amount', 'interpreter_id', 'guide_id', 'commission_rate_id');
        foreach ($noemptyfields as $field) {
            if (strlen($data[$field]) < 1) {
                $data[$field] = 0;
            }
        }
        if (empty($data['deposit_date'])) {
            unset($data['deposit_date']);
        }
        if (empty($this->id)) {
            unset($data['id']);
            $result = $this->add($data);
        } else {
            $result = $this->save($data);
        }
        return $this->returnMsg($result);
    }

}
