<?php

namespace XWAM\Model;

/**
 * DocumentCategoryModel 类
 *
 * @since 1.0 <2015-10-22> SoChishun <14507247@qq.com> Added.
 */
class DocumentCategoryModel extends AppbaseModel {

    protected $tableName = 't_porg_document_category';
    protected $patchValidate = true;

    /**
     * 返回树形列表
     * <p>
     * 注意加查询条件: $where['pid']=0
     * </p>
     * @param array $select_options 数据库查询选项 (where, order, limit, field等)
     * @param array $data_options 业务数据选项(array('checked_values' => array(), 'pid' => 0, 'pid_field' => 'pid', 'where_first' => '', 'children_key' => 'children'))
     * @return array
     */
    function select_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title, pid, type_name, allow_subitem, sort, access, status, create_time', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function select_json_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title as text', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function save_category() {
        $rules = array(
            array('title', 'require', '名称无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('type_name', 'require', '类型无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        $this->allow_subitem = in_array($this->type_name, array('document-list', 'file-list', 'node')) ? 'Y' : 'N'; // 是否允许子栏目 (2016-6-17 added.)
        if(!$this->pid){
            $this->pid=0;
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 复制栏目
     * @return type
     * @since 1.0 <2016-1-14> SoChishun Added.
     */
    function copy_category($id) {
        $table = $this->tableName;
        $this->execute("insert into $table (title, pid, type_name, allow_subitem, user_name, site_id) select concat(title,'_new'), pid, type_name, allow_subitem, user_name, site_id from $table where id=$id");
        return $this->returnMsg(true);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_channel($id) {
        if ($this->where(array('pid' => $id))->count() > 0) {
            return $this->returnMsg(false, '该栏目下有子栏目,请先转移或删除子栏目!');
        }
        if ($this->table('t_porg_document')->where(array('category_id' => $id))->count() > 0) {
            return $this->returnMsg(false, '该栏目下有文章,请先转移或删除文章!');
        }
        if ($this->table('t_porg_file')->where(array('category_id' => $id))->count() > 0) {
            return $this->returnMsg(false, '该栏目下有下载资源,请先转移或删除下载资源!');
        }
        $result = $this->delete($id);
        if (false !== $result) {
            $this->table('t_porg_document_category_content')->delete($id);
            $this->table('t_porg_document_category_link')->delete($id);
        }
        return $this->returnMsg($result);
    }

}
