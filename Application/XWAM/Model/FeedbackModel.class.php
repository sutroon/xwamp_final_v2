<?php

namespace XWAM\Model;

/**
 * DocumentModel 类
 *
 * @since VER:1.0; DATE:2016-6-23; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class FeedbackModel extends AppbaseModel {

    protected $tableName = 't_porg_feedback';

    /**
     * 返回树形列表
     * <p>
     * 注意加查询条件: $where['pid']=0
     * </p>
     * @param array $select_options 数据库查询选项 (where, order, limit, field等)
     * @param array $data_options 业务数据选项(array('checked_values' => array(), 'pid' => 0, 'pid_field' => 'pid', 'where_first' => '', 'children_key' => 'children'))
     * @return array
     */
    function select_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title, pid, contacts, email, mobile, content, status, create_time', 'order' => 'id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function save_feedback() {
        $rules = array(
            array('title', 'require', '标题无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $id = $this->id;
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 回复留言
     * @return type
     * @since 1.0 2016-6-23 SoChishun Added.
     */
    function reply_feedback() {
        $rules = array(
            array('pid', 'number', '编号无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('content', 'require', '内容无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_feedback($id) {
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

}
