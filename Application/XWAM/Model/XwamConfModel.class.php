<?php

namespace XWAM\Model;

/**
 * XwamConfModel 类
 *
 * @since VER:1.0; DATE:2016-4-21; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class XwamConfModel extends AppbaseModel {

    protected $tableName = 't_porg_xwam_conf';

    /**
     * 获取id_text列表
     * @param boolean $key_is_id 键名是否是ID(默认false)
     * @return type
     * @since 1.0 2016-5-27 SoChishun Added.
     */
    function get_idtext_list($key_is_id = false) {
        $list = array();
        if ($key_is_id) {
            $list = $this->where(array('status' => 1))->cache('site_idtext_kvlist', 10)->order('sort, id')->getField('id, site_title as text');
        } else {
            $list = $this->where(array('status' => 1))->cache('site_idtext_list', 10)->order('sort, id')->field('id, site_title as text')->select();
        }
        return $list;
    }

    /**
     * 获取网站
     * @param type $site_id
     * @param type $user_name
     * @param type $create
     * @return type
     * @since 1.0 2016-4-21 SoChishun Added.
     */
    public function find_site($site_id, $user_name, $create = true) {
        $data = $this->find($site_id);
        if (!$data && $create) {
            $site_code = sofn_generate_serial('S' . $site_id);
            $data = array(
                'id' => $site_id,
                'site_title' => 'XWAMP销售管理系统',
                'site_code' => $site_code,
                'site_app_name' => 'XWAMP',
                'site_app_version' => '1.0.0.0',
                'site_sub_path' => '',
                'session_prefix' => 'xwamp' . $site_code,
                'content_list_rows' => 30,
                'user_name' => $user_name,
                'site_closed_announcement' => '抱歉,网站正在维护,请您稍后访问~',
                'ui_aside_status' => 'Y',
            );
            $id = $this->add($data);
            $data = $this->find($id);
        }
        return $data;
    }

    public function save_site() {
        $validator = array(
            array('site_title', 'require', '网站标题无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('site_code', '', '网站代号已被使用!', self::MUST_VALIDATE, 'unique', self::MODEL_INSERT),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        //图片上传
        //$msg = \Common\Controller\UploadHandlerController::upload(array('savePath' => 'Logo/', 'skipEmpty' => true));
        //if (is_array($msg)) {
        //    $this->data['site_logo_url'] = $msg['site_logo_url']['filepath'];
        //}
        $this->session_prefix = $this->site_code;
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function remove_site($id) {
        $result = $this->where(array('id' => array('in', $id)))->delete();
        return $this->returnMsg($result);
    }

}
