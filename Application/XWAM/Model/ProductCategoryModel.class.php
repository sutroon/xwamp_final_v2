<?php

namespace XWAM\Model;

/**
 * ProductCategoryModel 类
 *
 * @since 1.0.0 <2016-5-28> SoChishun <14507247@qq.com> Added.
 */
class ProductCategoryModel extends AppbaseModel {

    /**
     * 数据表名称
     * @var string
     */
    protected $tableName = 't_porg_product_category';

    /**
     * 是否批量验证
     * @var boolean
     */
    protected $patchValidate = true;

    /**
     * 获取id_text列表
     * @param int $site_id
     * @param boolean $key_is_id
     * @return type
     * @since 1.0 2016-5-30 SoChishun Added.
     */
    public function get_idtext_list($site_id, $key_is_id = false) {
        $list = array();
        if ($key_is_id) {
            $list = $this->where(array('site_id' => $site_id))->cache('productcategory_idtext_kvlist_' . $site_id, 10)->getField('id, title as text');
        } else {
            $list = $this->where(array('site_id' => $site_id))->cache('productcategory_idtext_list_' . $site_id, 10)->field('id, title as text')->select();
        }
        return $list;
    }

    public function delete_category($id = '') {
        if (!$id) {
            return $this->returnMsg(false, '参数无效');
        }
        // 判断是否有子项目
        $list = $this->query('select a.title from t_porg_product_category a inner join t_porg_product_category b on b.pid=a.id where b.pid in (' . $id . ')');
        $names = false;
        if ($list) {
            foreach ($list as $row) {
                $names.=',' . $row['title'];
            }
        }
        if ($names) {
            return $this->returnMsg(false, '以下项目有子项目,请先转移子项目:' . "\n" . ltrim($names, ','));
        }
        // 判断是否有引用
        $category_ids = $this->table('t_porg_product_goods')->where(array('category_id' => array('in', $id)))->getField('category_id', true);
        if ($category_ids) {
            $names = $this->where(array('id' => array('in', $category_ids)))->getField('title', true);
            return $this->returnMsg(false, '以下项目已被占用,无法删除:' . "\n" . implode(',', $names));
        }
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

    /**
     * 
     * @param type $select_options
     * @param type $data_options
     * @return type
     */
    function select_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title, pid, sort, status, create_time', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    /**
     * 
     * @param type $select_options
     * @param type $data_options
     * @return type
     */
    function select_json_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title as text', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function save_category() {
        $rules = array(
            array('title', 'require', '名称无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->pid)) {
            unset($this->data['pid']);
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 复制类别
     * @return type
     * @since 1.0 <2016-1-14> SoChishun Added.
     */
    function copy_category($id) {
        $table = $this->tableName;
        $this->execute("insert into $table (title, pid, status, site_id) select concat(title,'_new'), pid, status, site_id from $table where id=$id");
        return $this->returnMsg(true);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

}
