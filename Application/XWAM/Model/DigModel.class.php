<?php

namespace XWAM\Model;

/**
 * DigModel 类
 *
 * @since 1.0 <2015-11-25> SoChishun <14507247@qq.com> Added.
 */
class DigModel extends AppbaseModel {

    function get_records() {
        import('Vendor.Snoopy.Snoopy');
        $snoopy = new \Snoopy();       //实例化一个对象
        $sourceURL = "http://finance.sina.com.cn/futures/quotes/CL.shtml";    //要抓取的网页
        $snoopy->fetch($sourceURL);
        //$snoopy->fetchlinks($sourceURL);        //获得网页的链接
        $a = $snoopy->results;     //得到网页链接的结果
        $pattern = '/<a .*?href="(.*?)".*?>/is';
        $pattern = '/<a .*?href="(.*?)(d+\.shtml)".*?>/is';
        //$re = "/\/\d+.shtml$/";     //匹配的正则
        //过滤获取指定的文件地址请求
        preg_match_all($pattern, $a, $aa);
        var_export($aa);
        exit;
        $aa = 'http://news.mydrivers.com/1/458/458124.htm';
        $this->getImgURL($aa);
    }

    function getImgURL($url) {
        if (!$url) {
            return;
        }
        $snoopy = new \Snoopy();
        $snoopy->fetch($url);
        $fileContent = $snoopy->results;    //获取过滤后的页面的内容            
        //匹配图片的正则表达式        
        $reTag = "<img[^s]+src=\"(http://[^\"]+).(jpg|png|gif|jpeg)\"[^/]*/>";
        if (preg_match($reTag, $fileContent)) {
            //过滤图片
            $ret = preg_match_all($reTag, $fileContent, $matchResult);
            for ($i = 0, $len = count($matchResult[1]); $i < $len; ++$i) {
                $this->saveImgURL($matchResult[1][$i], $matchResult[2][$i]);
            }
        }
    }

    function saveImgURL($name, $suffix) {
        $url = $name . "." . $suffix;
        $imgSavePath = $_SERVER['DOCUMENT_ROOT'] . '\\';  //图片保存地址
        $imgSavePath .= ("\\Uploads\\Dig\\" . pathinfo($url, PATHINFO_BASENAME));  //组装要保存的文件名
        echo "请求的图片地址：$url, 保存到：$imgSavePath <br/>";
        if (is_file($imgSavePath)) {
            //判断文件名是否存在，存在则删除         
            unlink($imgSavePath);
            echo "<p style='color:#f00;'>文件" . $imgSavePath . "已存在，将被删除</p>";
        }
        $imgFile = file_get_contents($url); //读取网络文件     
        $flag = file_put_contents($imgSavePath, $imgFile);   //写入到本地 
        if ($flag) {
            echo "<p>文件" . $imgSavePath . "保存成功</p>";
        }
    }

}
