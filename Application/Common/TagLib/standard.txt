/*
 * 插件开发规范
 *  1.文件名和类名必需一致，且必需是首字母大写.推荐用小写下划线连接命名.,如My_plugin
        1.1 名称全部为小写,包含下划线连字符，如：TagLibMyplugin, TagLibMy_plugin,调用方法 <taglib name="mypulbin,my_plubin" /><myplugin:method ... /><my_plugin:method... />
        1.2 驼峰命名法，如：TagLibMyPlugin, TagLibMy_Plugin, 加载方法'TAGLIB_BUILD_IN' => 'cx,MyPlugin, My_Plugin'(大小写敏感), 调用方法<taglib name="MyPlugin,My_Plugin" />(大小写敏感)<myplugin:method ... /><my_plugin:method />(不区分大小写)
 *  2.一个插件可以包含多个方法，调用方式 <plugin:method1 ... /><plugin:method2 ... /><plugin:method3 ... />,$tags中格式$tags=array('method1'=>array('attr'=>''...),'method2'=>array('attr'=>''...)...);
 *  3.方法名称可以为小写或驼峰命名法或下划线连接法，如 <plugin:mymethod />, <plugin:myMethod ... />, <plugin:my_method .../>都是可以的,推荐用小写下划线连接命名.
 *  4.$tags中的attr，必需为小写名称，可以包含下划线连字符，如myattr1, my_attr1都是可以的，但是不能包含驼峰命名方式,如myAttr1或MyAttr1, 系统会自动将驼峰命名法中的大写字母全部编转为小写字母,如 $tags中attr="myAttr1,MyAttr2",在读取的时候需用 $attr['myattr1'],$attr['myattr2']才能正确读取.
 *      4.1 $tags必需指定close的值,1=闭合(如循环标签<loop>...</loop>，0=不闭合(如控件标签<plugin:method ... />)
        4.2 在html页面中调用,attr不区分大小写,如 MyATTR1会自动转为myattr1
        4.3 $tags中的level参数用于指定递归嵌套的级数,如 'level'=>3, 只是可以嵌套3级循环
        4.4 在$tags中获取Action传递的变量,如 <myplugin:list datasource='dataset'>...</myplugin:list>,data是在Action中设置的($this->assign('dataset',$list);), 在TagLib中获取变量的方法: $datasource=$this->tpl->get($attr['datasource']);
        4.5 判断attr是否设置用isset(),判断是否为空用empty(),注意：变量未设置,0,false,null,''都会被empty()认为是空, 如：<myplugin:list datasource='dataset' />, TagLib中 $limit=isset($attr['limit'])?(empty($attr['limit'])?20:$attr['limit']):20; 即使HTML属性未设置，也不会对TagLib中的变量读取造成错误, 如 <myplugin:list />,TabLig中 echo $attr['myvar'].'ok'; 会输出 ok.(myvar没有在$tags->attr中设置也不会有影响)
 *  5.标签中必需有一个属性,否则会无法执行!!!!!(2014-12-1),如<xcallinput:siteid/>,这样会无法执行,必需至少有一个属性,如<xcallinput:siteid value="{$data.siteID}"/>

 * example
        <taglib name="XcallInput" />
        <xcallinput:test Attr1="a1" attr2="a2" />
        <xcallinput:test_test my_attr1="a13" my_attr2="a23" />
        <xcallinput:testTest myAttr1="a13" MyAttr2="a23" />
 * @since 1.0 2014-10-30 by sutroon
*/