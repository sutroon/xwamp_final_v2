#install_php_2_0
<br />
**简介：**<br />
一款通用php安装向导程序，通过简单配置，即可嵌入到任何php项目作为安装向导使用。<br />
**截图：**<br />
![1.png](ProjectResourceLibrary/snapshot/1.png)<br /><br />
![2.png](ProjectResourceLibrary/snapshot/2.png)<br /><br />
![3.png](ProjectResourceLibrary/snapshot/3.png)<br /><br />
![4.png](ProjectResourceLibrary/snapshot/4.png)<br /><br />
![5.png](ProjectResourceLibrary/snapshot/5.png)<br /><br />
![6.png](ProjectResourceLibrary/snapshot/6.png)<br /><br />
![7.png](ProjectResourceLibrary/snapshot/7.png)<br /><br />