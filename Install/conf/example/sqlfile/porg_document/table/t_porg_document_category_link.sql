drop table if exists t_porg_document_category_link;
create table if not exists t_porg_document_category_link (
    category_id int primary key comment '栏目编号',
    url varchar(128) not null default '' comment '链接地址',
    target varchar(8) not null default '' comment '打开目标(_blank,_self,_top等)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '栏目链接表\r\n@since 1.0 <2015-10-26> SoChishun <14507427@qq.com> Added.\r\n';
