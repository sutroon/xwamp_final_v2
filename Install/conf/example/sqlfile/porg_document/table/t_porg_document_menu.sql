drop table if exists t_porg_document_menu;
create table if not exists t_porg_document_menu (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '名称',
    pid int not null default 0 comment '父级编号',
    `code` varchar(32) not null default '' comment '栏目代码(唯一)',
    visit_count int not null default 0 comment '浏览次数',
    picture_url varchar(255) not null default '' comment '图片路径',
    link_url varchar(255) not null default '' comment '链接地址',
    open_target varchar(15) not null default '' comment '打开目标(iframe名称,_blank,_self)',
    access varchar(255) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密)',
    user_name varchar(32) not null default '' comment '创建人用户名',
    remark varchar(32) not null default '' comment '备注',
    sort smallint not null default 0 comment '排列次序',
    `status` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_title (title),
    INDEX idx_pid (pid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '网站前台菜单表\r\n@since 1.0 <2016-6-17> SoChishun <14507247@qq.com> Added.';
