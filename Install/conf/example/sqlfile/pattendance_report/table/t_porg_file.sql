drop table if exists t_porg_file;
create table if not exists t_porg_file (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '标题',
    file_name varchar(32) not null default '' comment '文件名称',
    file_path varchar(255) not null default '' comment '文件路径',
    file_size varchar(32) not null default '' comment '文件大小',
    file_type varchar(32) not null default '' comment '文件类型',
    file_ext varchar(8) not null default '' comment '文件扩展名',
    user_id int not null default 0 comment '上传者用户编号',
    remark varchar(32) not null default '' comment '备注',
    ex_tags varchar(32) not null default '' comment '扩展标签',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '文件表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

-- insert into t_porg_file (title, file_name, file_path, file_size, file_ext, ex_tags) values ();