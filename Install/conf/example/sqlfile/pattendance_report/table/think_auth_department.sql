drop table if exists think_auth_department;
create table if not exists think_auth_department(
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '部门名称',
    pid int not null default 0 comment '上级部门编号',
    `code` varchar(32) not null default '' comment '部门代号(如营销总部=YXZB,区域营销分部=QYYXFB,营销小组=YXXZ,财务部门=CWBM,行政部门=XZBM)',
    rules varchar(128) not null default '' comment '权限规则编号',
    sort smallint not null default 0 comment '排序',
    remark varchar(32) not null default '' comment '备注',
    `status` smallint not null default 1 comment '部门状态(0=禁用,1=正常)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_pid (pid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户部门表\r\n@since 1.0 <2014-7-11> sutroon <14507247@qq.com> Added.';