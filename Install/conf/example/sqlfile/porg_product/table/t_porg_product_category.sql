drop table if exists t_porg_product_category;
create table if not exists t_porg_product_category (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '名称',
    pid int not null default 0 comment '父级编号',
    `code` varchar(32) not null default '' comment '类别代码(唯一)',
    item_count int not null default 0 comment '子项目数量(如相册显示相片数量等)',
    visit_count int not null default 0 comment '浏览次数',
    meta_keywords varchar(32) not null default '' comment 'meta关键词',
    meta_description varchar(128) not null default '' comment 'meta描述',
    image_url varchar(255) not null default '' comment '图片路径',
    link_url varchar(255) not null default '' comment '链接地址',
    user_name varchar(32) not null default '' comment '创建人用户名',
    access varchar(512) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密)',
    remark varchar(32) not null default '' comment '备注',
    sort smallint not null default 0 comment '排列次序',
    status smallint not null default 1 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_类别表\r\n@since 1.0 <2015-4-2> sutroon <14507247@qq.com> Added.';
