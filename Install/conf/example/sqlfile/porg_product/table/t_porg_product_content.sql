drop table if exists t_porg_product_content;
create table if not exists t_porg_product_content (
    product_id int comment '商品编号',
    `code` varchar(32) not null default '' comment '代码',
    title  varchar(32) not null default '' comment '标题',
    content varchar(20480) not null default '' comment '商品内容',
    INDEX idxu_id_code (product_id,code)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品内容表\r\n@since 1.0 <2016-7-1> SoChishun <14507427@qq.com> Added.\r\n';