drop table if exists t_porg_commission_discount;
create table if not exists t_porg_commission_discount (
    id int auto_increment primary key comment '主键编号',
    time_date date not null comment '日期',
    sale_type varchar(16) not null default '' comment '销售类型(现场;定金)',
    travel_agency_id int not null comment '旅行社编号',
    commission_rate_id int not null default 0 comment '佣金比例编号(配置)',
    parking_fee decimal(8,2) not null default 0 comment '停车费',
    personnel_fee decimal(8,2) not null default 0 comment '人头费',
    sichou_commission_fee decimal(8,2) not null default 0 comment '丝绸佣金',
    sijin_commission_fee decimal(8,2) not null default 0 comment '丝巾佣金',
    baihuo_commission_fee decimal(8,2) not null default 0 comment '百货佣金',
    subtotal decimal(8,2) not null default 0 comment '合计',
    user_name varchar(32) not null default '' comment '创建人用户名',
    status smallint not null default 0 comment '结算状态',
    create_time timestamp not null default current_timestamp comment '创建时间',
    store_id int not null comment '分店编号',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    update_time datetime comment '更新时间',
    INDEX idx_time_date (time_date),
    INDEX idx_branch_store_id (`store_id`),
    INDEX idx_travel_agency_id (`travel_agency_id`),
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '旅行社返佣表\r\n@since 1.0 <2016-05-28> SoChishun <14507247@qq.com> Added.';

