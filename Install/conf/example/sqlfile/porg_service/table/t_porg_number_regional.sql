drop table if exists t_porg_number_regional;
create table if not exists t_porg_number_regional (
  id int auto_increment primary key comment '主键编号',
  area_name varchar(32) not null comment '地区名称',
  segment_number varchar(16) not null comment '号码前段部分(号段)',
  segment_number_len3 char(3) not null default '' comment '号段前3位',
  segment_number_length smallint not null default 0 comment '号段长度',
  number_length smallint not null comment '号码长度',
  sort int not null default 0 comment '排列次序',
  status smallint not null default 0 comment '号码状态(0=未呼,1已呼)',
  INDEX idx_area_name (area_name),
  INDEX idx_segment_number_length (segment_number_length),
  INDEX idx_segment_number_len3 (segment_number_len3)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 comment '号码归属地表 1.0 2014-6-27 by sutroon\r\n2.0 2016-1-23 SoChishun 重构.';