<?php

/*
 * 栏目体验数据
 * <p>
 * select id, title, pid, concat('array(\'title\'=>\'',title,'\', \'type_name\'=>\'',type_name,'\'),')  from t_porg_document_category where status=1 order by pid, sort, id;
 * </p>
 * @version 1.0 2016-6-15 SoChishun Added.
 */

return array(
    'table' => 't_porg_document_category',
    'is_tree' => true, // 是否树形结构
    'data' => array(
        array('title' => '公司信息', 'type_name' => 'node', 'allow_subitem' => 'Y', 'code' => 'aboutus', 'children' => array(
                array('title' => '关于我们', 'type_name' => 'page', 'code' => 'about_us'),
                array('title' => '联系我们', 'type_name' => 'page', 'code' => 'contact_us'),
                array('title' => '企业荣誉', 'type_name' => 'file-list', 'code' => 'comp_honor'),
            )),
        array('title' => '新闻中心', 'type_name' => 'node', 'allow_subitem' => 'Y', 'code' => 'news', 'children' => array(
                array('title' => '本站公告', 'type_name' => 'document-list'),
                array('title' => '公司动态', 'type_name' => 'document-list'),
                array('title' => '行业新闻', 'type_name' => 'document-list'),
            )),
        array('title' => '成功案例', 'type_name' => 'file-list'),
        array('title' => '特殊栏目', 'type_name' => 'node', 'allow_subitem' => 'Y', 'children' => array(
                array('title' => '底部信息', 'type_name' => 'page', 'code' => 'footinfo'),
            )),
    ),
);
