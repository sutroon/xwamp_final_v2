<?php

/*
 * 创建管理员帐号脚本
 * <p>
 * 键名必需包含user,占位符{user_name},{password}
 * <p>
 * @version 1.0 2016-4-22 SoChishun Added.
 */

return array(
    /* user键名必需! (支持占位符{user_name},{password}) */
    'user' => "insert into t_porg_user (user_name, password, type_name, site_id, regist_ip, role_ids, status) values ('sa','123456', 'SYSTEMADMIN', 0, '127.0.0.1', 1, 1),('{user_name}','{password}', 'ADMIN', 1, '127.0.0.1', 2, 1);",
    /* 角色 */
    'group' => "insert into think_auth_group (title, rules, site_id) values ('系统管理员','*',1),('管理员','*',1);",
    /* 部门 */
    'department' => '',
    /* 网站配置 */
    'siteconf' => "insert into t_porg_site_conf (site_title, site_code, session_prefix) values ('XWAM销售网站', 'xwam', 'xwam'),('网站名称', 'ljs', 'ljs');",
);
