<?php

/*
 * 网站前台导航菜单体验数据
 * 
 * @version 1.0 2016-6-17 SoChishun Added.
 */

return array(
    'table' => 't_porg_document_menu',
    'is_tree' => true, // 是否树形结构
    'data' => array(
        array('title' => '====顶部导航菜单====', 'link_url' => '#', 'code' => 'topnav', 'children' => array(
                array('title' => '网站首页', 'link_url' => 'Index/index'),
                array('title' => '公司简介', 'link_url' => 'Aboutus/about'),
                array('title' => '产品展示', 'link_url' => 'Product/product_list'),
                array('title' => '企业荣誉', 'link_url' => 'CompHonor/index'),
                array('title' => '成功案例', 'link_url' => 'CompVisualize/index'),
                array('title' => '客户留言', 'link_url' => 'Feedback/index'),
                array('title' => '联系我们', 'link_url' => 'Contactus/index'),
            )),
        array('title' => '====底部导航菜单====', 'link_url' => '#', 'code' => 'footnav', 'children' => array(
                array('title' => '网站首页', 'link_url' => 'Index/index'),
                array('title' => '关于我们', 'link_url' => 'Aboutus/about'),
                array('title' => '新闻中心', 'link_url' => 'News/news_list'),
                array('title' => '企业荣誉', 'link_url' => 'CompHonor/index'),
                array('title' => '产品展示', 'link_url' => 'Product/product_list'),
                array('title' => '成功案例', 'link_url' => 'CompVisualize/index'),
                array('title' => '在线留言', 'link_url' => 'Feedback/index'),
                array('title' => '联系我们', 'link_url' => 'Contactus/index'),
            )),
        array('title' => '====栏目菜单.关于我们====', 'link_url' => '#', 'code' => 'aboutus', 'children' => array(
                array('title' => '关于我们', 'link_url' => 'Aboutus/about'),
                array('title' => '联系我们', 'link_url' => 'Contactus/index'),
                array('title' => '企业荣誉', 'link_url' => 'CompHonor/index'),
            )),
    ),
);
