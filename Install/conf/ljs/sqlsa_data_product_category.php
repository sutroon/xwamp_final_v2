<?php

/*
 * 商品类别体验数据
 * 
 * @version 1.0 2016-6-20 SoChishun Added.
 */

return array(
    'table' => 't_porg_product_category',
    'is_tree' => true, // 是否树形结构
    'data' => array(
        array('title' => '苗木工程'),
        array('title' => '大型观叶植物'),
        array('title' => '小型观叶植物'),
        array('title' => '水生植物'),
        array('title' => '花卉租赁'),
    ),
);
