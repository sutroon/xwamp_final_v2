<?php

/**
 * ljs 项目配置文件
 * @since 1.0 <2015-7-10> SoChishun Added.
 * @since 1.1 2016-6-17 SoChishun 新增default_admin和scripts配置
 */
return array(
    /* 应用程序名称 (安装向导界面显示) */
    'app_name' => 'XCRM',
    /* 预设默认值-数据库 (2015-7-10) */
    'default_db' => array(
        'db_name' => 'db_ljs_v1', // 数据库名称
        'db_host' => '127.0.0.1', // 数据库服务器
        'db_user' => 'xcall', // 数据库用户名
        'db_pwd' => 'root!@#xcall098)(*Admin%^&', // 数据库密码
        'table_prefix' => '', // 数据表前缀
    ),
    /* 预设默认值-管理员账户 (2016-6-17) */
    'default_admin' => array(
        'user_name' => 'admin', // 管理员用户名
        'password' => 'admin123', // 管理员密码
        'email' => '', // 管理员邮箱
    ),
    /* 创建脚本配置 (2016-6-17) */
    'scripts' => array(
        /* 创建数据表脚本 */
        'create_table' => 'sqls_table.php',
        /* 创建数据表视图脚本 */
        'create_view' => '',
        /* 创建数据表函数脚本 */
        'create_routine' => '',
        /* 创建权限节点脚本 */
        'create_rule' => array(
            'file' => 'sqlsa_rule.php', // 文件名称
            'mode' => 'array-data', // 数据模式：array-data|array-sql|sql-file (array-data会自动解析,array-sql则直接逐行执行,sql-file则读取拆分逐行执行)
        ),
        /* 创建用户帐号脚本 */
        'create_user' => 'sqls_user.php',
        /* 创建体验数据脚本 (2016-6-15) */
        'create_data' => array(
            'advertising' => array(
                'title' => '广告体验数据', // checkbox文本
                'file' => 'sqlsa_data_advertising.php',
                'mode' => 'array-data', // 数据模式：array-data|array-sql|sql-file (array-data会自动解析,array-sql则直接逐行执行,sql-file则读取拆分逐行执行)
            ),
            'channel' => array(
                'title' => '栏目体验数据', // checkbox文本
                'file' => 'sqlsa_data_channel.php',
                'mode' => 'array-data', // 数据模式：array-data|array-sql|sql-file (array-data会自动解析,array-sql则直接逐行执行,sql-file则读取拆分逐行执行)
            ),
            'navmenu' => array(
                'title' => '导航菜单体验数据', // checkbox文本
                'file' => 'sqlsa_data_navmenu.php',
                'mode' => 'array-data', // 数据模式：array-data|array-sql|sql-file (array-data会自动解析,array-sql则直接逐行执行,sql-file则读取拆分逐行执行)
            ),
            'productcategory' => array(
                'title' => '商品类别体验数据', // checkbox文本
                'file' => 'sqlsa_data_product_category.php',
                'mode' => 'array-data', // 数据模式：array-data|array-sql|sql-file (array-data会自动解析,array-sql则直接逐行执行,sql-file则读取拆分逐行执行)
            ),
        ),
        /* 创建补丁脚本 (2016-6-17) */
        'create_patch' => 'sqls_patch.php',
    ),
);

