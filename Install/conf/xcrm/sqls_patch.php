<?php

/*
 * 创建补丁脚本
 * <p>补丁脚本一般是最后面执行的，用于对数据库体验数据缺省值的补充</p>
 * @version 1.0 2016-6-17 SoChishun Added.
 */

return array(
    "update think_auth_rule set status=1;", // 批量更新权限节点状态
);
