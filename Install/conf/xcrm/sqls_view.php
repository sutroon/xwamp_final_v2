<?php

/*
 * 创建数据表视图的脚本
 * @version 1.0 2016-6-29 SoChishun Added.
 */
return array(
    'v_porg_customer_appointment' => "create or replace view v_porg_customer_appointment as select c.serial_no , c.name , c.sex , c.telphone , c.address , c.buy_count , c.total_amount , c.buy_time , c.user_name , c.labels , c.create_time , c.site_id , a.id , a.customer_id , a.appointment_type , a.appointment_time , a.appointment_remark , a.review_time , a.review_result , a.review_remark , a.status from t_porg_customer_appointment a left join t_porg_customer c on a.customer_id=c.id order by id desc;",
    'v_porg_customer_dispatch' => "create or replace view v_porg_customer_dispatch as select c.serial_no, c.name, c.sex, c.telphone, c.mobile, c.address, c.buy_count, c.total_amount, c.buy_time, c.review_time, c.interested, c.site_id, d.id, d.title, d.customer_id, d.old_user_name, d.new_user_name, d.admin_name, d.dispatch_remark, d.status, d.create_time from t_porg_customer_dispatch d left join t_porg_customer c on d.customer_id=c.id order by id desc;",
);
