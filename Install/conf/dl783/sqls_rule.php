<?php

/*
 * 权限规则节点脚本
 * @version 1.0 2016-6-13 SoChishun Added.
 */

return array(
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (1,'','系统','0','','M1_XT',1,'','M',0,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (2,'','系统设置','1','','XT_M2_XTSZ',2,'','M',0,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (3,'SiteConf.site_edit','网站设置','2','','XT_M3_WZSZ',3,'SiteConfig/site_edit','M',2,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (4,'SystemConf.conf_list','系统变量配置','2','','XT_M3_BLPZ',4,'SystemConf/conf_list','M',2,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (5,'CustomConf.conf_list','用户变量配置','2','','XT_M3_BLPZ',5,'CustomConf/conf_list','M',2,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (6,'','用户','0','','M1_YH',6,'','M',0,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (7,'','用户管理','6','','YH_M2_YHGL',7,'','M',0,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (8,'UserAdmin.user_list','管理员管理','7','','YH_M3_GLYGL',8,'UserAdmin/user_list','M',2,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (9,'Role.index','角色管理','7','','YH_M3_JSGL',9,'Role/role_list','M',2,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (10,'','内容','0','','M1_NR',10,'','M',0,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (11,'','内容管理','10','','POrg_NR_M2_NRGL',11,'','M',0,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (12,'Channels.channel_list','站点栏目','11','','NR_M3_ZDLM',12,'Channels/channel_list','M',2,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (13,'NewsCategory.category_list','友情链接','11','','NR_M3_YQLJGL',13,'Links/links_list','M',2,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (14,'Advertising.advert_list','媒体广告','11','','NR_M3_MTGGGL',14,'Advertising/advert_list','M',2,1);",
    "insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (15,'Notices.document_list','公告管理','11','','NR_M3_ZDLM',15,'Notices/document_list','M',2,1);",
);
