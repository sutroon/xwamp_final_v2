<?php

/*
 * 栏目体验数据
 * <p>
 * select id, title, pid, concat('array(\'title\'=>\'',title,'\', \'type_name\'=>\'',type_name,'\'),')  from t_porg_document_category where status=1 order by pid, sort, id;
 * </p>
 * @version 1.0 2016-6-15 SoChishun Added.
 */

return array(
    'table' => 't_porg_document_category',
    'is_tree' => true, // 是否树形结构
    'data' => array(
        array('title' => '关于我们', 'type_name' => 'document-list', 'children' => array(
                array('title' => '在线咨询', 'type_name' => 'url'),
                array('title' => '经验团队', 'type_name' => 'file-list'),
                array('title' => '分析师介绍', 'type_name' => 'document-list'),
                array('title' => '公司简介', 'type_name' => 'page'),
                array('title' => '交易所优势', 'type_name' => 'page'),
                array('title' => '董事会寄语', 'type_name' => 'page'),
                array('title' => '企业文化', 'type_name' => 'page'),
                array('title' => '荣誉资质', 'type_name' => 'file-list'),
                array('title' => '组织架构', 'type_name' => 'page'),
                array('title' => '企业招聘', 'type_name' => 'page'),
            )),
        array('title' => '新闻中心', 'type_name' => 'document-list', 'children' => array(
                array('title' => '本站公告', 'type_name' => 'document-list'),
                array('title' => '每日要闻', 'type_name' => 'document-list'),
                array('title' => '公司动态', 'type_name' => 'document-list'),
                array('title' => '财经资讯', 'type_name' => 'document-list'),
                array('title' => '产品资讯', 'type_name' => 'document-list'),
                array('title' => '行业新闻', 'type_name' => 'document-list'),
                array('title' => '新手的成功途径', 'type_name' => 'document-list'),
                array('title' => '经典案例', 'type_name' => 'document-list'),
            )),
        array('title' => '交易品种', 'type_name' => 'document-list', 'children' => array(
                array('title' => '产品介绍', 'type_name' => 'document-list', 'children' => array(
                        array('title' => '农幅产品', 'type_name' => 'page'),
                        array('title' => '金属产品', 'type_name' => 'page'),
                        array('title' => '化工产品', 'type_name' => 'page'),
                    )),
            )),
        array('title' => '下载中心', 'type_name' => 'file-list', 'children' => array(
                array('title' => '电脑软件下载', 'type_name' => 'file-list'),
                array('title' => '手机软件下载', 'type_name' => 'file-list'),
            )),
        array('title' => '服务指南', 'type_name' => 'document-list', 'children' => array(
                array('title' => '开户流程', 'type_name' => 'page'),
                array('title' => '出入金流程', 'type_name' => 'page'),
            )),
        array('title' => '政策法规', 'type_name' => 'document-list', 'children' => array(
                array('title' => '交收管理办法', 'type_name' => 'page'),
                array('title' => '结算管理办法', 'type_name' => 'page'),
                array('title' => '会员管理办法', 'type_name' => 'page'),
                array('title' => '法律法规', 'type_name' => 'document-list'),
            )),
    ),
);
