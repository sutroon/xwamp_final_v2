<?php

/*
 * 广告体验数据
 * 广告类型(B:网幅广告,包含横幅、按钮、通栏、竖边、巨幅等;T:文本链接广告;EDM:电子邮件广告;S:赞助式广告;BC:内容式广告,包含关键字广告、比对内容广告等;C:分类式广告;I:插播式广告,一般指弹出式广告、浮动广告;RM:富媒体,使用浏览器插件或其他脚本语言,具有复杂视觉效果和交互功能的网络广告;V:视频广告)
 * @version 1.0 2016-6-15 SoChishun Added.
 */

return array(
    'table' => 't_porg_advertising',
    'data' => array(
        array('location' => '全网首页', 'channel' => '首页', 'ad_name' => '第1轮播焦点图', 'title' => '首页第1轮播焦点图', 'ad_code' => 'SY_JDT_RM1'),
        array('location' => '全网首页', 'channel' => '首页', 'ad_name' => '首页2栏通屏', 'title' => '首页2栏通屏', 'ad_code' => 'SY_B1'),
        array('location' => '全网首页', 'channel' => '首页', 'ad_name' => '2屏右侧Banner', 'title' => '首页2屏右侧Banner', 'ad_code' => 'SY_B2'),
        array('location' => '资讯类频道首页', 'channel' => '关于我们频道首页', 'ad_name' => '首页1栏通屏', 'title' => '关于我们频道首页1栏通屏', 'ad_code' => 'GYWMSY_B1'),
        array('location' => '资讯类频道首页', 'channel' => '新闻中心频道首页', 'ad_name' => '首页1栏通屏', 'title' => '新闻中心频道首页1栏通屏', 'ad_code' => 'XWZXSY_B1'),
        array('location' => '资讯类频道首页', 'channel' => '产品中心频道首页', 'ad_name' => '首页1栏通屏', 'title' => '产品中心频道首页1栏通屏', 'ad_code' => 'CPZXSY_B1'),
        array('location' => '资讯类频道首页', 'channel' => '下载中心频道首页', 'ad_name' => '首页1栏通屏', 'title' => '下载中心频道首页1栏通屏', 'ad_code' => 'XZZXSY_B1'),
        array('location' => '资讯类频道首页', 'channel' => '服务指南频道首页', 'ad_name' => '首页1栏通屏', 'title' => '服务指南频道首页1栏通屏', 'ad_code' => 'FWZNSY_B1'),
        array('location' => '资讯类频道首页', 'channel' => '招商合作频道首页', 'ad_name' => '首页1栏通屏', 'title' => '招商合作频道首页1栏通屏', 'ad_code' => 'ZSHZSY_B1'),
        array('location' => '资讯类频道首页', 'channel' => '在线咨询频道首页', 'ad_name' => '首页1栏通屏', 'title' => '在线咨询频道首页1栏通屏', 'ad_code' => 'ZXZXSY_B1'),
    ),
);
