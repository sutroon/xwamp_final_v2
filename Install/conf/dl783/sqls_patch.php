<?php

/*
 * 创建补丁脚本
 * <p>补丁脚本一般是最后面执行的，用于对数据库体验数据缺省值的补充</p>
 * @version 1.0 2016-6-17 SoChishun Added.
 */

return array(
    "update t_porg_advertising set user_name='admin', site_id=1;", // 批量更新广告归属
    "update t_porg_document_category set user_name='admin', site_id=1;", // 批量更新栏目归属
);
