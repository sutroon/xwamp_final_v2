<?php

/**
 * dl783 项目配置文件
 * 
 * @since 1.0 <2015-7-10> SoChishun Added.
 */
return array(
    /* 应用程序名称 */
    'app_name' => 'DL783',
    /* 预设默认值 */
    'default_db' => array(
        'db_name' => 'db_dl783_v1',
        'db_host' => '127.0.0.1',
        'db_user' => 'xcall',
        'db_pwd' => 'root!@#xcall098)(*Admin%^&',
        'table_prefix' => '',
    ),
    /* 预设默认值-管理员账户 (2016-6-17) */
    'default_admin' => array(
        'user_name' => 'admin', // 管理员用户名
        'password' => 'admin123', // 管理员密码
        'email' => '', // 管理员邮箱
    ),
    /* 创建脚本配置 (2016-6-17) */
    'scripts' => array(
        /* 创建数据表脚本 */
        'create_table' => 'sqls_table.php',
        /* 创建权限节点脚本 */
        'create_rule' => 'sqls_rule.php',
        /* 创建用户帐号脚本 */
        'create_user' => 'sqls_user.php',
        /* 创建体验数据脚本 (2016-6-15) */
        'create_data' => array(
            'advertising' => array(
                'title' => '广告体验数据', // checkbox文本
                'file' => 'sqls_data_advertising.php',
                'mode' => 'array', // 数据模式：array|sql (array会自动解析,sql则直接执行)
            ),
            'channel' => array(
                'title' => '栏目体验数据', // checkbox文本
                'file' => 'sqls_data_channel.php',
                'mode' => 'array', // 数据模式：array|sql (array会自动解析,sql则直接执行)
            ),
        ),
        /* 创建补丁脚本 (2016-6-17) */
        'create_patch' => 'sqls_patch.php',
    ),
);

