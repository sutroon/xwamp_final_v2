<?php
/**
 * 系统设置
 * @since 1.0 <2015-5-26> SoChishun <14507247@qq.com> Added.
 */
ob_start();
include './inc/function.php';
$config = load_config();
if (!$config) {
    die('资源文件目录不存在!');
}
session_start();
if (!isset($_SESSION['dbconfig'])) {
    if (!I('db_name')) {
        exit('数据库配置无效!');
    }
    $_SESSION['dbconfig'] = $_POST;
}
define('MENU_CUR', '初始化数据');
msglog('====stepc_db_install.php====',false);
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo MENU_CUR ?>-INSTALL</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="//libs.useso.com/js/yui/3.17.2/cssreset/cssreset-min.css" rel="stylesheet" />
        <link href="./skin/theme-blue.css" rel="stylesheet" />
        <script src="//libs.useso.com/js/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>   
    </head>
    <body class="page-step-setting">
        <?php require './inc/inc_head.php' ?>
        <div class="bodier">
            <fieldset>
                <h3>安装数据库</h3>
                <div style="max-height:420px; width:800px; overflow-y: auto;">
                    <table id="creating-tables-status" class="table-grid" cellspacing="0">
                        <tr><th>安装项目</th><th class="al-right">安装进度</th></tr>
                        <?php
                        create_table($config);
                        ?>
                    </table>
                </div>
                <div class="footer">
                    <a href="./stepb_db_setting.php">上一步</a>
                    <a href="./stepd_data.php">下一步</a>
                </div>
            </fieldset>
        </div>
    </body>
</html>
<?php

/**
 * 创建数据表
 * @param array $config
 * @return type
 * @since 1.0 <2015-5-26> SoChishun <14507247@qq.com> Added.
 * @since 1.1 2016-6-17 SoChishun 重构.
 */
function create_table($config) {
    if (!in_array('mysql', get_loaded_extensions())) {
        die('系统不支持mysql数据库扩展，请联系管理员开启!');
    }
    $echo_tpl = '<tr><td class="table">%s</td><td class="status break-line al-right">%s</td></tr>';
    $echo_tpl_c2 = '<tr><td colspan="2" class="status break-line al-right">%s</td></tr>';
    $conf_db = $_SESSION['dbconfig'];
    $link = @mysql_connect($conf_db['db_host'], $conf_db['db_user'], $conf_db['db_pwd']);
    if (false === $link) {
        msglog(sprintf($echo_tpl_c2, '<span class="red">' . mysql_error() . '</span>'));
        return;
    }
    msglog(sprintf($echo_tpl_c2, '<span class="ok">连接数据库成功!</span>'));
    // 创建数据库
    $res = @mysql_query('CREATE DATABASE IF NOT EXISTS ' . $conf_db['db_name'] . ' DEFAULT CHARSET utf8 COLLATE utf8_general_ci;');
    if (false === $link) {
        msglog(sprintf($echo_tpl_c2, '<span class="red">' . mysql_error() . '</span>'));
        return;
    }
    msglog(sprintf($echo_tpl_c2, '<span class="ok">创建数据库[' . $conf_db['db_name'] . ']成功!</span>'));
    // 选择数据库
    $res = @mysql_select_db($conf_db['db_name']);
    if (false === $res) {
        msglog(sprintf($echo_tpl_c2, '<span class="red">' . mysql_error() . '</span>'));
        return;
    }
    // 设置编码
    $res = @mysql_query('SET NAMES UTF8');
    if (false === $res) {
        msglog(sprintf($echo_tpl_c2, '<span class="red">' . mysql_error() . '</span>'));
        return;
    }
    // 安装数据库数据表对象脚本
    $asqls = include MODULE_PATH . $config['scripts']['create_table'];
    patchQuery($asqls, true);
    // 安装数据库视图对象脚本
    $asqls = include MODULE_PATH . $config['scripts']['create_view'];
    patchQuery($asqls, true);
    // 安装数据库程序对象脚本
    $asqls = include MODULE_PATH . $config['scripts']['create_routine'];
    patchQuery($asqls, true);
    // 安装权限节点脚本
    $acontent = $config['scripts']['create_rule'];
    if ('array-data' == $acontent['mode']) {
        require './inc/ArrayToSQLHelper.php';
        $atsh = new ArrayToSQLHelper();
        $adata = include MODULE_PATH . $acontent['file'];
        if (isset($adata['table'])) {
            $table = $adata['table'];
            unset($adata['table']);
            $asqls = $atsh->generateRuleSql($table, $adata);
        } else {
            msglog(sprintf($echo_tpl_c2, '<span class="red">权限节点数据内容格式有误!</span>'));
            return;
        }
    } else if ('array-sql' == $acontent['mode']) {
        $asqls = $acontent;
    } else {
        msglog(sprintf($echo_tpl_c2, '<span class="red">权限节点数据内容格式不受支持!</span>'));
        return;
    }
    patchQuery($asqls);
    mysql_close($link);
    $_SESSION['suinstall-db-config'] = $conf_db;
    return array('status' => true, 'id' => $id, 'info' => $id);
}
?>
    