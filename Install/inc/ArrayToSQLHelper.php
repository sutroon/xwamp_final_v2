<?php

/**
 * 数组数据转SQL命令 类
 *
 * @since VER:1.0; DATE:2016-6-17; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class ArrayToSQLHelper {

    private $n = 1;

    /**
     * 重置
     * <p>
     * 非首次运行generateTreeSql()前需执行重置方法!
     * </p>
     * @since 1.0 2016-6-17 SoChishun Added.
     */
    public function reset() {
        $this->n = 1;
    }

    /**
     * 生成层级命令
     * <p>
     * 用于类别数据
     * </p>
     * @param string $table 数据表名称
     * @param array $adata 数据数组
     * @param integer $pid 父级编号 (默认0)
     * @return array
     * @since 1.0 2016-6-17 SoChishun Added.
     * @example generateTreeSql($a['table'], $a['data']);
     */
    public function generateTreeSql($table, $adata, $pid = 0) {
        $asqls = array();
        foreach ($adata as $row) {
            $children = isset($row['children']) ? $row['children'] : false;
            if ($children) {
                unset($row['children']);
            }
            $akeys = array_keys($row);
            $avals = array_values($row);
            $asqls[$this->n] = sprintf("insert into %s (id, pid, `%s`) values (%d, %d, '%s');", $table, implode('`,`', $akeys), $this->n, $pid, implode("','", $avals));
            $this->n++;
            if ($children) {
                $asqls += $this->generateTreeSql($table, $children, $this->n - 1);
            }
        }
        return $asqls;
    }

    /**
     * 生成规则命令
     * <p>用于权限菜单节点</p>
     * @param string $table 数据表名称
     * @param array $adata 数据数组
     * @param integer $pid 父级编号 (默认0)
     * @return array
     * @since 1.0 2016-6-17 SoChishun Added.
     * @example generateRuleSql($a['table'], $a['data']);
     */
    public function generateRuleSql($table, $adata, $pid = 0) {
        $asqls = array();
        foreach ($adata as $row) {
            if (isset($row['enable'])) { // 是否可用
                if (false === $row['enable']) {
                    continue; // 记录不可用,跳过
                }
                unset($row['enable']); // 去掉是否可用
            }
            if (isset($row['comment'])) {
                unset($row['comment']); // 去掉注释
            }
            $children = isset($row['children']) ? $row['children'] : false;
            if ($children) {
                unset($row['children']);
            }
            $akeys = array_keys($row);
            $avals = array_values($row);
            $asqls[$this->n] = sprintf("insert into %s (id, pid, `%s`) values (%d, %d, '%s');", $table, implode('`,`', $akeys), $this->n, $pid, implode("','", $avals));
            $this->n++;
            if ($children) {
                $asqls += $this->generateRuleSql($table, $children, $this->n - 1);
            }
        }
        return $asqls;
    }

    /**
     * 生成列表命令
     * @param string $table 数据表名称
     * @param array $adata 数据数组
     * @return array
     * @since 1.0 2016-6-16 SoChishun Added.
     */
    public function generateListSql($table, $adata) {
        $asqls = array();
        foreach ($adata as $row) {
            $asqls[] = sprintf("insert into %s (`%s`) values ('%s');", $table, implode('`,`', array_keys($row)), implode("','", array_values($row)));
        }
        return $asqls;
    }

}
