<!-- 2016-6-13 -->
<div class="header">
    <div class="frame-style">
        <div class="logo"><a href="index.php" target="_blank"><?php echo $config['app_name'] ?> INSTALL安装向导</a></div>
        <div class="breadcrumb">
            <?php
            $amenus = array('欢迎界面', '环境检测', '数据库配置', '初始化数据', '完成安装');
            echo '<ul>';
            foreach ($amenus as $smenu) {
                echo '<li', ($smenu == MENU_CUR ? ' class="cur"' : ''), '>', $smenu, '</li>';
            }
            echo '</ul>';
            ?>
        </div>
    </div>
</div>