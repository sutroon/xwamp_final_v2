<?php

/* 
 * 文件路径：\ThinkPHP\Common\functions.php
 */

/**
 * 用于实例化访问控制器
 * @param string $name 控制器名
 * @param string $path 控制器命名空间（路径）
 * @return Think\Controller|false
 */
function controller($name, $path = '') {
    $layer = C('DEFAULT_C_LAYER');
    if (!C('APP_USE_NAMESPACE')) {
        $class = parse_name($name, 1) . $layer;
        import(MODULE_NAME . '/' . $layer . '/' . $class);
    } else {
        // 修改前-Begin
        // $class = ( $path ? basename(ADDON_PATH) . '\\' . $path : MODULE_NAME ) . '\\' . $layer;
        // 修改前-End
        // 修改后-Begin
        $class = ( $path ? MODULE_NAME . '\\' . basename(ADDON_PATH) . '\\' . $path : MODULE_NAME ) . '\\' . $layer;
        // 修改后-End
        $array = explode('/', $name);
        foreach ($array as $name) {
            $class .= '\\' . parse_name($name, 1);
        }
        $class .= $layer;
    }
    if (class_exists($class)) {
        return new $class();
    } else {
        return false;
    }
}