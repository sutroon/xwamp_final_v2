<?php

namespace Org\SueCrypt;

/**
 * SueFile 类
 *
 * @since VER:1.0; DATE:2016-4-26; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class SueFile {

    /**
     * 字符串加密解密算法
     * 加密后变成乱码,可还原
     * @param string $data 字符串
     * @param string $key 密匙
     * @return string 返回加密后的密文或还原后的原文
     * @since 1.0 <2014-10-15> SoChishun Added.
     * @since 2.0 <2016-4-26> SoChishun 拆分,规范化,静态方法
     * @example
     *  $str = encrypt_file('hello sutroon ! 好厉害！'); // Jyl�n�ah�-Sl�������������yP�
     *  echo $str . '<br />';
     *  echo encrypt_file($str, 'DENCRYPT'); // hello sutroon ! 好厉害！
     */
    public static function encrypt($data, $key = 'sue123!@#') {
        $td = mcrypt_module_open(MCRYPT_DES, '', 'ecb', ''); //使用MCRYPT_DES算法,ecb模式
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        $ks = mcrypt_enc_get_key_size($td);
        $key = substr(md5($key), 0, $ks);
        mcrypt_generic_init($td, $key, $iv); //初始处理
        $data = mcrypt_generic($td, $data); //加密
        //结束
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return $data;
    }

    /**
     * 解密
     * @param string $data
     * @param type $key
     * @return string
     * @since 1.0 2016-4-26 SoChishun Added.
     */
    public static function decrypt($data, $key = 'sue123!@#') {
        $td = mcrypt_module_open(MCRYPT_DES, '', 'ecb', ''); //使用MCRYPT_DES算法,ecb模式
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        $ks = mcrypt_enc_get_key_size($td);
        $key = substr(md5($key), 0, $ks);
        mcrypt_generic_init($td, $key, $iv); //初始处理
        $data = trim(mdecrypt_generic($td, $data)) . "\n"; //解密后,可能会有后续的\0,需去掉
        //结束
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return $data;
    }

}
