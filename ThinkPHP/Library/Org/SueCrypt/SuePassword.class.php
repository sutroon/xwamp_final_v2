<?php

namespace Org\SueCrypt;

/**
 * SuePassword 类
 *
 * @since VER:1.0; DATE:2016-4-26; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class SuePassword {

    // ==密码加密==
    /**
     * 加密密码
     * @param string $str 原密码
     * @param string $key 混淆密匙
     * @param string $flag 密码标识,建议加上,以兼容未加密的密码
     * @return string 加密后的密码,长度不超过原文长度的2倍(密码越长,增量比例越少),密文可能包含符号(%#*.)
     * @since 1.0 <2015-7-9> SoChishun Added.
     * @example password('root!@#xcall098)(*Admin%^&') => '*p2y3U6nn87DUNmX7XltpCnh95ZajtiPflhUTxJrVbVGTk'
     */
    public function encrypt($str, $key = 'Sue123!@#', $flag = '*') {
        if (!$str) {
            return $str;
        }
        $str = SueDes::encrypt($str, $key); // Des加密
        return $flag . SueStrmix::encrypt($str); // 混淆
    }

    /**
     * 解密密码
     * @param string $str 加密后的密码
     * @param string $key 混淆密匙
     * @param string $flag 密码标识
     * @return string 密码原文
     * @since 1.0 <2015-7-9> SoChishun Added.
     * @example depassword('*p2y3U6nn87DUNmX7XltpCnh95ZajtiPflhUTxJrVbVGTk') => root!@#xcall098)(*Admin%^&
     */
    public function decrypt($str, $key = 'Sue123!@#', $flag = '*') {
        if (!$str || ($flag && $flag !== $str[0])) {
            return $str;
        }
        if ($flag) {
            $str = substr($str, strlen($flag));
        }
        $org = SueStrmix::decrypt($str); // 反混淆
        return SueDes::decrypt($org, $key); // 反Des加密
    }

    // ==/密码加密==
}
