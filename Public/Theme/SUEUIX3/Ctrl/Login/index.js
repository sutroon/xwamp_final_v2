/**
 * index.js
 * 
 * @since 1.0.0.0 <2015-3-23> SoChishun <14507247@qq.com> Added.
 */

$(function () {
    // Enter提交表单 2016-1-15 SoChishun Added.
    $(document).keyup(function (e) {
        if (!e) {
            e = window.event; //火狐中是 window.event
        }
        if ((e.keyCode || e.which) == 13) {
            $(':submit').trigger('click');
            return false;
        }
    });
    // 垂直居中效果 2015-12-1 SoChishun Added.
    $(window).resize(function () {
        var $w = $(this);
        $('.login-panel').height($w.height());
        var $p = $('.login-form');
        $p.css({
            position: 'absolute',
            left: ($w.width() - $p.outerWidth()) / 2,
            top: ($w.height() - $p.outerHeight()) / 2 + $(document).scrollTop()
        });
    }).resize();
    // 点击刷新验证码
    $('.change-captcha').click(function () {
        var $img = $(this).find('img');
        $img.attr('src', $img.attr('src'));
        return false;
    })
    // 2014-9-27 by sutroon 新增验证码动态校验功能
    $('input[name="captcha"]').keyup(function () {
        var $input = $(this);
        var val = $input.val();
        if (val.length > 3) {
            var url = $(this).data('confirmUrl').replace('varkey', val);
            $.get(url, function (data) {
                var $em = $input.parent().find('em');
                if ($em[0]) {
                    $em.removeClass('error');
                    $em.removeClass('success');
                    $em.addClass(data ? 'success' : 'error').text(data ? '[正确]' : '[错误]');
                } else {
                    $input.after(data ? '<em class="success">[正确]</em>' : '<em class="error">[错误]</em>');
                }
            });
        } else {
            $input.parent().find('em').remove();
        }
    })
    // 保存记住我 2016-1-19 SoChishun 重构
    $('[name="remember_me"]').click(function () {
        if ($(this).prop('checked')) {
            Cookies.set('remember_me', JSON.stringify({u: $('#login_name').val(), p: $('#password').val()}));
        } else {
            Cookies.remove('remember_me');
        }
    })
    read_member();
// 读取记住我 2015-5-5
    function read_member() {
        var data = Cookies.get('remember_me');
        if (!data) {
            $('[name="remember_me"]').prop('checked', false);
            return;
        }
        data = JSON.parse(data);
        $('#login_name').val(data.u);
        $('#password').val(data.p);
        $('[name="remember_me"]').prop('checked', true);
    }

// 表单验证 2015-3-23
    $('#frm-login').validate({
        errorElement: "em",
        rules: {
            login_name: {required: true, minlength: 3, maxlength: 16},
            password: {required: true, minlength: 1, maxlength: 32}
        }
    });
});