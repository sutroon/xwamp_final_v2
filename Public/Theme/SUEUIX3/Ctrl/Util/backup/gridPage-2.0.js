/**
 * 网格页面类
 * @type Object
 * @since 1.0 <2015-10-17> SoChishun Added.
 * @since 2.0 <2016-5-9> SoChishun 重构, 取消实例对象, GridPage 重命名为 gridPage
 */
var gridPage = {
    config: {gridContainer: '#grid-container'},
    /* 
     * 网格页面视图初始化入口
     * <br />依赖：IFrameDialog
     * @since 1.0 <2015-10-16> SoChishun Added.
     */
    ui_init: function () {
        // 表格操作下拉菜单开关 2015-10-16 SoChishun Added.
        $('.xsui-operate-group>li').hover(function () {
            $(this).find('ul').show();
        }, function () {
            $(this).find('ul').hide();
        });

        // 网格辅助,显示行号和复选框 2015-10-17 SoChishun Added.
        $('.xsui-table').table();

        this.header_button_click_handler();
        this.table_button_click_handler();
    },
    load_grid: function (queryParams) {
        var $gc = $(this.config.gridContainer);
        $gc.data('queryParams', queryParams);
        $(document.body).append('<div id="doc-loading" style="position:absolute;left:45%;width:200px;height:36px;top:45%;background:#FFFFFF;opacity:0.8;filter:alpha(opacity=60);z-index:9999999;"><div style="border:2px solid #B7CDFC;cursor:pointer;margin:0 auto;width:200px;height:36px;line-height:36px;font-size:12px;background:#fff;text-align:center;position:relative;">加载中,请稍后...<a href="#" onclick="$(this).parent().parent().remove();return false;" style="position:absolute;right:2px;top:2px;text-decoration:none;color:#666;">&times</a></div></div>');
        $.ajax({
            type: 'post',
            dataType: 'html',
            url: $gc.data('url'),
            data: queryParams,
            success: function (data) {
                $('#doc-loading').remove();
                $gc.html(data);
            },
            error: function () {
                $('#doc-loading').remove();
                $('<div title="消息">抱歉,数据加载失败,请联系管理员!</div>').dialog();
            },
        });
    },
    /**
     * 预置网格页面头部常见按钮的事件委托方法
     * <br />不在这些常见按钮中的其他按钮就只能自己编程搞定了~
     * @since 1.0 <2015-10-17> SoChishun Added.
     */
    header_button_click_handler: function () {
        // 高级搜索 2015-10-17 SoChishun Added.
        $('#btn-search-dialog').on('click', function () {
            ifrmDialog.open_search_dialog('#search-dialog');
            return false;
        })
        // 新增按钮 2015-10-16 SoChishun Added. 2016-1-29 新增 lnk-dialog-g全局弹窗类
        $('.xsui-page-header .lnk-add-dialog, .lnk-dialog-g').on('click', function () {
            var $a = $(this);
            ifrmDialog.open_add_dialog($a.attr('href'), {title: $a.attr('title') ? $a.attr('title') : $a.text()}, 'item-add');
            return false;
        });
        // 查看按钮 2016-4-21 SoChishun Added.
        $('.lnk-view-dialog').on('click', function () {
            var $a = $(this);
            ifrmDialog.open_view_dialog($a.attr('href'), {title: $a.attr('title') ? $a.attr('title') : $a.text()}, 'item-view');
            return false;
        });
        // 批量删除和批量后台处理按钮 2015-10-16 SoChishun Added.
        $('.xsui-page-header .lnk-remove, .xsui-page-header .lnk-copy, .xsui-page-header .lnk-progress').on('click', function () {
            gridPage.ajax_progress_rows($(this), true);
            return false;
        });
        // 批量编辑处理按钮 2016-2-2 SoChishun Added.
        $('.xsui-page-header .lnk-edit-dialog').on('click', function () {
            var ids = $('.xsui-table').table('getCheckedValues');
            if (!ids) {
                alert('没有选中任何项目!');
                return false;
            }
            var $a = $(this);
            ifrmDialog.open_add_dialog($a.attr('href').replace('varkey', ids), {title: $a.attr('title') ? $a.attr('title') : $a.text()}, 'items-add');
            return false;
        });
        $('.xsui-page-header .lnk-refresh').on('click', function () {
            location.reload();
        });
        // 批量删除和批量后台处理按钮 2015-10-16 SoChishun Added.
        $('.lnk-tab').on('click', function () {
            window.parent.mditab.add_tab_by_link($(this));
            return false;
        });

    },
    /**
     * 预置网格单元格中常见按钮的事件委托方法
     * <br />不在这些常见按钮中的其他按钮就只能自己编程搞定了~
     * @since 1.0 <2015-10-17> SoChishun Added.
     */
    table_button_click_handler: function () {
        // table-cell 编辑按钮 2015-10-16 SoChishun Added.
        $('td .lnk-edit-dialog').on('click', function () {
            var $a = $(this);
            ifrmDialog.open_edit_dialog($a.attr('href'), {title: $a.attr('title')}, 'exten-edit-' + $a.data('id'));
            return false;
        });
        // 批量删除和批量后台处理按钮 2015-10-16 SoChishun Added.
        $('td .lnk-remove, td .lnk-copy, td .lnk-confirm, td .lnk-progress').on('click', function () {
            gridPage.ajax_progress_rows($(this));
            return false;
        });
        // 批量删除和批量后台处理按钮 2015-10-16 SoChishun Added.
        $('td .lnk-tab').on('click', function () {
            window.parent.mditab.add_tab_by_link($(this));
            return false;
        });

    },
    /**
     * 后台处理多行
     * @param String url
     * @since 1.0 <2015-10-17> SoChishun Added.
     * @since 2.0 <2015-11-10> SoChishun 新增确认提示功能.[--示例:data-options="prompt_value:'yes',prompt_message:'您将删除{n}条记录,为防止错误删除,请输入命令[yes]'"--]
     * @since VER:1.0; DATE:2016-1-14; AUTHOR:SoChishun; DESC:新增lnk-copy
     */
    ajax_progress_rows: function ($a, multiple) {
        var n = 1;
        var url = $a.attr('href');
        if (true == multiple) {
            var ids = $('.xsui-table').table('getCheckedValues');
            if (!ids) {
                alert('没有选中任何项目!');
                return;
            }
            n = ids.split(',').length;
            url = url.replace('varkey', ids);
        }
        var soptions = $a.data('options');
        if (soptions) {
            try {
                var options = eval('({' + soptions + '})');
            } catch (e) {
                alert(e);
                return false;
            }
            // 验证口令
            //  data-options="prompt_message:'您必需输入口令才能执行删除操作！\n请输入口令:yes',prompt_value:'yes'"
            if (options.prompt_value) {
                if (undefined == options.prompt_message) {
                    options.prompt_message = '请输入口令：';
                } else if (options.prompt_message.indexOf('{n}')) {
                    options.prompt_message = options.prompt_message.replace('{n}', n);
                }
                if (undefined == options.prompt_err) {
                    options.prompt_err = '口令输入错误!';
                }
                var val = prompt(options.prompt_message);
                if (null == val) {
                    return false;
                }
                if (options.prompt_value != val) {
                    alert(options.prompt_err);
                    return false;
                }
            }
            // 确认框
            //  data-options="confirm_message:'您确定要将密码重置为123456吗?'"
            if (options.confirm_message) {
                if (options.confirm_message.indexOf('{n}')) {
                    options.confirm_message = options.confirm_message.replace('{n}', n);
                }
                if (!confirm(options.confirm_message)) {
                    return false;
                }
            }
            // 提示框
            if (options.alert_message) {
                alert(options.alert_message);
            }
        } else {
            // 删除的时候默认提示,不提示则添加 data-options="confirm_message:false"
            if ($a.is('.lnk-remove')) {
                if (!confirm(true == multiple ? '您确定要删除勾选的' + n + '条记录吗?' : '您确定要删除此记录吗?')) {
                    return false;
                }
            }
            // 复制的时候默认提示,不提示则添加 data-options="confirm_message:false"
            if ($a.is('.lnk-copy')) {
                if (!confirm(true == multiple ? '您确定要复制勾选的' + n + '条记录吗?' : '您确定要复制此记录吗?')) {
                    return false;
                }
            }
        }
        $.getJSON(url, function (data) {
            if ('string' == typeof (data)) {
                data = JSON.parse(data);
            }
            if (data.status) {
                location.replace(location.href);
                //location.reload(true);
            } else {
                $('<div title="结果">' + data.info + '</div>').dialog();
            }
        });
    },
    /**
     * 超链接按钮倒计时
     * @param $a jQuery封装好的超链接对象
     * @returns {boolean}
     * @since 1.0 2014-10-9 by sutroon
     * @example <a class="btn-refresh btn-icon lnk-countdown" href="#" data-interval="1000" data-count="5" data-func="func1()">刷新</a>
     */
    countDownAnchor: function ($a) {
        if ($a.hasClass('ui-button-disabled')) {
            return false;
        }
        var func = $a.data('func');
        if (undefined !== func) {
            eval(func);
        }
        $a.button({disabled: true});
        var count = $a.data('count');
        if (undefined == count) {
            count = 5;
        }
        var i = count;
        var interval = $a.data('interval');
        if (undefined == interval) {
            interval = 1000;
        }
        var $btnTxt = $a.find('.ui-button-text');
        $btnTxt.append('<span>' + count + '</span>');
        var cd_btn = setInterval(switch_show, interval, $a);

        function switch_show($a) {
            i--;
            if (i < 1) {
                i = count;
                $a.button({disabled: false});
                $btnTxt.find('span').remove();
                clearInterval(cd_btn);
            }
            $btnTxt.find('span').text(i);
        }

        return false;
    },
    /**
     * 自动刷新页面局部内容
     * @param selector 容器选择器,&lt;span id="auto-refresh-page"&gt;&lt;/span&gt;
     * @param defaultTime 默认时间,默认30秒
     * @param minTime 最小时间,默认10秒,0=不限
     * @param func 定时执行的方法,如 refresh_page
     * @param isFirst 是否首次运行,放在局部刷新后操作的方法内时需填写false
     * @since 1.0 2014-10-30 by sutroon
     * @example
     *  1.在页面全局javascript中autoRefreshContent('#auto-refresh-page', 30, 10, refresh_page);
     *  2.在局部页面刷新后操作方法内autoRefreshContent('#auto-refresh-page', 30, 10, refresh_page, false);
     *  具体如：
     *      &lt;div id="main-content" data-func="config_main_table();"&gt;...&lt;span id="auto-refresh-page">&lt;/span&gt;...&lt;/div&gt;
     *      &lt;script&gt;
     *          function config_main_table(){
     *              ...
     *              autoRefreshContent('#auto-refresh-page', 30, 10, refresh_page, false);
     *          }
     *          config_main_table();
     *          autoRefreshContent('#auto-refresh-page', 30, 10, refresh_page);
     *      &lt;/script&gt;
     */
    autoRefreshContent: function (selector, defaultTime, minTime, func, isFirst) {
        if (undefined == window['auto_refresh_time']) {
            window['auto_refresh_time'] = defaultTime;
        }
        if ($(selector).html().length < 1) {
            $(selector).append('<input type="checkbox" value="1" /> <span>-</span>/<input type="text" size="2" value="' + window['auto_refresh_time'] + '" />秒');
        }
        $(selector + ' input:text').blur(function () {
            var val = $(this).val();
            if (isNaN(val)) {
                alert('请填写数值!');
                $(this).val(defaultTime);
                return;
            }
            if (minTime > 0 && val < minTime) {
                alert('最小值是' + minTime);
                $(this).val(defaultTime);
                return;
            }
            window['auto_refresh_time'] = val;
        })
        if (false === isFirst) {
            return;
        }
        $(selector + ' input:text').val(window['auto_refresh_time']);
        var i = 0;

        function refresh_this_page() {
            i++;
            $(selector + ' span').text(i);
            if (i >= window['auto_refresh_time']) {
                if (undefined !== func) {
                    func();
                }
                i = 0;
            }
        }

        if (undefined !== si) {
            clearInterval(si);
        }
        var si = setInterval(refresh_this_page, 1000);
        if (undefined == $(selector + ' input:checked')[0]) {
            clearInterval(si);
        }
        $(selector + ' input:checkbox').click(function () {
            if ($(this).prop('checked')) {
                si = setInterval(refresh_this_page, 1000);
            } else {
                clearInterval(si);
            }
        })
    },
}