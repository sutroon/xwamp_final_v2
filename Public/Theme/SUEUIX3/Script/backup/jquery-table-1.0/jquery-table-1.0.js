/**
 * xsui-table插件
 * <br />显示行号和多选复选框
 * @param {jQuery} $
 * @since 1.0 <2015-10-17> SoChishun Added.
 * @since VER:1.1; DATE:2016-1-14; AUTHOR:SoChishun; DESC:
 *      新增 data-checkbox="false"功能
 *      新增 data-checkbox-attr='name="id" value=":value"' 功能
 * @example $('.xsui-table').table();
 */
(function ($) {
    $.fn.table = function (options) {
        // 插件方法
        if (options && 'string' == typeof (options)) {
            switch (options) {
                case 'getCheckedValues': // 获取勾选的值
                    return $.fn.table.getCheckedValues(this);
                    break;
            }
            return false;
        }
        // 插件初始化
        var opts = $.extend({}, $.fn.table.defaults, options);
        if (!opts.rownumbers && !opts.multipleSelect) {
            return false;
        }
        return this.each(function () {
            var $this = $(this);
            // 2016-1-14 新增 data-checkbox="false"功能
            var checkbox = $this.data('checkbox');
            var checkattr = $this.data('checkboxAttr');
            if(!checkattr){
                checkattr='';
            }
            if (undefined == checkbox) {
                checkbox = opts.multipleSelect;
            } else {
                checkbox = 'true' == checkbox.toString().toLowerCase();
            }
            $this.find('thead tr').prepend((opts.rownumbers ? '<th class="cell-rownumber">&nbsp;</th>' : '') + (checkbox ? '<th class="cell-multiple-select"><input type="checkbox" onclick="" /></th>' : ''));
            if (checkbox) {
                $this.find('thead :checkbox').click(function () {
                    $this.find('tbody :checkbox').prop('checked', $(this).prop('checked'));
                });
            }
            var std = opts.rownumbers ? '<td class="cell-rownumber">:i</td>' : '';
            std += checkbox ? '<td class="cell-multiple-select"><input type="checkbox"' + checkattr + ' /></td>' : '';
            $this.find('tbody tr').each(function (i) {
                $(this).prepend(std.replace(':i', (i + 1)).replace(':value', $(this).data('key')));
            });
        });
    };
    /**
     * 获取勾选的值
     * @param {HTMLTable} table
     * @returns String
     * @since 1.0 <2015-10-17> SoChishun Added.
     */
    $.fn.table.getCheckedValues = function (table) {
        var ids = '', id = '';
        $(table).find('tbody .cell-multiple-select :checked').each(function () {
            id = $(this).parent().parent().data('key');
            if (id) {
                ids += ',' + id;
            }
        })
        return ids ? ids.substring(1) : ids;
    }
    /**
     * 默认配置
     * @since 1.0 <2015-10-17> SoChishun Added.
     */
    $.fn.table.defaults = {
        rownumbers: true, // 是否显示行数列
        multipleSelect: true, // 是否显示checkbox列
    };
})(jQuery);