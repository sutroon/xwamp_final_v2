/**
 * jquery-messager-1.0.js
 * 
 * @since 1.0 <2016-1-19> SoChishun <14507247@qq.com> Added.
 */

/**
 * 消息对话框
 * @since 1.0 2014-9-19 by sutroon
 * @example $.messager.alert('提示','删除失败');
 */
(function ($) {
    $.extend({
        'messager': {
            /**
             * 弹出消息提示框
             * @param msg
             * @param caption
             * @since 1.0 2014-9-18 by sutroon
             * @example $.somessager.alert('提示','请选中一行');
             */
            'alert': function (title, msg) {
                $('<div title="' + (title ? title : '消息') + '">' + msg + '</div>').dialog({
                    buttons: {
                        '确定': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }
        }
    });
})(jQuery);

