/**
 * jquery-pagination-1.0.js
 * 
 * @since 1.0 <2016-1-8> SoChishun <14507247@qq.com> Added.
 */
// example:
// <div id="aap" class="xsui-pagination" data-page-size="10" data-record-count="3000" data-page="5" data-query-params="a=a1&b=b1&c=c1"></div>
// $('#aap').pagination();
(function ($) {
    $.fn.pagination = function (options) {
        // 插件初始化
        var opts = $.extend({}, $.fn.pagination.defaults, options);
        return this.each(function () {
            var $this = $(this);
            var pager = opts;
            if ($this.data('pageSize')) {
                pager.pageSize = $this.data('pageSize');
            }
            if ($this.data('recordCount')) {
                pager.recordCount = $this.data('recordCount');
            }
            if ($this.data('pageVar')) {
                pager.pageVar = $this.data('pageVar');
            }
            if ($this.data('queryParams')) {
                pager.queryParams = $this.data('queryParams');
            }
            pager.pageCount = Math.ceil(pager.recordCount / pager.pageSize); // 分页数量
            var tplvalues = {};
            if (pager.listCount) {
                var midnum = Math.floor(pager.listCount / 2);
                var avar = [1, pager.listCount];
                if (pager.currentPage > midnum) {
                    avar[0] = pager.currentPage - midnum;
                    avar[1] = avar[0] + pager.listCount;
                    if (avar[1] > pager.pageCount) {
                        avar[0] = pager.pageCount - pager.listCount;
                        avar[1] = pager.pageCount;
                    }
                }
                var numbtns = '';
                for (var i = avar[0]; i <= avar[1]; i++) {
                    numbtns += '<a class="pagination-num" href="">' + i + '</a>';
                }
                tplvalues.list = numbtns;
            }
            tplvalues.first = '<a class="pagination-first" href="#">' + pager.firstText + '</a>';
            tplvalues.prev = '<a class="pagination-prev" href="#">' + pager.prevText + '</a>';
            tplvalues.next = '<a class="pagination-next" href="#">' + pager.nextText + '</a>';
            tplvalues.last = '<a class="pagination-last" href="#">' + pager.lastText + '</a>';
            if (pager.jumpTextbox) {
                tplvalues.textbox = '<input type="text" size="5" class="pagination-textbox" />';
            }
            if (pager.jumpCombobox) {
                var str = '';
                for (var i = 1; i <= pager.pageCount; i++) {
                    str += '<option value="' + i + '">' + i + '</option>';
                }
                tplvalues.combobox = '<select class="pagination-combobox">' + str + '</select>';
            }
            if (pager.messageText) {
                tplvalues.message = pager.messageText;
                var replaces = ['currentPage', 'recordCount', 'pageSize', 'pageCount'];
                for (var i in replaces) {
                    tplvalues.message = tplvalues.message.replace('{' + replaces[i] + '}', pager[replaces[i]]);
                }
            }
            var bar = '';
            var elements = ['first', 'prev', 'list', 'next', 'last', 'textbox', 'combobox', 'message'];
            for (var i in elements) {
                if (pager.template.indexOf(elements[i])) {
                    bar += tplvalues[elements[i]];
                }
            }
            $this.html(bar);
        });
    };
    /**
     * 默认配置
     * @since 1.0 <2015-10-17> SoChishun Added.
     */
    $.fn.pagination.defaults = {
        urlMode: 'PATHINFO', // URL访问模式
        currentPage: 1, // 当前页码
        recordCount: 0, // 记录数量
        pageSize: 30, // 分页尺寸
        pageVar: 'page', // 分页变量名称
        queryParams: '', // 分页查询变量
        listCount: 5, // 页码列表显示数量
        jumpTextbox: true, // 跳转文本框
        jumpCombobox: true, // 跳转下拉框
        firstText: '首页',
        prevText: '上一页',
        nextText: '下一页',
        lastText: '末页',
        ellipseText: '...', // 页码列表省略显示文本
        messageText: '<span class="pagination-stat">页次 {currentPage}/{pageCount}, 每页 {pageSize} 条, 总共 {recordCount} 条</span>', // 统计信息
        template: ['first', 'prev', 'list', 'next', 'last', 'textbox', 'combobox', 'message'],
        debug: true,
        paged: function (pager) {
            if (this.debug) {
                console.log(pager);
            }
        }
    };
})(jQuery);


// http://www.xuebuyuan.com/2215285.html
function url_setvar(url, name, value) {
    var patter1 = "(\\?)" + name + "=([^&]+)(&|$)";
    var patter2 = "(\\&)" + name + "=([^&]+)(&|$)";
    if (typeof value != 'undefined') { //赋值  继续在追加URL后面追加值
        if (url.indexOf('?') == -1) {
            return (url + '?' + name + '=' + value);
        } else {
            return (url + '&' + name + '=' + value);
        }
    } else { //取值
        var reg = new RegExp(patter1, "i");
        var m = url.match(reg);
        if (m) {
            return m[2];
        } else {
            reg = new RegExp(patter2, "i");
            m = url.match(reg);
            if (m) {
                return m[2];
            }
        }
        return '';
    }
}



//删除url指定名称的参数
function url_delvar(url, name) {
    var patter1 = "(\\?)" + name + "=([^&]+)(&|$)";
    var patter2 = "(\\&)" + name + "=([^&]+)(&|$)";
    var reg = new RegExp(patter1, "i");
    var m = url.match(reg);
    url = url.replace(reg, "?");
    reg = new RegExp(patter2, "i");
    url = url.replace(reg, "");
    return url;
}