/**
 * 中国省市区插件
 * @param {type} $
 * @returns {undefined}
 * @since 1.0 2014-12-11 by sutroon
 * @remark 搞了1个工作日,总算完成了一个心头大事!
 * @example
 *  <input type="text" name="address" />
 *  <div id="region1"><select name="province"></select><select name="city"></select><select name="area"></select></div>
 *  <div id="region2"><select name="province"></select><select name="city"></select><select name="area"></select></div>
 *  <select name="province1"></select><select name="city1"></select><select name="area1"></select>     * 
 *  $(function () {
 *      $.cnRegion({'province': '福建省', 'city': '厦门市', 'area': '思明区', 'names': 'province,city,area', 'container': '#region1', 'changeFunc': ('undefined' == typeof (changeRegion) ? undefined : changeRegion)});
 *      $.cnRegion({'province': '山西省', 'city': '太原市', 'area': '小店区', 'names': 'province,city,area', 'container': '#region2'});
 *      $.cnRegion({'province': '', 'city': '', 'area': '', 'names': 'province1,city1,area1'});
 *  });
 *  function changeRegion(province, city, area) {
 *      $('input[name="address"]').val(province + ' ' + city + ' ' + area);
 *  }     
 */
(function ($) {
    $.extend({
        'cnRegion': function (options) {
            var opts = $.extend({}, {'province': '', 'city': '', 'area': '', 'url': '', 'names': 'province,city,area', 'container': '', 'changeFunc': undefined}, options);
            if (!opts.url) {
                opts.url = $('#hidval_region_api').val();
            }
            var names = opts.names.split(',');
            if (opts.container) {
                opts.container += ' ';
            }
            names[0] = opts.container + '#' + names[0];
            names[1] = opts.container + '#' + names[1];
            names[2] = opts.container + '#' + names[2];
            var $province = $(names[0]);
            var $city = $(names[1]);
            var $area = $(names[2]);
            var defaultoption = '<option value="">请选择</option>';
            $province.html(defaultoption);
            $city.html(defaultoption);
            $area.html(defaultoption);
            $.post(opts.url, function (data) {
                if (typeof (data) == 'string') {
                    return;
                }
                var str = defaultoption;
                for (var i in data) {
                    str += '<option value="' + data[i] + '">' + data[i] + '</option>';
                }
                $province.html(str);
                if (opts.province) {
                    $province.find('option[value="' + opts.province + '"]').prop('selected', 'selected');
                }
            });
            if (opts.province) {
                load_city(opts.province);
            }
            if (opts.city) {
                load_area(opts.province, opts.city);
            }
            $province.change(function () {
                if ($(this).val()) {
                    load_city($(this).val());
                    $area.html(defaultoption);
                } else {
                    $city.html(defaultoption);
                    $area.html(defaultoption);
                }
                if ('function' == typeof (opts.changeFunc)) {
                    opts.changeFunc($(this).val(), '', '');
                }
            });
            $city.change(function () {
                if ($(this).val()) {
                    load_area($(names[0]).val(), $(this).val());
                } else {
                    $area.html(defaultoption);
                }
                if ('function' == typeof (opts.changeFunc)) {
                    opts.changeFunc($(names[0]).val(), $(this).val(), '');
                }
            });
            if ('function' == typeof (opts.changeFunc)) {
                $area.change(function () {
                    opts.changeFunc($(names[0]).val(), $(names[1]).val(), $(this).val());
                })
            }
            function load_city(province) {
                if (!province) {
                    $city.html('<option value="">请选择</option>');
                    return;
                }
                $.post(opts.url, {'province': province}, function (data) {
                    if (typeof (data) === 'string') {
                        return;
                    }
                    var str = defaultoption;
                    for (var i in data) {
                        str += '<option value="' + data[i] + '">' + data[i] + '</option>';
                    }
                    $city.html(str);
                    if (opts.city) {
                        $city.find('option[value="' + opts.city + '"]').prop('selected', 'selected');
                    }
                });
            }
            function load_area(province, city) {
                if (!province) {
                    $area.html('<option value="">请选择</option>');
                    return;
                }
                $.post(opts.url, {'province': province, 'city': city}, function (data) {
                    if (typeof (data) === 'string') {
                        return;
                    }
                    var str = defaultoption;
                    for (var i in data) {
                        str += '<option value="' + data[i] + '">' + data[i] + '</option>';
                    }
                    $area.html(str);
                    if (opts.area) {
                        $area.find('option[value="' + opts.area + '"]').prop('selected', 'selected');
                    }
                });
            }
        }
    });
})(jQuery);