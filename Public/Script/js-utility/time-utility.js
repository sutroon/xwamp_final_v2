/**
 * 时间通用类
 * @type type
 * @since 1.0 <2015-11-9> SoChishun Added.
 */
var TimeUtil = {
    /**
     * 日期合法性验证
     * <br />格式为：YYYY-MM-DD或YYYY/MM/DD
     * @param {type} DateStr
     * @returns {Boolean}
     */
    IsValidDate: function (DateStr)
    {
        var sDate = DateStr.replace(/(^\s+|\s+$)/g, ''); //去两边空格;   
        if (sDate == '') {
            return true;
        }
        //如果格式满足YYYY-(/)MM-(/)DD或YYYY-(/)M-(/)DD或YYYY-(/)M-(/)D或YYYY-(/)MM-(/)D就替换为''   
        //数据库中，合法日期可以是:YYYY-MM/DD(2003-3/21),数据库会自动转换为YYYY-MM-DD格式   
        var s = sDate.replace(/[\d]{ 4,4 }[\-/]{ 1 }[\d]{ 1,2 }[\-/]{ 1 }[\d]{ 1,2 }/g, '');
        if (s == '') //说明格式满足YYYY-MM-DD或YYYY-M-DD或YYYY-M-D或YYYY-MM-D   
        {
            var t = new Date(sDate.replace(/\-/g, '/'));
            var ar = sDate.split(/[-/:]/);
            if (ar[0] != t.getYear() || ar[1] != t.getMonth() + 1 || ar[2] != t.getDate())
            {
                //alert('错误的日期格式！格式为：YYYY-MM-DD或YYYY/MM/DD。注意闰年。');   
                return false;
            }
        } else
        {
            //alert('错误的日期格式！格式为：YYYY-MM-DD或YYYY/MM/DD。注意闰年。');   
            return false;
        }
        return true;
    },
    /**
     * 日期时间检查
     * <br />格式为：YYYY-MM-DD HH:MM:SS
     * @param {type} str
     * @returns {Boolean}
     */
    CheckDateTime: function (str)
    {
        var reg = /^(\d+)-(\d{ 1,2 })-(\d{ 1,2 }) (\d{ 1,2 }):(\d{ 1,2 }):(\d{ 1,2 })$/;
        var r = str.match(reg);
        if (r == null)
            return false;
        r[2] = r[2] - 1;
        var d = new Date(r[1], r[2], r[3], r[4], r[5], r[6]);
        if (d.getFullYear() != r[1])
            return false;
        if (d.getMonth() != r[2])
            return false;
        if (d.getDate() != r[3])
            return false;
        if (d.getHours() != r[4])
            return false;
        if (d.getMinutes() != r[5])
            return false;
        if (d.getSeconds() != r[6])
            return false;
        return true;
    },
    /**
     * 把日期分割成数组
     * @returns {TimeUtility.toArray.myArray}
     */
    toArray: function ()
    {
        var myDate = this;
        var myArray = Array();
        myArray[0] = myDate.getFullYear();
        myArray[1] = myDate.getMonth();
        myArray[2] = myDate.getDate();
        myArray[3] = myDate.getHours();
        myArray[4] = myDate.getMinutes();
        myArray[5] = myDate.getSeconds();
        return myArray;
    },
    /**
     * 取得日期数据信息
     * @param {String} interval 数据类型(y 年 m月 d日 w星期 ww周 h时 n分 s秒)
     * @returns {Number|String}
     */
    DatePart: function (interval)
    {
        var myDate = this;
        var partStr = '';
        var Week = ['日', '一', '二', '三', '四', '五', '六'];
        switch (interval)
        {
            case 'y' :
                partStr = myDate.getFullYear();
                break;
            case 'm' :
                partStr = myDate.getMonth() + 1;
                break;
            case 'd' :
                partStr = myDate.getDate();
                break;
            case 'w' :
                partStr = Week[myDate.getDay()];
                break;
            case 'ww' :
                partStr = myDate.WeekNumOfYear();
                break;
            case 'h' :
                partStr = myDate.getHours();
                break;
            case 'n' :
                partStr = myDate.getMinutes();
                break;
            case 's' :
                partStr = myDate.getSeconds();
                break;
        }
        return partStr;
    },
    /**
     * 取得当前日期所在月的最大天数
     * @returns {TimeUtility.MaxDayOfDate.result}
     */
    MaxDayOfDate: function ()
    {
        var myDate = this;
        var ary = myDate.toArray();
        var date1 = (new Date(ary[0], ary[1] + 1, 1));
        var date2 = date1.dateAdd(1, 'm', 1);
        var result = dateDiff(date1.Format('yyyy-MM-dd'), date2.Format('yyyy-MM-dd'));
        return result;
    },
    /**
     * 取得当前日期所在周是一年中的第几周
     * @returns {unresolved}
     */
    WeekNumOfYear: function ()
    {
        var myDate = this;
        var ary = myDate.toArray();
        var year = ary[0];
        var month = ary[1] + 1;
        var day = ary[2];
        document.write('< script language=VBScript\> \n');
        document.write('myDate = Date(\'' + month + '-' + day + '-' + year + '\') \n');
        document.write('result = DatePart(\'ww\', myDate) \n');
        document.write(' \n');
        return result;
    },
    /**
     * 字符串转成日期类型
     * <br />格式 MM/dd/YYYY MM-dd-YYYY YYYY/MM/dd YYYY-MM-dd  
     * @param {type} DateStr
     * @returns {Date}
     */
    StringToDate: function (DateStr)
    {

        var converted = Date.parse(DateStr);
        var myDate = new Date(converted);
        if (isNaN(myDate))
        {
            //var delimCahar = DateStr.indexOf('/')!=-1?'/':'-';  
            var arys = DateStr.split('-');
            myDate = new Date(arys[0], --arys[1], arys[2]);
        }
        return myDate;
    },
    /**
     * 判断是否闰年
     * @param {DateTime} date 日期对象
     * @returns {Boolean}
     */
    isLeapYear: function (date)
    {
        if (undefined == date) {
            date = new Date();
        }
        return (0 == date.getYear() % 4 && ((date.getYear() % 100 != 0) || (date.getYear() % 400 == 0)));
    },
    /**
     * 返回格式化后的日期字符串
     * @param {String} format 格式,如: yyyy-MM-dd hh:mm:ss W
     * @param {DateTime} date 日期对象
     * @returns {String}
     * @since 1.1 2014-9-26 by sutroon 新增W支持
     * @example format('yyyy-MM-dd HH:mm:ss W',new Date());
     */
    format: function (format, date) {
        if (!date) {
            date = new Date();
        }
        if (!format) {
            format = 'yyyy-MM-dd hh:mm:ss';
        }
        var week = ['日', '一', '二', '三', '四', '五', '六'];
        var o = {
            'W+': week[date.getDay()], // week
            "M+": date.getMonth() + 1, //month
            "d+": date.getDate(), //day
            "h+": date.getHours(), //hour
            "H+": date.getHours(), //hour
            "m+": date.getMinutes(), //minute
            "i+": date.getMinutes(), //minute
            "s+": date.getSeconds(), //second
            "q+": Math.floor((date.getMonth() + 3) / 3), //quarter
            "S": date.getMilliseconds() //millisecond
        }
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        }
        for (var k in o)
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
            }
        return format;
    },
    /**
     * 求两个时间的天数差 日期格式为 YYYY-MM-dd
     * @param {type} DateOne
     * @param {type} DateTwo
     * @returns {Number}
     */
    daysBetween: function (DateOne, DateTwo)
    {
        var OneMonth = DateOne.substring(5, DateOne.lastIndexOf('-'));
        var OneDay = DateOne.substring(DateOne.length, DateOne.lastIndexOf('-') + 1);
        var OneYear = DateOne.substring(0, DateOne.indexOf('-'));

        var TwoMonth = DateTwo.substring(5, DateTwo.lastIndexOf('-'));
        var TwoDay = DateTwo.substring(DateTwo.length, DateTwo.lastIndexOf('-') + 1);
        var TwoYear = DateTwo.substring(0, DateTwo.indexOf('-'));

        var cha = ((Date.parse(OneMonth + '/' + OneDay + '/' + OneYear) - Date.parse(TwoMonth + '/' + TwoDay + '/' + TwoYear)) / 86400000);
        return Math.abs(cha);
    },
    /**
     * 日期计算
     * @param {type} strInterval
     * @param {type} Number
     * @returns {Date}
     */
    DateAdd: function (strInterval, Number) {
        var dtTmp = new Date();
        switch (strInterval) {
            case 's' :
                return new Date(Date.parse(dtTmp) + (1000 * Number));
            case 'n' :
                return new Date(Date.parse(dtTmp) + (60000 * Number));
            case 'h' :
                return new Date(Date.parse(dtTmp) + (3600000 * Number));
            case 'd' :
                return new Date(Date.parse(dtTmp) + (86400000 * Number));
            case 'w' :
                return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));
            case 'q' :
                return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
            case 'm' :
                return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
            case 'y' :
                return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        }
    },
    DateDiff: function (strInterval, dtEnd) {
        var dtStart = new Date();
        if (typeof dtEnd == 'string')//如果是字符串转换为日期型  
        {
            dtEnd = StringToDate(dtEnd);
        }
        switch (strInterval) {
            case 's' :
                return parseInt((dtEnd - dtStart) / 1000);
            case 'n' :
                return parseInt((dtEnd - dtStart) / 60000);
            case 'h' :
                return parseInt((dtEnd - dtStart) / 3600000);
            case 'd' :
                return parseInt((dtEnd - dtStart) / 86400000);
            case 'w' :
                return parseInt((dtEnd - dtStart) / (86400000 * 7));
            case 'm' :
                return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtStart.getFullYear()) * 12) - (dtStart.getMonth() + 1);
            case 'y' :
                return dtEnd.getFullYear() - dtStart.getFullYear();
        }
    },
};