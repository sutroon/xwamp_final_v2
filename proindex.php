<?php
/**
 * 项目超级入口
 * <p>用于众多开发项目的登录</p>
 * @version 1.0 2016-6-24 SoChishun Added.
 */
// 版本号
$version = '1.0';
// 项目配置
$aprojects = array(
    'xcall_a11v3' => array(
        'summary' => 'xcall呼叫管理平台,公司核心产品之一', // 项目描述摘要
        'duration' => '2016-6-24至-', // 项目周期
        'url' => 'http://localhost:8899',
    ),
    'xcrm_v2' => array(
        'summary' => 'xcrm客户关系管理系统(xcall配套项目),公司核心产品之二', // 项目描述摘要
        'duration' => '2016-6-24至-', // 项目周期
        'url' => 'http://localhost:8803',
    ),
    'xwamp_v2' => array(
        'summary' => 'dn968,dl783等投资会员官网项目', // 项目描述摘要
        'duration' => '2016-6-24至-', // 项目周期
        'url' => 'http://localhost:8502',
    ),
    'luolai' => array(
        'summary' => 'dn968,dl783等投资会员官网项目', // 项目描述摘要
        'duration' => '2016-6-24至-', // 项目周期
        'url' => 'http://localhost:8504',
    ),
);
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>项目超级入口-<?php echo $version ?></title>
        <style type="text/css">
            body { font-size:12px; color:#333;}
            p{line-height:21px; margin:5px;}
            strong {color:#444;}
            fieldset{border:solid 1px #CCC; max-width: 600px;}
            legend a { text-decoration: none;}
            h2, h3 { margin:10px 0;}
            #plan-content {display:none; border-top:dotted 1px #CCC; padding-bottom: 10px; max-height: 300px; overflow: auto;}
        </style>
    </head>
    <body>
        <h1>项目超级入口</h1>
        <?php
        foreach ($aprojects as $title => $proj) {
            echo '<ul class="proj">';
            echo '<li>项目名称：', $title, '</li>';
            echo '<li>描述摘要：', $proj['summary'], '</li>';
            echo '<li>项目周期：', $proj['duration'], '</li>';
            echo '<li>项目入口：<a href="', $proj['url'], '" target="_blank">', $proj['url'], '</a></li>';
            echo '</ul>';
        }
        ?>
        <fieldset>
            <legend>2016-27周计划 <a href="#" onclick="toggle();return false;">[详情]</a></legend>
            <div id="plan-content">
                <section>
                    <h3>==== 公司项目 ====</h3> 
                    <p>
                        <strong>xcall网站计划：</strong><br />
                        无，重心放在jcxcrm客户端
                    </p>
                    <p>
                        <strong>xcrm网站计划：</strong><br />
                        1 恢复激进改进而导致不兼容源码<br />
                        2 修复发现的问题
                    </p>
                    <p>
                        <strong>jxcrm客户端计划：</strong><br />
                        1 完成员工管理模块界面搭建<br />
                        2 完成xcrm接口模块开发，能从jcxcrm调用xcrm接口模块获取数据
                    </p>
                    <p>
                        <strong>xcall服务器助理计划：</strong><br />
                        无，重心放在jcxcrm客户端
                    </p>
                </section>
                <section>
                    <h3>==== 个人项目 ====</h3>
                    <p>
                        <strong>wx公众号计划：</strong><br />
                        1 完成演示代码的测试<br />
                        2 搭建完整公众号
                    </p>
                    <p>
                        <strong>xwamp案例计划：</strong><br />
                        1 完成gms案例开发
                    </p>
                </section>
            </div>
        </fieldset>
        <script type="text/javascript">
            function toggle() {
                var dom = document.getElementById('plan-content');
                if (dom.style.display == 'block') {
                    dom.style.display = 'none';
                } else {
                    dom.style.display = 'block';
                }
            }

        </script>
    </body>
</html>
